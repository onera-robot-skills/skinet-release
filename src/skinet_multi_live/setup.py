from setuptools import setup

package_name = 'skinet_multi_live'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='BPelletier71',
    maintainer_email='baptiste.pelletier@onera.fr',
    description='Multi Live Skillset Net',
    license='ONERA/DTIS',
    tests_require=['pytest'],
    entry_points={
            'console_scripts': [
                    'skinet_multi_live = skinet_multi_live.skinet_multi_live_sub:main',
            ],
    },
)
