from robot_language import *

import sys
from os import getcwd
sys.path.append(getcwd())

from skinet.terminal_display import show_multi_live_help,print_color
import skinet.live_tools as live
import skinet.multi_tools as multi
from skinet.tpn_tools import fuse_nets
from skinet.runtime_tools import run_sift,treat_query
from time import sleep
import curses
from threading import Thread,Event
from queue import Queue
from importlib import import_module

import rclpy
from rclpy.node import Node
from std_msgs.msg import String, Header
import skinet_interfaces.msg as skinet_msgs

#todo : generate interfaces imports that way, so that (hopefully) it stops bugging

class SkinetMultiLiveSub(Node):

    def __init__(self):
        global status_msgs, node_names,msg_queue
        super().__init__('skinet')

        ### Structural subscribers to skillsets
        self.subs = dict()
        for robot in status_msgs.keys():
            self.subs[robot] = dict()
            for msg_type in status_msgs[robot]:
                self.subs[robot][msg_type[0]] = self.create_subscription(
                    msg_type[0],
                    node_names[robot]+msg_type[1],
                    self.make_callback(robot),
                    10) ### TODO : NE MARCHE PLUS ?!

        ### UX subscribers/publishers
        self.query_sub_ = self.create_subscription(String,'/skinet/ask',self.ask_muse,10)
        self.query_pub_ = self.create_publisher(skinet_msgs.Response,'/skinet/response',10)
        self.query_pub_timer_ = self.create_timer(0.1, self.give_answer)

        ### Logs
        self.history_pub_ = self.create_publisher(skinet_msgs.History, '/skinet/history', 10)
        self.history_timer_ = self.create_timer(0.1, self.publish_history)
        self.state_space_pub_ = self.create_publisher(skinet_msgs.StateSpace, '/skinet/state_space', 10)
        self.state_space_timer_ = self.create_timer(0.1, self.publish_state_space)

    def make_callback(self,robot):
        def listener_callback(msg):
            global msg_queue
            msg_queue.put([robot,msg])
        return listener_callback
    
    def publish_history(self):
        global fired_tr_queue, multi_rl, safety_prop
        while not fired_tr_queue.empty():
            msg = skinet_msgs.History()
            msg.header = Header()
            msg.header.stamp = self.get_clock().now().to_msg()
            msg.last_tr = fired_tr_queue.get()
            for robot in multi_rl.values():
                state = skinet_msgs.NetState()
                state.name = robot["name"]
                state.marking = robot["net"].marking
                for tr in robot["net"].firable():
                    transition = skinet_msgs.Transition()
                    transition.name = tr.name
                    transition.skillset_name = robot["name"]
                    transition.inputs = tr.inputs
                    transition.outputs = tr.outputs
                    state.firable.append(transition)
                msg.net_states.append(state)
            
            for prop in safety_prop:
                state = skinet_msgs.SafetyState()
                state.name = prop["name"]
                state.status = prop["status"]
                for tr in prop["unsafe_tr"]:
                    transition = skinet_msgs.Transition()
                    robot, tr_name = tr.split(":")
                    transition.name = tr_name
                    transition.skillset_name = robot
                    transition.inputs = multi_rl[robot]["net"].transitions[tr_name].inputs
                    transition.outputs = multi_rl[robot]["net"].transitions[tr_name].outputs
                    state.unsafe_tr.append(transition)
                msg.safety_states.append(state)
                
            self.history_pub_.publish(msg)

    def publish_state_space(self):
        global sift_msg_queue,safety_prop
        while not sift_msg_queue.empty():
            sift_msg = sift_msg_queue.get()
            msg = skinet_msgs.StateSpace()
            msg.header = Header()
            msg.header.stamp = self.get_clock().now().to_msg()
            msg.expired = False
            if "time limit expired" in sift_msg:
                msg.expired = True
                marks = sift_msg[1].split(", ")[0]
            else:
                marks = sift_msg[0].split(", ")[0]
            unsafe_markings_list = []
            for i,prop in enumerate(safety_prop):
                unsafe = skinet_msgs.UnsafeMarkings()
                unsafe.name = prop["name"]
                unsafe.size = len(sift_msg[-1][prop["name"]])
                unsafe.unsafe_markings = sift_msg[-1][prop["name"]]
                unsafe_markings_list.append(unsafe)
            msg.unsafe_markings = unsafe_markings_list
            msg.markings = int(marks[:marks.index(" marking(s)")])
            msg.time = float(sift_msg[-2][:-1])
            self.state_space_pub_.publish(msg)

    def ask_muse(self,msg):
        global last_query
        last_query = msg.data
    
    def give_answer(self):
        global last_answer_queue,multi_rl
        while not last_answer_queue.empty():
            msg = skinet_msgs.Response()
            msg.header = Header()
            msg.header.stamp = self.get_clock().now().to_msg()
            answer = last_answer_queue.get()
            msg.query = answer[0]
            tr_path = []
            for tr in answer[1]:
                transition = skinet_msgs.Transition()
                robot,tr_name = tr
                transition.name = tr_name
                transition.skillset_name = robot
                transition.inputs = multi_rl[robot]["net"].transitions[tr_name].inputs
                transition.outputs = multi_rl[robot]["net"].transitions[tr_name].outputs
                tr_path.append(transition)
            msg.path = tr_path
            self.query_pub_.publish(msg)
        

def main(args=None):

    # ------------------------ Display help ------------------------
    if sys.argv[1] == "-h" or "-h" in sys.argv[2:]:
        show_multi_live_help()

    # ------------------------ Init ------------------------
    print()
    tool.start_phase("----------------")
    tool.start_phase("SkiNet Multi Live Net Runtime Verification")
    tool.start_phase("----------------")
    print()
    robot_lang_fusion_file = sys.argv[1]
    if ".rlf" not in robot_lang_fusion_file:
        print_color("Infile must be .rlf (robot language fusion) file.","RED")
        quit()
    manual_mode = False
    block_safe = False
    calc_time = 2
    if "-safe" in sys.argv[2:]:
        block_safe = True
    if "-t" in sys.argv[2:]:
        try:
            calc_time = int(sys.argv[sys.argv.index("-t")+1])
        except:
            if ValueError:
                print_color("Error in [-t] argument: must be an int.","RED")
                quit()

    # ------------------------ Parsing .rlf file ------------------------

    global safety_prop
    rl_files,robot_names,safety_prop, check_robots, custom_nodes = multi.parse_rlf_file(robot_lang_fusion_file)

    global multi_rl
    multi_rl = multi.create_multi_dict(rl_files,robot_names,custom_nodes,check_robots)

    global status_msgs
    status_msgs = dict()

    global node_names
    node_names = dict()

    global skill_result
    skill_result = dict()
    global skill_state
    skill_state = dict()

    skill_running = dict()

    for robot in multi_rl.keys():
        name_skillset = multi_rl[robot]["skillset_name"]
        node_names[robot] = multi_rl[robot]["node"]

        ###Retrieve skillset interfaces
        status_msgs[robot] = live.init_status_msgs(name_skillset,multi_rl[robot]["skills"])
        skill_result[robot] = dict()
        skill_state[robot] = dict()
        for sk_i in multi_rl[robot]["skills"]:
            skill_result[robot][sk_i] = getattr(import_module("._skill_"+sk_i.lower()+"_response",name_skillset+"_skillset_interfaces.msg"),"Skill"+camelize(sk_i.lower())+"Response")
            skill_state[robot][sk_i] = getattr(import_module("._skill_"+sk_i.lower()+"_status",name_skillset+"_skillset_interfaces.msg"),"Skill"+camelize(sk_i.lower())+"Status")
        
        skill_running[robot] = [False for skill in multi_rl[robot]["skills"]]
    rclpy.init(args=args)

    global msg_queue
    msg_queue = Queue(maxsize=0)

    global fired_tr_queue
    fired_tr_queue = Queue(maxsize=0)

    global sift_msg_queue
    sift_msg_queue = Queue(maxsize=0)
    global last_query
    last_query = -1
    global last_answer_queue
    last_answer_queue = Queue(maxsize=0)
    global muse_childs
    muse_childs = Queue(maxsize=0)
    global forbidden_markings
    forbidden_markings = Queue(maxsize=0)

    kill_sift_thread = Event()

    path,fused_net,robot_index = fuse_nets(multi_rl,safety_prop)
    sift_thread = Thread(None, target=run_sift,args=[kill_sift_thread,path,fused_net,fired_tr_queue,sift_msg_queue,muse_childs,multi_rl,calc_time,safety_prop,robot_index,forbidden_markings])
    sift_thread.start()

    skinet_multi_live_sub = SkinetMultiLiveSub()

    # ros_thread = Thread(None,target=rclpy.spin,args=[skinet_multi_live_sub])
    # ros_thread.start()  


    # ------------------------ Start SkiNet Live window ------------------------

    tool.start_phase("Skillset Net Manual Play")

    def live_window(stdscr):
        global multi_rl, msg_queue, last_query

        stdscr = curses.initscr()
        stdscr.keypad(True)
        stdscr.clear()
        live.init_curses()
        lines, cols = stdscr.getmaxyx()

        run_live = True
        has_died = False
        fired_tr = []
        previously_fired_tr = []
        tr_path = []
        t_stock = []
        current_forbidden_marking = []
        n=0
        
        current_muse = -1
        n_muse = 0

        while muse_childs.empty():
            live.print_loading_screen(stdscr,lines,cols)
        current_muse,current_ktz = muse_childs.get()
        n_muse += 1
        stdscr.addstr(lines-8,0,"Muse updated. (#"+str(n_muse)+")",curses.color_pair(6))
        current_forbidden_marking = forbidden_markings.get()

        # line_0 = lines-7
        # max_hist = 10
        # prop_thread = Thread(None,target=multi.print_property,args=[stdscr,lines,cols,max_hist,line_0,multi_rl])
        # prop_thread.start()

        #main loop
        while run_live:
            n+=1
            stdscr.clear()
            lines, cols = stdscr.getmaxyx()

            hearbeat_on = live.print_SkiNet(stdscr,lines, cols, multi_rl, manual_mode)

            cols = cols//2
            for i in range(3,lines-8):
                stdscr.addstr(i,cols+1,"|",curses.color_pair(1))

            n_firable = 0
            multi_firable = []

            for robot_name,robot in multi_rl.items():
                #read current output of play

                enabled = robot["net"].enabled()
                firable = robot["net"].firable()

                multi_firable.append([robot_name,[n_firable,n_firable+len(firable)],firable])          

                #draw window
                live.print_firable(stdscr,lines, cols,firable,multi=True,n_tr=n_firable,name=robot_name)
                live.print_skillset_state(stdscr,lines, cols, robot["net"],robot["resources"],robot["res_states"],robot["skills"])
                #check resources
                # for res in robot["resources"]:
                #     has_died = not live.resources_ok(res_states[-2],res_mark)

                n_firable += len(firable)

            lines, cols = stdscr.getmaxyx()
            col_start = cols//2+2
            max_hist = 10
            line_0 = 4

            live.print_actions(stdscr,lines,cols,line_0,col_start)

            ### History ###
            line_0 += 6
            stdscr.addstr(line_0,col_start,"-"*(cols-col_start),curses.color_pair(1))    
            stdscr.addstr(line_0,col_start+col_start//2 - len("[history]")//2,"[history]",curses.color_pair(6))  

            if len(fired_tr) > 0:
                live.print_history_multi(stdscr,lines,cols,line_0,col_start,fired_tr,multi_rl,max_hist)

            stdscr.addstr(lines-8,0,"-"*(cols-1),curses.color_pair(1))

            ### Previous path ###
            if tr_path != []:
                stdscr.addstr(lines-7,0," "*(cols-1),curses.color_pair(1))
                stdscr.addstr(lines-7,0,"Previous path: ",curses.color_pair(6))
                stdscr.addstr(lines-7,len("Previous path: ")," > ".join([tr[0]+":"+tr[1] for tr in tr_path]),curses.color_pair(1))

            ### Safety ###
            if safety_prop != []:
                line_0 += max_hist +3
                stdscr.addstr(line_0,col_start,"-"*(cols-col_start),curses.color_pair(1))    
                stdscr.addstr(line_0,col_start+col_start//2 - len("[safety]")//2,"[safety]",curses.color_pair(6))  
                stdscr.refresh()
                multi.print_safety(stdscr,lines,cols,line_0,col_start,multi_rl,safety_prop,multi_firable,current_muse,current_forbidden_marking,check_robots,robot_index)

            if previously_fired_tr != fired_tr:
                i = len(previously_fired_tr)
                for tr in fired_tr[i:]:
                    fired_tr_queue.put(tr[1])
                    previously_fired_tr.append(tr)

            stdscr.refresh()

            tr_found=[]
            # had_reset = live.check_reset_multi(multi_rl,fired_tr)
            had_reset = False
            t_stock,had_stock,fired_tr = live.check_stock_multi(had_reset,multi_rl,t_stock,fired_tr)

            while (tr_found==[]) and (not had_reset and not had_stock):

                if not muse_childs.empty():
                    current_muse,current_ktz = muse_childs.get()
                    n_muse += 1
                    stdscr.addstr(lines-8,0,"Muse updated. (#"+str(n_muse)+")",curses.color_pair(6))
                    current_forbidden_marking = forbidden_markings.get()

                if last_query != -1:
                    tr_path = treat_query(stdscr,lines,cols,current_muse,multi_rl,current_ktz,safety_prop,last_query,current_forbidden_marking,robot_index)
                    stdscr.addstr(lines-4,0," > ".join([tr[0]+":"+tr[1] for tr in tr_path]),curses.color_pair(6))
                    last_answer_queue.put([last_query,tr_path])
                    last_query = -1

                if not msg_queue.empty(): 
                    robot, last_msg =  msg_queue.get()
                    print(robot,last_msg)
                    firable = multi_rl[robot]["net"].firable()
                    if type(last_msg) == status_msgs[robot][0][0]: #skillset status update msg

                        #resources
                        res_update = []
                        for res in last_msg._resources:
                            res_update.append(str(res._state))

                        #skill is running? if yes, start!
                        current_skill_states = []
                        for i,skill in enumerate(multi_rl[robot]["skills"]):
                            current_skill_states.append(getattr(last_msg,"skill_"+skill.lower())._state)
                            if current_skill_states[i] != skill_running[robot][i]:
                                if current_skill_states[i] == skill_state[robot][skill].RUNNING and skill_running[robot][i] == False:
                                    for tr_fr in firable:
                                        if "t__"+skill+"__start__" in tr_fr.name:
                                            tr_found.append([robot,tr_fr.name])
                                            break
                                    skill_running[robot][i] = True
                                elif current_skill_states[i] == skill_state[robot][skill].READY and skill_running[robot][i] == True:
                                    skill_running[robot][i] = False

                    elif type(last_msg) == status_msgs[robot][-2][0]: #event request update msg
                        last_event = last_msg._name
                    elif type(last_msg) == status_msgs[robot][-1][0]: #event response update msg
                        if last_msg._response == last_msg.SUCCESS:
                            for tr_fr in firable:
                                if "t__"+last_event+"__" in tr_fr.name:
                                    tr_found.append([robot,tr_fr.name])
                                    break

                    else: #skill response update msg
                        result = live.get_results(last_msg,status_msgs[robot],skill_result[robot],multi_rl[robot]["skills"])
                        stdscr.addstr(lines-1,0,str(result),curses.color_pair(1))
                        if result[1] == True:
                            for tr_fr in firable:
                                if "t__"+result[0] in tr_fr.name:
                                    tr_found.append([robot,tr_fr.name])
                                    break
                                elif "t__"+result[0] not in tr_fr.name and tr_fr == firable[-1]:
                                    if "__pre_fail__" not in result[0]: #precondition failures are considered junk if not firable immediately
                                        t_stock.append([robot,"t__"+result[0]])
                        else:
                            if "__interrupt__" in result[0]: #interrupt happened from GUI but was not defined in skillset
                                pass #TO_DO when marking update is developed

                else:
                    sleep(0.1) #checks for updates at 10Hz.
                    live.update_heartbeat(hearbeat_on,stdscr,lines,cols,name_skillset,n,has_died)
                    n+=1

            if tr_found != []:
                stdscr.refresh()
                multi_rl[tr_found[0][0]]["net"].fire(tr_found[0][1])

            if tr_found != []:
                if type(tr_found[0]) == list :
                    for t in tr_found:
                        fired_tr.append(t)
                else:
                    fired_tr.append(tr_found)


            stdscr.refresh()

        current_muse.close()

    curses.wrapper(live_window)

    kill_sift_thread.set()
    sift_thread.join()

    curses.nocbreak()
    curses.echo()
    curses.endwin()

    skinet_multi_live_sub.destroy_node()
    rclpy.shutdown()
    


if __name__ == '__main__':
    main()
