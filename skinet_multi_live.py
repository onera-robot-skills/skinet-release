from robot_language import *
from skinet.terminal_display import show_multi_live_help, print_color
from skinet.tpn_tools import fuse_nets
from skinet.runtime_tools import run_sift, treat_query
import skinet.live_tools as live
import skinet.multi_tools as multi
from sys import argv, exit
from time import sleep
from threading import Thread, Event
from queue import Queue
import curses


def main():

    # ------------------------ Display help ------------------------
    if argv[1] == "-h" or "-h" in argv[2:]:
        show_multi_live_help()

    # ------------------------ Init ------------------------
    print()
    tool.start_phase("----------------")
    tool.start_phase("SkiNet Multi Live Net Runtime Verification")
    tool.start_phase("MANUAL MODE")
    tool.start_phase("----------------")
    print()
    robot_lang_fusion_file = argv[1]
    if ".rlf" not in robot_lang_fusion_file:
        print_color("Infile must be .rlf (robot language fusion) file.", "RED")
        quit()
    manual_mode = True
    block_safe = False
    global display_title
    display_title = False
    calc_time = 2
    if "-safe" in argv[2:]:
        block_safe = True
    if "-t" in argv[2:]:
        try:
            calc_time = int(argv[argv.index("-t")+1])
        except:
            if ValueError:
                print_color("Error in [-t] argument: must be an int.", "RED")
                quit()
    if "--title" in argv[2:]:
        display_title = True

    # ------------------------ Parsing .rlf file ------------------------

    rl_files, robot_names, safety_prop, check_robots, node_names = multi.parse_rlf_file(
        robot_lang_fusion_file)

    global multi_rl
    multi_rl = multi.create_multi_dict(
        rl_files, robot_names, node_names, check_robots)

    global fired_tr_queue
    fired_tr_queue = Queue(maxsize=0)
    global sift_msg_queue
    sift_msg_queue = Queue(maxsize=0)
    global last_query
    last_query = -1
    global muse_childs
    muse_childs = Queue(maxsize=0)
    global forbidden_markings
    forbidden_markings = Queue(maxsize=0)

    kill_sift_thread = Event()

    path, fused_net, robot_index = fuse_nets(multi_rl, safety_prop)
    sift_thread = Thread(None, target=run_sift, args=[kill_sift_thread, path, fused_net, fired_tr_queue,
                         sift_msg_queue, muse_childs, multi_rl, calc_time, safety_prop, robot_index, forbidden_markings])
    sift_thread.start()

    # ------------------------ Start SkiNet Live window ------------------------

    tool.start_phase("Skillset Net Manual Play")

    def live_window(stdscr):
        global multi_rl, run_live, display_title

        stdscr = curses.initscr()
        stdscr.keypad(True)
        stdscr.clear()
        live.init_curses()
        lines, cols = stdscr.getmaxyx()

        run_live = True
        has_died = False
        fired_tr = []
        previously_fired_tr = []
        tr_path = []
        t_stock = []
        current_forbidden_marking = []
        n = 0

        current_muse = -1
        n_muse = 0

        n_title = 0
        while muse_childs.empty() or display_title:
            live.print_loading_screen(
                stdscr, lines, cols, display_title=display_title, n=n_title)
            try:
                n_title += 1
                sleep(0.1)
            except:
                if KeyboardInterrupt:
                    display_title = False
        current_muse, current_ktz = muse_childs.get()
        n_muse += 1
        stdscr.addstr(lines-8, 0, "Muse updated. (#" +
                      str(n_muse)+")", curses.color_pair(6))
        current_forbidden_marking = forbidden_markings.get()

        # main loop
        while run_live:
            try:
                n += 1
                stdscr.clear()
                lines, cols = stdscr.getmaxyx()

                live.print_SkiNet(stdscr, lines, cols, multi_rl, manual_mode)

                cols = cols//2
                for i in range(3, lines-8):
                    stdscr.addstr(i, cols+1, "|", curses.color_pair(1))

                n_firable = 0
                multi_firable = []

                for robot_name, robot in multi_rl.items():
                    # read current output of play

                    firable = robot["net"].firable()

                    firable = [tr for tr in firable if (
                        "__pre_fail__" not in tr.name and "__val_fail" not in tr.name)]

                    multi_firable.append(
                        [robot_name, [n_firable, n_firable+len(firable)], firable])

                    # draw window
                    live.print_firable(
                        stdscr, lines, cols, firable, multi=True, n_tr=n_firable, name=robot_name)
                    live.print_skillset_state(
                        stdscr, lines, cols, robot["net"], robot["resources"], robot["res_states"], robot["skills"])

                    n_firable += len(firable)

                lines, cols = stdscr.getmaxyx()
                col_start = cols//2+2
                max_hist = 6
                line_0 = 4

                live.print_actions(stdscr, lines, cols, line_0, col_start)

                ### History ###
                line_0 += 5
                stdscr.addstr(line_0, col_start, "-" *
                              (cols-col_start), curses.color_pair(1))
                stdscr.addstr(line_0, col_start+col_start//2 -
                              len("[history]")//2, "[history]", curses.color_pair(6))

                if len(fired_tr) > 0:
                    live.print_history_multi(
                        stdscr, lines, cols, line_0, col_start, fired_tr, multi_rl, max_hist)

                stdscr.addstr(lines-8, 0, "-"*(cols-1), curses.color_pair(1))

                ### Previous path ###
                if tr_path != []:
                    stdscr.addstr(lines-7, 0, " "*(cols-1),
                                  curses.color_pair(1))
                    stdscr.addstr(lines-7, 0, "Previous path: ",
                                  curses.color_pair(6))
                    stdscr.addstr(lines-7, len("Previous path: "), " > ".join(
                        [tr[0]+":"+tr[1] for tr in tr_path]), curses.color_pair(1))

                stdscr.refresh()

                ### Safety ###
                if safety_prop != []:
                    line_0 += max_hist + 1
                    stdscr.addstr(line_0, col_start, "-" *
                                  (cols-col_start), curses.color_pair(1))
                    stdscr.addstr(line_0, col_start+col_start//2 -
                                  len("[safety]")//2, "[safety]", curses.color_pair(6))

                    multi.print_safety(stdscr, lines, cols, line_0, col_start, multi_rl, safety_prop,
                                       multi_firable, current_muse, current_forbidden_marking, check_robots, robot_index)

                if previously_fired_tr != fired_tr:
                    i = len(previously_fired_tr)
                    for tr in fired_tr[i:]:
                        fired_tr_queue.put(tr[1])
                        previously_fired_tr.append(tr)

                stdscr.refresh()

                tr_found = []

                if not muse_childs.empty():
                    last_tr = fired_tr_queue.get()
                    current_muse, current_ktz = muse_childs.get()
                    n_muse += 1
                    stdscr.addstr(lines-8, 0, "Muse updated. (#" +
                                  str(n_muse)+")", curses.color_pair(6))
                    current_forbidden_marking = forbidden_markings.get()

                ## Get property ###
                last_query = multi.check_property(
                    stdscr, lines, cols, multi_rl)

                if last_query != -1:
                    tr_path = treat_query(stdscr, lines, cols, current_muse, multi_rl, current_ktz,
                                          safety_prop, last_query, current_forbidden_marking, robot_index)
                    stdscr.addstr(
                        lines-6, 0, " > ".join([tr[0]+":"+tr[1] for tr in tr_path]), curses.color_pair(6))
                    last_query = -1
                stdscr.refresh()

                ### Get user transitions ###
                run_live, tr_found = live.do_user_input_multi(
                    stdscr, lines, cols, multi_rl, multi_firable, tr_path, block_safe, safety_prop, current_muse, current_forbidden_marking, check_robots, robot_index)

                if tr_found != []:
                    if type(tr_found[0]) == list:
                        for t in tr_found:
                            fired_tr.append(t)
                    else:
                        fired_tr.append(tr_found)

                stdscr.refresh()

            except:
                if KeyboardInterrupt:
                    run_live = False

        curses.nocbreak()
        curses.echo()
        curses.endwin()
        print("Closing cleanly...")
        current_muse.sendline("quit")
        current_muse.close()
        kill_sift_thread.set()
        sift_thread.join()
        print("Bye!")
        quit()

    curses.wrapper(live_window)


if __name__ == '__main__':
    main()
    exit()
