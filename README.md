# SkiNet - Skillset Petri Net Generation & Verification

Welcome to SkiNet !
 
Prerequisite packages: 
- Robot_language plugin & its dependencies.
    - refer to: https://gitlab.com/onera-robot-skills/robot_language
- Tina toolbox (LAAS, CNRS)
    - https://projects.laas.fr/tina/index.php
    - don't forget to export the /bin to your .bashrc: 
    ```
    export PATH=$PATH:/home/(username)/tina-3.7.0/bin
    ```

- Related articles :
    - SkiNet : https://arxiv.org/abs/2209.14039 (FMAS proceedings, 2022)
    - SkiNet Live : (to be published in ICRA proceedings, 2023)

# Dependencies

```
pip3 install jinja2 inflection typing argparse z3-solver antlr4-python3-runtime

```

# Using SkiNet

## Installation

```
git clone https://gitlab.com/onera-robot-skills/skinet-release.git
cd skinet_release
pip install .
```

## General help

Obtain general command arguments with:
```
python3 -m skinet -h
```

## Getting a .rl skillset model to work with

A few examples are available in the /examples folder. Check them out to get started.
Skinet by running the robot_language tool parsing and verification. The verbosity is set to only show the verification step warnings.
You may see to check and solve those before moving on to net verification, as they could lead to unexpected behaviors in your robot, even if the net is safe.


## Generate the Skillset Petri net from a skillset model

For simple net generation:
```
python3 -m skinet (.rl file name/location)
```
Example with the spot_easy.rl file:
```
python3 -m skinet examples/spot_easy.rl
```
This command generates a .net and pretty .ndr file. These files can be used with the Tina toolbox like regular net files.
Generated nets are found here:
    ~/.skinet_nets/(name of skillset)_skinet/
 
## Skillset Verification

Before getting into specific missions to test your skillset, it is important to go over basic health properties of the generated Petri net so that no unexpected behaviors arise.


Solve LTL/CTL analysis of the skillset with Selt/Muse (add --no-ktz if you wish to skip the state-space generation):
```
python3 -m skinet examples/spot_easy.rl -a
```
Temporal Logic solving with the Selt/Muse tool. The CTL operators are available in 'skinet/skinet.mmc'.
Basic check involves: ('all' for all checks, or simply press Enter)
    - Net deadlock ('dead')
    - Transition liveness ('live')
    - Invariants and 1-safeness ('safe')
    - Skills can be activated from any point in the marking graph, individual check ('skill')
    - Skillset deadlocks: states where no skill can be activated anymore ('deadset')
    - 'quit' to quit!

# SkiNet Play - Play skillset nets to test your skillset model.

You can quickly play around with your skillset using the following script in the repo:
```
python3 -m skinet (.rl file name/location) -play
```
Example:
```
python3 -m skinet spot.rl -play
```
The marking and available firable transitions will be shown. Fire them by typing the associated integer.
You can undo your firing by typing a negative integer. The player stops if it reaches a deadlock.

# SkiNet Verif - Query custom LTL/CTL property to skillset net.

To verify a custom LTL/CTL property on a skillset model net using Tina Selt/Muse, you can use the following program:
```
python3 -m skinet (.rl file name/location) -vltl/-vctl "property"
```
Example:
```
python3 -m skinet spot.rl -vctl "AG -(spot_status__Standing \/ spot_status__Sitting)"
python3 -m skinet spot.rl -vctl "AG (spot_status__Standing \/ spot_status__Sitting)"
```
First example returns an empty set, as no marking has both states of spot_status marked.
Second example returns the opposite.

The naming conventions used (v0.2) are as follows (if the result is '-1', then you have a typing error):
    - skill state      : skill.idle, skill.executing
    - resource state   : res_name.res_state
    - event transition : event_name
    - skill transition : skill.start.x, skill.result.result_name.x


# SkiNet Live - Runtime monitoring - Manual Mode

SkiNet Live is an extension of SkiNet that allows for a more ergonomic testing of the Petri Net, and a live player that will fire transitions automatically when ran parallely with the skillset manager, by reading the ROS topics and messages.
Here, you will find instructions for the Manual mode of SkiNet Live (does not use ROS or skillset managers).

## General help
Obtain general help and command arguments with:
```
python3 skinet_multi_live.py -h
```

To use, simply run skinet_multi_live.py with the desired .rlf (robot-language fusion) multi-skillset file:
```
python3 skinet_multi_live.py (.rlf file name/location)
```
Example with the .rl file:
```
python3 skinet_multi_live.py examples/multi/system.rlf 
```
Warning1: Change the .rl skillset model file paths in the .rlf file with a text editor to suit your system paths.

Warning2: If you get an error here: "_curses.error: addwstr() returned ERR" It means your console is not large enough to display everything. Simply put it in fullscreen, and/or reduce the font size, so that everything can fit.

From there, your terminal will run the Petri Net play tool, and you will be able to fire transitions as you please while the status of resources and skills is updated.
If the user interface doesn't open and you get a curses.error, open the terminal in full screen size.

You can ask questions to SkiNet by using the property prompt at the bottom, else you press Enter to type desired transitions. Instructions on how to write them are in the top right corner of the UI, under the "actions" section. Typing Enter will send the question and return some results.

You can fire transitions by typing their number, as displayed in the left section of the UI (Firable:...) in each skillset status area.

# SkiNet Live - Runtime monitoring - Live Mode using ROS2

To run the live runtime verification package to monitor the ROS2 skillset managers, you simply go to the SkiNet directory and run:
```
colcon build
source install/setup.bash
ros2 run skinet_multi_live skinet_multi_live.py (rlf file) 
```
And watch the UI update as your robots roam around!


# Thank you for using SkiNet !!!

If you find any bugs or unexpected behaviors in the nets (such as more than one token in a place !!!).
Feel free to send me your skillset models if you want them to be added to the examples. This is warmly welcome, especially if they are problematic, since it will allows to test the robustness of SkiNet to finding problems ;) !

Have fun!!
