#!/usr/bin/env python3

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='skinet',
    version='0.3',
    description='SkiNet: Skillset Petri Net verification tool for Robot-Language',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Baptiste Pelletier',
    author_email='baptiste_pelletier_jv71@yahoo.fr',
    url='https://gitlab.com/onera-robot-skills/skinet-release',
    packages=[
        'skinet'
    ],
    package_dir={
        'skinet': 'skinet'
    },
    package_data={'skinet': ['skinet.mmc']},
    classifiers=[
        "Programming·Language·::·Python·::·3",
        "License :: CeCILL-B Free Software License Agreement (CECILL-B)",
        "Operating·System·::·OS·Independent"
    ],
    install_requires=["robot_language"],
    python_requires=">=3.10",
)
