from robot_language import *

import skinet.tina_tools as tina_tools
import skinet.net_def as net_def
from skinet.skillset_net import SkillsetNet
from skinet.tpn_tools import labelize_ndr, eventless_ndr
from skinet.net_typing import userstr_to_property, place_to_userstr, trname_to_userstr

import os
import argparse


def read_args():
    """
    Parse command line arguments.
    """

    parser = argparse.ArgumentParser(
        prog='python -m skinet',
        description='SkiNet: Petri net verification tool for Robot-Language.')
    parser.add_argument('file', type=str, help='model file')

    # analysis
    main_group = parser.add_argument_group(
        description="--- Main analysis commands ---"
    )
    main_group.add_argument('-t', '--tina-txt', required=False,
                            action='store_true', help='exports Tina Petri net analysis as txt')
    main_group.add_argument('-nd', '--net-draw', required=False,
                            action='store_true', help='print the generated net in the ND Petri net visualization tool')
    main_group.add_argument('-a', '--analysis', required=False,
                            action='store_true', help='LTL/CTL analysis through Selt/Muse')
    main_group.add_argument('-s', '--struct', required=False,
                            action='store_true', help='structural analysis to check net place invariants.')
    main_group.add_argument('-play', '--play', required=False,
                            action='store_true', help='play the skillset model via the generated Petri net')

    # ltl/ctl verif
    verif_group = parser.add_argument_group(
        description="--- Verify user properties ---"
    )
    verif_group.add_argument('-vltl', '--verif-ltl', required=False,
                             type=str,
                             metavar='"property"',
                             help='verify a property using LTL (with Selt)')
    verif_group.add_argument('-vctl', '--verif-ctl', required=False,
                             type=str,
                             metavar='"property"',
                             help='verify a property using CTL (with Muse)')
    verif_group.add_argument('-mmc', '--mmc', required=False,
                             action='store_true', help='create a CTL formulae file with predefined operators')

    # options
    option_group = parser.add_argument_group(
        description="--- Other options ---"
    )
    option_group.add_argument('-ne', '--no-events', required=False,
                              action='store_true', help='disable skillset events')
    option_group.add_argument('-nk', '--no-ktz', required=False,
                              action='store_true', help='skip state-space generation (uses previous, in Kripke transition system "ktz" file)')
    option_group.add_argument('-ex', '--exits', required=False,
                              action='store_true', help='adds skill exit places to the net')
    option_group.add_argument('-l', '--label', required=False,
                              action='store_true', help='adds labels to places for net fusion')

    skinet_args = parser.parse_args()

    return skinet_args


def run_options(skinet_args, net_file, path, Skillset):
    """
    Run end options after generating the net.
    """
    res_places = []
    res_names = []
    resources = []
    resources_net = []

    for res in Skillset.resources:
        states = list(res.states)
        res_names.append(res.name)
        resources.append([res.name, states])
        init_mark = [0 for i in range(len(states))]
        init_mark[states.index(res.initial)] = 1
        resources_net.append(init_mark)
    for res in resources:
        res_place = []
        for state in res[1]:
            res_place.append(res[0]+"__"+state)
        res_places.append(res_place)
    skillnames = [sk.name for sk in Skillset.skills]
    eventnames = [evt.name for evt in Skillset.events]
    net_marking_init = []
    for res in resources:
        net_marking_init.append(
            res[0]+"__"+net_def.res_token(res, resources, resources_net))

    exits = []
    if skinet_args.exits:
        exits, skillnames = net_def.get_exits(net_file, Skillset.skills)
    
    tina_tools.ndr_make(net_file)

    if skinet_args.net_draw == True:
        # convert net to pretty ndr
        tool.start_phase("Prettify " + Skillset.name +
                         " Net...")
        tina_tools.ndr_clean(net_file, res_places,
                             net_marking_init, skillnames, exits, eventnames)

        # open ndr
        if skinet_args.label:
            tool.start_phase("Opening " + Skillset.name +
                             " Skillset Labelled Net...")
            net_location = path+"/"+Skillset.name
            if skinet_args.exits:
                net_location += "_exits"
            labelize_ndr(net_location, skillnames, eventnames,
                         empty=True, no_event=False, no_reset=True)
            eventless_ndr(net_file, skillnames, eventnames)
            labelize_ndr(net_location+"_eventless", skillnames,
                         eventnames, empty=True, no_event=True, no_reset=True)
            tina_tools.ndr_open(net_location+"_labelled")
        else:
            tool.start_phase("Opening " + Skillset.name + " Skillset Net...")
            tina_tools.ndr_open(net_file)

    if skinet_args.tina_txt:
        tool.start_phase("Generating State Space Analysis (Tina) ")
        tina_tools.Tina_check(net_file)

    if skinet_args.struct:
        tool.start_phase("Generating Structural Analysis (Struct)")
        tina_tools.Struct_check(
            net_file, Skillset, res_names, res_places, exits, skillnames)

    if skinet_args.analysis:
        if not skinet_args.no_ktz:
            tool.start_phase("Generating Kripke Transition System (.ktz)")
            print("Normal Net")
            tina_tools.ktz_make(net_file, do_print=True)
            print("Eventless Net")
            eventless_ndr(net_file, skillnames, eventnames)
            tina_tools.ktz_make(net_file+"_eventless", do_print=True)

        tool.start_phase("LTL/CTL Analysis (Selt/Muse)")
        tina_tools.Analysis_check(
            net_file, resources, exits, skillnames, eventnames)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def pretty_print_action(index: int, action: dict):
    if action["type"] == "skill":
        if action["action"] == "inv_fail":
            print(
                "{bcolors.HEADER}> {:^3}{bcolors.ENDC}: {bcolors.FAIL}{}{bcolors.ENDC}"
                .format(str(index), action["name"]+"."+action["action"] + ("."+action["suffix"] if action["suffix"] != "" else ""), bcolors=bcolors))
        else:
            print(
                "{bcolors.HEADER}> {:^3}{bcolors.ENDC}: {}"
                .format(str(index), action["name"]+"."+action["action"] + ("."+action["suffix"] if action["suffix"] != "" else ""), bcolors=bcolors))
    if action["type"] == "event":
        print(
            "{bcolors.HEADER}> {:^3}{bcolors.ENDC}: {bcolors.WARNING}triggered {}{bcolors.ENDC}"
            .format(str(index), action["name"], bcolors=bcolors))
        
def skinet_verif(skinet_args):
    """
    Run verification on the generated state-space.
    """
    # creates a SkillsetNet instance straight away like this!
    skillset_net = SkillsetNet(rl_model=skinet_args.file)
    print("Skillset: "+skillset_net.name)
    if skinet_args.no_events:
        print(" - no events -")
        skillset_net.remove_event()
    # LTL
    if skinet_args.verif_ltl != None:
        print("Target logic : LTL")
        print("Property     : "+skinet_args.verif_ltl)
        request = userstr_to_property(skinet_args.verif_ltl, skillset_net)
        skillset_net.start_selt_child()
        result = skillset_net.ask_selt(request,debug=True)
        print("Result: " +
              str(result))
        if result == -1:
            skillset_net.ask_selt(request, debug=True)

    # CTL
    if skinet_args.verif_ctl != None:
        print("Target logic : CTL")
        print("Property     : "+skinet_args.verif_ctl)
        request = userstr_to_property(skinet_args.verif_ctl, skillset_net)
        print(request)
        skillset_net.start_muse_child()
        # switch depending on desired result format
        output_format = "set"  # set bool card
        skillset_net.muse_child.sendline("output "+output_format+";")
        result = skillset_net.ask_muse(request)
        if result == -1:
            skillset_net.ask_muse(request, debug=True)
        else:
            prompt_user(result,skillset_net)

def prompt_user(result,skillset_net):
    output_format = "set"  # set bool card
    # prompt state to check
    if result != []:
        print(
            "{bcolors.HEADER}- Analysis -{bcolors.ENDC}".format(bcolors=bcolors))
        show_marking = False
        show_full = False
        choice = None
        while True:
            result_str = str(result) if len(
                result) < 20 else str(result[:20])[:-1]+",...]"
            print("Result ("+output_format+"): " +
                result_str)
            
            choice = input(
                "Play a result[enter state number, q or press enter to stop, m to show/hide markings, f to show/hide full trace]:")
            if choice == "q" or choice == "":
                return
            elif choice == "m":
                show_marking = not show_marking
                print(
                    " - marking display: "+show_marking)
                print()
            elif choice == "f":
                show_full = not show_full
                print(
                    " - full trace: "+show_full)
                print()
            elif int(choice) in result:
                path_to_state = skillset_net.get_path(
                    int(choice), 0)
                dodge = 0 if show_full else 5
                length = len(path_to_state.keys())
                printed_dots = False
                for i, tr in path_to_state.items():
                    if i > length-1-dodge or i < dodge:
                        pretty_print_action(i, tr["action"])
                        skillset_net.fire(tr["action"]["transition"])
                        if show_marking:
                            print_places = []
                            for place in skillset_net.marking:
                                if net_def.PLACE_SUFFIX.IDLE in place or net_def.PLACE_SUFFIX.REQUEST_START in place:
                                    # places we don't want shown
                                    pass
                                else:
                                    print_places.append(
                                        place_to_userstr(place))
                            if i == int(choice):
                                print("  marking: ".format(),
                                    ", ".join(print_places))
                            else:
                                print("  marking: ".format(),
                                    ", ".join(print_places))
                            print()
                    else:
                        if not printed_dots:
                            print("    ...")
                            printed_dots = True
                firable = [trname_to_userstr(tr.name) for tr in skillset_net.firable()]
                if any("inv_fail" in tr for tr in firable):
                    print("{bcolors.BOLD}Invariant failure found at state{bcolors.ENDC}: ".format(bcolors=bcolors)
                            +", ".join([trname_to_userstr(tr.name) for tr in skillset_net.firable()]))
    else:
        print("Result ("+output_format+"): " +
            str(result))



def skinet_play(skinet_args):
    """
    Play the execution of skillset model via the generated net.
    """
    # creates a SkillsetNet instance straight away like this!
    skillset_net = SkillsetNet(rl_model=skinet_args.file)
    skillset_net.play_skillset(auto=False)


def main():
    # ------------------------ Init ------------------------
    print()
    tool.start_phase("----------------")
    tool.start_phase("SkiNet: Skillset Net Generation and Verification")
    tool.start_phase("----------------")
    print()
    skinet_args = read_args()

    # bypass long net generation if just doing verif or play
    if skinet_args.verif_ltl != None or skinet_args.verif_ctl != None:
        skinet_verif(skinet_args)
        quit()

    if skinet_args.play:
        skinet_play(skinet_args)
        quit()

    # ------------------------ RL Skillset Parsing & Verif ------------------------
    tool.set_verbosity(2)  # avoids RL spamming messags
    tool.start_phase("Parsing")
    tree, model = parse_file(skinet_args.file)
    # Resolve
    if tool.is_ok:
        tool.start_phase("Resolve")
        model.resolve()
    # -- Uncomment this to display RL verif
    # Check
    # if tool.is_ok:
    #     tool.start_phase("Check")
    #     model.check()
    # # Verification
    # tool.set_verbosity(2)
    # if tool.is_ok:
    #     tool.start_phase("Verification")
    #     verify_model(model)

    name = model.skillsets
    Skillset = model.skillsets[0]
    name_skillset = str(name[0])
    # Create resources
    resources = []
    resources_net = []
    for res in Skillset.resources:
        states = list(res.states)
        resources.append([res.name, states])
        init_mark = [0 for i in range(len(states))]
        init_mark[states.index(res.initial)] = 1
        resources_net.append(init_mark)
    skillnames = [sk.name for sk in Skillset.skills]
    eventnames = [evt.name for evt in Skillset.events]

    # Check and create path
    path = os.path.expanduser("~")+"/.skinet_nets/"
    if not os.path.exists(path):
        os.makedirs(path)
    path += name_skillset+"_skinet"
    if not os.path.exists(path):
        os.makedirs(path)
    else:
        print("Path " + path +
              " already exists, files will be overwritten.") if not skinet_args.mmc else 0

    if skinet_args.mmc:
        tool.start_phase("Generating CTL Formulae File")
        tina_tools.create_formulae_mmc(path, skillnames)
        quit()

    # ------------------------ Net generation ------------------------

    # Create net file
    net_file = path + "/" + name_skillset
    if skinet_args.exits:
        net_file += "_exits"
    net = open(net_file + ".net", "w")
    net.write("net " + name_skillset + "\n")
    tool.start_phase("Creating " + name_skillset + " Skillset Net...")

    # Resources places
    # Create resource states places
    net_places = []
    res_places = []
    res_names = []
    for res in Skillset.resources:
        res_names.append(res.name)
    for res in resources:
        res_place = []
        for state in res[1]:
            net_places.append(res[0]+"__"+state)
            res_place.append(res[0]+"__"+state)
        res_places.append(res_place)

    # Get initial marking of resources
    net_marking_init = []
    for res in resources:
        net_marking_init.append(
            res[0]+"__"+net_def.res_token(res, resources, resources_net))
    # Add to net file
    for place in net_places:
        if place in net_marking_init:
            net_def.add_p(net, place, 1)
        else:
            net_def.add_p(net, place, 0)
    # Events transitions
    t_events = net_def.write_events_transit(
        net, model, mark_request=not skinet_args.no_events)

    if skinet_args.exits:
        net_def.create_skill_net(net, model, t_events)
        exits, skillnames = net_def.get_exits(net_file, Skillset.skills)
    else:
        net_def.create_skill_net_exitless(net, model, t_events)
        exits = []

    net.close()

    # ------------------------ Net analysis tools ------------------------

    run_options(skinet_args, net_file, path, Skillset)


if __name__ == '__main__':
    main()
