import sys
from time import time
from skinet.terminal_display import print_color

def flatten(xss):
    return [x for xs in xss for x in xs]

def cut_skill_options(skill,string : str):
    try:
        i_start = string.index("[")
        i_end = string.index("]")
        string = string[i_start+1:i_end].split(",")
        if len(string) == 1:
            exit_type = string[0]
            return ["sk",skill,skill+"_"+exit_type,exit_type,i_end-i_start]
        else:
            exit_type = string[0]
            exit_name = string[1]
            return ["sk",skill,skill+"_"+exit_type+"_"+exit_name,exit_type,i_end-i_start]
    except:
        if ValueError:
            print_color("Typing error!","RED")
            print_color("Ill-typed skill: "+skill,"YELLOW")
            print("Expected format: skill[exit_type,exit_name].")
            quit()
    


def parse_mission(mission, dict):
    mission = mission.replace(" ", "")
    mission = mission.replace(" ", "\n")
    c = 0
    mission_sequence = []
    nb_blocks_check = 0
    nb_blocks = 0
    skillnames,resnames,eventnames,compo,seqnames = dict
    dict = flatten(dict)

    while c < len(mission):
        c1 = c
        word = ""
        not_a_word = True
        while not_a_word:
            word = mission[c : c1 + 1]
            if word in dict:
                if word in skillnames:
                    skill = word
                    word = cut_skill_options(skill,mission[c:])
                    mission_sequence.append(word)
                    not_a_word = False
                    c = c1 + word[4] +1
                elif word in eventnames:
                    mission_sequence.append(["ev",word])
                    not_a_word = False
                    c = c1
                elif word in compo:
                    mission_sequence.append(["comp",word])
                    not_a_word = False
                    c = c1
                elif word in seqnames:
                    mission_sequence.append(["seq",word])
                    not_a_word = False
                    c = c1
            if word == "(":
                nb_blocks_check += 1
                nb_blocks += 1
            elif word == ")":
                nb_blocks_check -= 1
                if nb_blocks_check < 0:
                    print_color("Error while parsing: found orphan ')'.","YELLOW")
                    quit()
            c1 += 1
            if c1 > len(mission) and not_a_word:
                print_color(
                    "Error while parsing: could not parse. Did you mispell something?","YELLOW"
                )
                print("Last parsing attempt: ", word)
                quit()
        c += 1
    if nb_blocks_check != 0:
        print_color("Error while parsing: missing closing ')'.","YELLOW")
        quit()
    
    return mission_sequence, nb_blocks


def get_blocks(mission_sequence):
    current_sequence = []
    blocks = []
    nb_blocks_check = 0
    loc_blocks_starts = []
    loc_blocks_ends = []
    loc_root_blocks_in_mission = []
    lifo_block = []
    nb_step = 0
    loc_root_blocks = []
    mission_index = 0
    ### Looking for blocks
    while nb_step < len(mission_sequence):
        step = mission_sequence[nb_step]
        if step == "(":
            if nb_blocks_check == 0:
                loc_root_blocks.append(nb_step + 1)
                loc_root_blocks_in_mission.append(mission_index)
                mission_index += 1
            nb_blocks_check += 1
            lifo_block.append(nb_step + 1)
        elif step == ")":
            loc_blocks_starts.append(lifo_block.pop(-1))
            loc_blocks_ends.append(nb_step)
            blocks.append(mission_sequence[loc_blocks_starts[-1] : loc_blocks_ends[-1]])
            nb_blocks_check -= 1
        elif nb_blocks_check == 0:
            current_sequence.append(step)
            mission_index += 1
        nb_step += 1
    for nb, index in enumerate(loc_root_blocks):
        ### Start recursive block search
        loc, block = loc_root_blocks_in_mission[nb], get_blocks(
            blocks[loc_blocks_starts.index(index)]
        )
        current_sequence.insert(loc, block)

    return current_sequence


def read_rlm_file(file):
    with open(file, "r") as mission_file:
        lines = mission_file.readlines()
    text = ""
    for line in lines:
        if "//" in line:
            text += line[: line.index("//")]
        else:
            text += line
    text = "".join(text)
    list = text.split()
    nb_sequences = list.count("Sequence")
    nb_missions = list.count("Mission")
    if nb_missions == 0:
        print("Warning: no mission found in .rlm file.")
    else:
        missions = []
        c = 0
        for i in range(nb_missions):
            while list[c] != "Mission":
                c += 1
            c += 1
            mission_name = list[c]
            c2 = c + 1
            while list[c2] != "}":
                c2 += 1
            missions.append([mission_name, "".join(list[c + 2 : c2])])
    c = 0
    sequences = []
    for i in range(nb_sequences):
        while list[c] != "Sequence":
            c += 1
        c += 1
        sequence_name = list[c]
        c2 = c + 1
        while list[c2] != "}":
            c2 += 1
        sequences.append([sequence_name, "".join(list[c + 2 : c2])])
    return missions, sequences


def create_net(
    net,
    mission_sequence,
    dict,
    compo,
    previous_place,
    current_action,
    origin_place,
    is_retry,
    coords,
    numbers,
):
    x, y, x_jump, y_jump = coords
    i_step = 0
    for i, step in enumerate(mission_sequence):
        if step in dict and current_action == "->":
            numbers[dict.index(step)] += 1
            if numbers[dict.index(step)] > 0:
                step += "_" + str(numbers[dict.index(step)])
            x += x_jump
            i_step += 1
            if type(previous_place) != list:
                net.write(
                    "t " + str(x) + " " + str(y) + " t_" + step + " n 0 w n {} ne\n"
                )
                net.write("e " + previous_place + " t_" + step + " 1 n\n")
                x += x_jump
                net.write("p " + str(x) + " " + str(y) + " " + step + " 0 n {} ne\n")
                net.write("e " + "t__" + step + " " + step + " 1 n\n")
                if is_retry:
                    net.write(
                        "t "
                        + str(x - x_jump)
                        + " "
                        + str(y + y_jump)
                        + " t_fail_"
                        + step
                        + " s 0 w n {} ne\n"
                    )
                    # x += x_jump
                    net.write("e " + "t_fail_" + step + " " + origin_place + " 1 n\n")
                    net.write("e " + previous_place + " t_fail_" + step + " 1 n\n")
            else:
                if previous_place[0] == "+" or previous_place[0] == "_ite_":
                    net.write(
                        "p "
                        + str(x + x_jump)
                        + " "
                        + str(y)
                        + " "
                        + step
                        + " 0 n {} ne\n"
                    )
                    for n, pr_pl_i in enumerate(previous_place[1]):
                        x_i = x
                        y_i = y
                        net.write(
                            "t "
                            + str(x_i)
                            + " "
                            + str(y_i + n * y_jump)
                            + " t_"
                            + step
                            + "_"
                            + str(n)
                            + " n 0 w n {} ne\n"
                        )
                        net.write(
                            "e " + pr_pl_i + " t_" + step + "_" + str(n) + " 1 n\n"
                        )
                        x_i += x_jump
                        net.write(
                            "e " + "t__" + step + "_" + str(n) + " " + step + " 1 n\n"
                        )
                        if is_retry:
                            net.write(
                                "t "
                                + str(x_i - x_jump)
                                + " "
                                + str(y_i + y_jump + n / 2 * y_jump)
                                + " t_fail_"
                                + step
                                + "_"
                                + str(n)
                                + " s 0 w n {} ne\n"
                            )
                            net.write(
                                "e "
                                + "t_fail_"
                                + step
                                + "_"
                                + str(n)
                                + " "
                                + origin_place
                                + " 1 n\n"
                            )
                            net.write(
                                "e "
                                + pr_pl_i
                                + "t_fail_"
                                + step
                                + "_"
                                + str(n)
                                + " 1 n\n"
                            )
                    x += x_jump
                elif previous_place[0] == "|":
                    net.write(
                        "p "
                        + str(x + x_jump)
                        + " "
                        + str(y)
                        + " "
                        + step
                        + " 0 n {} ne\n"
                    )
                    net.write(
                        "t "
                        + str(x)
                        + " "
                        + str(y + y_jump * len(previous_place[1]) / 2)
                        + " t_"
                        + step
                        + " n 0 w n {} ne\n"
                    )
                    for n, pr_pl_i in enumerate(previous_place[1]):
                        x_i = x
                        y_i = y
                        x_i += x_jump
                        net.write("e " + pr_pl_i + " t_" + step + " 1 n\n")
                        if is_retry:
                            net.write(
                                "t "
                                + str(x_i - x_jump)
                                + " "
                                + str(y_i + y_jump + n / 2 * y_jump)
                                + " t_fail_"
                                + step
                                + "_"
                                + str(n)
                                + " s 0 w n {} ne\n"
                            )
                            net.write(
                                "e "
                                + "t_fail_"
                                + step
                                + "_"
                                + str(n)
                                + " "
                                + origin_place
                                + " 1 n\n"
                            )
                            net.write(
                                "e "
                                + pr_pl_i
                                + "t_fail_"
                                + step
                                + "_"
                                + str(n)
                                + " 1 n\n"
                            )
                    net.write("e " + "t__" + step + " " + step + " 1 n\n")
                    x += x_jump
            previous_place = step
        elif step in dict and current_action == "_err_":
            step = "NOT_" + step
            x += x_jump
            i_step += 1
            net.write("t " + str(x) + " " + str(y) + " t_" + step + " n 0 w n {} ne\n")
            net.write("e " + previous_place + " t_" + step + " 1 n\n")
            x += x_jump
            net.write("p " + str(x) + " " + str(y) + " " + step + " 0 n {} ne\n")
            net.write("e " + "t__" + step + " " + step + " 1 n\n")
            previous_place = step
        elif step in compo:
            current_action = step
        elif step in dict and is_retry:
            numbers[dict.index(step)] += 1
            if numbers[dict.index(step)] > 0:
                step = step + "_" + str(numbers[dict.index(step)])
            x += x_jump
            i_step += 1
            net.write("t " + str(x) + " " + str(y) + " t_" + step + " n 0 w n {} ne\n")
            net.write("e " + previous_place + " t_" + step + " 1 n\n")
            net.write(
                "t "
                + str(x)
                + " "
                + str(y + y_jump)
                + " t_fail_"
                + step
                + " s 0 w n {} ne\n"
            )
            x += x_jump
            net.write("p " + str(x) + " " + str(y) + " " + step + " 0 n {} ne\n")
            net.write("e " + "t_fail_" + step + " " + origin_place + " 1 n\n")
            net.write("e " + previous_place + " t_fail_" + step + " 1 n\n")
            net.write("e " + "t__" + step + " " + step + " 1 n\n")
            previous_place = step
        elif type(step) != str:
            mission_sequence = step
            is_retry = False
            coords = x, y, x_jump, y_jump
            if current_action == "_retry_":
                is_retry = True
                origin_place = previous_place
            if "+" in step:
                origin_place = previous_place
                x_max = x
                choice_end = ["+", []]
                for n, road in enumerate(step):
                    x_i = x
                    y_i = y
                    if road not in compo:
                        x_i += x_jump
                        y_i += n / 2 * y_jump
                        choice_origin_place = origin_place + "_choice_" + str(n // 2)
                        net.write(
                            "t "
                            + str(x_i)
                            + " "
                            + str(y_i + n / 2 * y_jump)
                            + " t_"
                            + choice_origin_place
                            + " s 0 w n {} ne\n"
                        )
                        x_i += x_jump
                        net.write(
                            "p "
                            + str(x_i)
                            + " "
                            + str(y_i + n / 2 * y_jump)
                            + " "
                            + choice_origin_place
                            + " 0 n {} ne\n"
                        )
                        net.write(
                            "e "
                            + origin_place
                            + " "
                            + "t__"
                            + choice_origin_place
                            + " 1 n\n"
                        )
                        net.write(
                            "e "
                            + "t__"
                            + choice_origin_place
                            + " "
                            + choice_origin_place
                            + " 1 n\n"
                        )
                        current_action = "->"
                        if type(road) != list:
                            road = [road]
                        coords_i = x_i, y_i + n / 2 * y_jump, x_jump, y_jump
                        previous_place, x_i, y_i = create_net(
                            net,
                            road,
                            dict,
                            compo,
                            choice_origin_place,
                            current_action,
                            origin_place,
                            is_retry,
                            coords_i,
                            numbers,
                        )
                        choice_end[1].append(previous_place)
                        if x_i > x_max:
                            x_max = x_i
                x = x_max
                previous_place = choice_end
            elif "|" in step:
                origin_place = previous_place
                x += x_jump
                concur_end = ["|", []]
                concur_origin_place = origin_place + "_concur"
                net.write(
                    "t "
                    + str(x)
                    + " "
                    + str(y + y_jump * len(step) / 4)
                    + " t_"
                    + concur_origin_place
                    + " s 0 w n {} ne\n"
                )
                net.write(
                    "e " + origin_place + " " + "t__" + concur_origin_place + " 1 n\n"
                )
                x_max = x
                for n, road in enumerate(step):
                    x_i = x
                    y_i = y
                    if road not in compo:
                        y_i += n / 2 * y_jump
                        x_i += x_jump
                        net.write(
                            "p "
                            + str(x_i)
                            + " "
                            + str(y_i + n / 2 * y_jump)
                            + " "
                            + concur_origin_place
                            + "_"
                            + str(n // 2)
                            + " 0 n {} ne\n"
                        )
                        net.write(
                            "e "
                            + "t__"
                            + concur_origin_place
                            + " "
                            + concur_origin_place
                            + "_"
                            + str(n // 2)
                            + " 1 n\n"
                        )
                        current_action = "->"
                        if type(road) != list:
                            road = [road]
                        coords_i = x_i, y_i + n / 2 * y_jump, x_jump, y_jump
                        previous_place, x_i, y_i = create_net(
                            net,
                            road,
                            dict,
                            compo,
                            concur_origin_place + "_" + str(n // 2),
                            current_action,
                            origin_place,
                            is_retry,
                            coords_i,
                            numbers,
                        )
                        concur_end[1].append(previous_place)
                        if x_i > x_max:
                            x_max = x_i
                x = x_max
                previous_place = concur_end
            elif "," in step:
                x += x_jump
                if len(step) != 5:
                    print("_ite_ structure not proper, please check.")
                    quit()
                origin_place = previous_place
                if_road = step[0]
                then_road = step[2]
                else_road = step[4]
                net.write(
                    "t "
                    + str(x)
                    + " "
                    + str(y)
                    + " t_"
                    + str(if_road)
                    + "_success"
                    + " s 0 w n {} ne\n"
                )
                net.write(
                    "e "
                    + origin_place
                    + " "
                    + "t__"
                    + str(if_road)
                    + "_success"
                    + " 1 n\n"
                )
                net.write(
                    "p "
                    + str(x + x_jump)
                    + " "
                    + str(y)
                    + " "
                    + "then_road"
                    + " 0 n {} ne\n"
                )
                net.write(
                    "e "
                    + "t__"
                    + str(if_road)
                    + "_success"
                    + " "
                    + "then_road"
                    + " 1 n\n"
                )
                if type(then_road) != list:
                    then_road = [then_road]
                coords_i = x + x_jump, y, x_jump, y_jump
                previous_place_then, x_then, y = create_net(
                    net,
                    then_road,
                    dict,
                    compo,
                    "then_road",
                    "->",
                    origin_place,
                    is_retry,
                    coords_i,
                    numbers,
                )
                net.write(
                    "t "
                    + str(x)
                    + " "
                    + str(y + y_jump)
                    + " t_"
                    + if_road
                    + "_failure"
                    + " s 0 w n {} ne\n"
                )
                net.write(
                    "e "
                    + origin_place
                    + " "
                    + "t__"
                    + str(if_road)
                    + "_failure"
                    + " 1 n\n"
                )
                net.write(
                    "p "
                    + str(x + x_jump)
                    + " "
                    + str(y + y_jump)
                    + " "
                    + "else_road"
                    + " 0 n {} ne\n"
                )
                net.write(
                    "e "
                    + "t__"
                    + str(if_road)
                    + "_failure"
                    + " "
                    + "else_road"
                    + " 1 n\n"
                )
                if type(else_road) != list:
                    else_road = [else_road]
                coords_i = x + x_jump, y + y_jump, x_jump, y_jump
                previous_place_else, x_else, y_i = create_net(
                    net,
                    else_road,
                    dict,
                    compo,
                    "else_road",
                    "->",
                    if_road,
                    is_retry,
                    coords_i,
                    numbers,
                )
                x = max(x_then, x_else)
                previous_place = ["_ite_", [previous_place_then, previous_place_else]]
            else:
                previous_place, x, y = create_net(
                    net,
                    mission_sequence,
                    dict,
                    compo,
                    previous_place,
                    current_action,
                    origin_place,
                    is_retry,
                    coords,
                    numbers,
                )
                is_retry = False

    return previous_place, x, y

def print_progress(i,length,dtime):
    sys.stdout.write('\r')
    sys.stdout.write("Progress: [%-20s] %d%%" % ('='*int(20*i/(length-1)), 100*i/(length-1)))
    sys.stdout.write(" ({:.1f}s.)".format(dtime))
    sys.stdout.flush()

def extract_state_space(file_path):
    markings = []
    write_line = False
    get_markings = False
    transit_choice = []
    states = []
    time_in = time()
    with open(file_path, "r") as tina_file:
        length=len(tina_file.readlines())
        tina_file.seek(0)
        for i,line in enumerate(tina_file):
            if int(100*(i+1)/length)%5 == 0:
                print_progress(i,length,time()-time_in)
            if "MARKINGS:" in line:
                get_markings = True
            elif get_markings and "REACHABILITY GRAPH:" not in line:
                markings.append(line)
            elif "REACHABILITY GRAPH:" in line:
                write_line = True
                get_markings = False
            elif "LIVENESS ANALYSIS" in line:
                write_line = False
            elif write_line and "LIVENESS ANALYSIS" not in line:
                markings.append(line)
                line = line.replace(" ", "")
                line = line.replace("\n", "")
                if "->" in line:
                    state_i = line[:line.index("->")]
                    states.append(state_i)
                    available_transits = line[line.index("->")+2:]
                    if available_transits.find(",")==-1:
                        transit_choice.append(available_transits)
                    else:
                        transit_choice_i = []
                        while available_transits.find(",")!=-1:
                            transit_choice_i.append(available_transits[:available_transits.index(",")])
                            available_transits = available_transits[available_transits.index(",")+1:]
                        transit_choice_i.append(available_transits)
                        transit_choice.append(transit_choice_i)
            else:
                pass
        sys.stdout.write('\n')
    markings = markings[1:-1]
    return markings, states, transit_choice

def remove_number(string):
    string = string[::-1]
    string = string[string.find("__")+2:]
    string = string[::-1]
    return string

def create_tr(mission_net,mission_tr,nb_tr,previous_place,entry_place,instance_place):
    tr = "tr t"+str(nb_tr)+" "+" ".join(previous_place)+" -> "+entry_place+" "+instance_place+"\n"
    mission_net.write(tr)
    mission_tr.append(["t"+str(nb_tr),entry_place,previous_place])
    return mission_tr

def create_skill_segment(mission_net,mission_tr,nb_instances,nb_tr,skillnames,skill,sk_in,sk_out,previous_place):
    entry_place = sk_in
    exit_place = sk_out
    instance_place = sk_in+"_"+str(nb_instances[skillnames.index(skill)])
    
    mission_net.write("pl "+entry_place+":"+entry_place+" (0) \n")
    mission_net.write("pl "+exit_place+":"+exit_place+" (0) \n")
    mission_net.write("pl "+instance_place+" (0) \n")
    nb_instances[skillnames.index(skill)] += 1
    
    mission_tr = create_tr(mission_net,mission_tr,nb_tr,previous_place,entry_place,instance_place)
    nb_tr+=1
    previous_place = [exit_place,instance_place]
    return nb_instances,nb_tr,previous_place