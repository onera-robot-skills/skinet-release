
from .net_def import *
from .skillset_net import SkillsetNet

# USER FRIENDLY FORMAT GUIDE
#   - cs:
#       - places: cs_name[robot1,robot2,...].executing
#                 cs_name[robot1,robot2,...].idle
#                 cs_name[robot1,robot2,...].request
#       - transitions: cs_name[robot1,robot2,...].start
#                      cs_name[robot1,robot2,...].success.x
#                      cs_name[robot1,robot2,...].failure.y
#   - skill:
#       - places: robot.skill.executing
#                 robot.skill.start
#                 robot.skill.request
#       - transitions: robot.skill.start
#                      robot.skill.success.x
#                      robot.skill.failure.y,
#                      robot.skill.precondition.z
#                      robot.skill.invariant.a
#                      robot.skill.interrupt
#   - resources:
#       - robot.resources.res_name.res_state
#
#   - Logic operators:
#       - and,or,not
#   - LTL operators:
#       - F,G,X,U
#   - CTL operators:
#       - A,E + (F,G,X,U)


SKILL_PLACE_DICT = {PLACE_SUFFIX.IDLE: "idle",
                    PLACE_SUFFIX.EXECUTING: "executing"}
SKILL_PLACE_DICT_INV = {v: k for k, v in SKILL_PLACE_DICT.items()}

LTL_OPS = {"F": "<>", "G": "[]", "X": "()", "U": "U"}
CTL_OPS = ["A"+x for x in LTL_OPS.keys()]+["E"+x for x in LTL_OPS.keys()]


def place_to_userstr(place: str) -> str:
    """
    Returns a place name into a user friendly format
    """
    place_items = place.split("__")
    # skill place
    if place_items[1] in SKILL_PLACE_DICT.keys():
        return place_items[0]+place_items[2]+"."+SKILL_PLACE_DICT[place_items[1]]
    # resource place
    else:
        return ".".join(place_items)


def userstr_to_place(user_str: str) -> str:
    """
    Returns the place name from the user friendly format input
    """
    #TODO
    return user_str

def trname_to_userstr(tr_name: str) -> str:
    """
    Returns the transition name to the user friendly format
    """
    tr_name = tr_name.split("__")[1:-1]
    return tr_name[0] if len(tr_name) == 1 else ".".join(tr_name)


def userstr_to_netitem(user_str: str, skillset_net: SkillsetNet = None) -> str:
    """
    Returns a place name or a transition name depending on user input
    """
    # Tina, LTL, CTL operators
    if user_str == "and":
        return "/\\"
    if user_str == "or":
        return "\\/"
    if user_str == "not":
        return "-"
    if user_str in CTL_OPS:
        return user_str
    if user_str in LTL_OPS.keys():
        return LTL_OPS[user_str]

    # Net items
    userstr_items = user_str.split(".")

    # event
    if len(userstr_items) == 1:
        return "<"+"__".join(userstr_items)+"__0>T"
    elif len(userstr_items) == 2:
        # skill val_fail, interrupt, idle, running
        if userstr_items[-1] == "validate":
            return "x__"+userstr_items+[1]+"__val_fail"
        if userstr_items[-1] == "interrupt":
            return "x__"+userstr_items[1]+"__"+userstr_items[-1]
        if userstr_items[-1] in SKILL_PLACE_DICT_INV.keys():
            return SKILL_PLACE_DICT_INV[userstr_items[-1]]+"__"+userstr_items[0]
        # resource
        return "__".join(userstr_items)
    else:
        return user_str


def userstr_to_property(user_str: str, skillset_net: SkillsetNet) -> str:
    prop_str = ""
    while user_str != "":
        i = 0
        loop = True
        word = ""
        while loop:
            if not user_str[i].isalpha() and user_str[i] not in ["_", ".", ",", "[", "]"]:
                if word not in ["", "T", "F"]:
                    prop_str += userstr_to_netitem(word)+user_str[i]
                else:
                    prop_str += word+user_str[i]
                user_str = user_str[i+1:]
                word = ""
                loop = False
            else:
                word += user_str[i]
                i += 1
                if i == len(user_str):
                    prop_str += userstr_to_netitem(word, skillset_net)
                    user_str = ""
                    loop = False
    return prop_str
