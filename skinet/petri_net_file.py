from skinet.petri_net import *
from os import remove as os_remove
from os.path import dirname
from copy import deepcopy


class PetriNetFile(dict):
    """
    A class to manipulate Petri net files (.net). Creates a dictionary from a PetriNet class.
    Comes with basic functions to change places marking, transitions I/O, priorities, etc...
    """

    def __init__(self, name: str = None, net: PetriNet = None, dir: str = ""):

        if name:
            self.__name = name
        else:
            self.__name = "petri_net"

        if net:
            self.__net = net
            if not name:
                self.__name = self.__net.name
            self._init_places()
            self._init_transitions()
            self._init_priorities()
        else:
            self.__net = PetriNet(self.__name)

        self.__dir = dir
        self.__filename = self.__name+".net"
        self.__ndrfilename = None

    def _init_places(self):
        for place in self.__net.places.values():
            self[place] = place.to_net_file()

    def _init_transitions(self):
        for transition in self.__net.transitions.values():
            self[transition] = transition.to_net_file()

    def _init_priorities(self):
        for tr, prio_tr in self.__net.priorities.items():
            if prio_tr != []:
                tr_name = "{"+tr.name+"}" if "/" in tr.name else tr.name
                list_trs = [
                    "{"+tr_.name+"}" if ("/" in tr_.name and tr_.name[0] != "{") else tr_.name for tr_ in prio_tr]
                self[tr.name+"__prio"] = "pr " + \
                    " ".join(list_trs)+" > "+tr_name+"\n"

    @property
    def filename(self) -> str:
        return self.__filename

    @filename.setter
    def filename(self, value: str):
        self.__filename = value
        self._write_net_file()

    @property
    def dir(self) -> str:
        return self.__dir

    def to_str(self) -> str:
        """
        Returns a string of all lines in the Petri net file.
        """
        to_str_ = "net "+(("{"+self.__net.name+"}")
                          if "/" in self.__net.name else self.__net.name)+"\n"
        pr_str_ = ""
        for k, v in self.items():
            if isinstance(k, (Place, Transition)):
                to_str_ += v
            else:
                pr_str_ += v
        return to_str_+pr_str_

    def to_ndr(self) -> dict:
        """
        Returns a .ndr version of the Petri net file.
        """
        raise NotImplementedError("Not yet supported.")

    def to_ndr_file(self, filename: str = None, dir: str = "") -> str:
        """
        Creates a ndr file of the Petri net.
        """
        self.__dir = dir
        if not filename:
            filename = self.__net.name+".ndr"
        self.__ndrfilename = filename
        with open(self.__dir+"/"+self.__ndrfilename, "w") as file:
            file.write(self.to_ndr())
        return filename

    def to_net_file(self, filename: str = None, dir: str = None) -> str:
        """
        Creates a net file of the Petri net.
        """
        if not filename:
            filename = self.__filename
        self.__filename = filename
        if not dir:
            self.__dir = dirname(self.__filename)
        else:
            self.__dir = dir
        self._write_net_file()
        return filename

    def _write_net_file(self):
        with open(self.__dir+self.__filename, "w") as file:
            file.write(self.to_str())

    def remove_file(self):
        if self.__filename:
            os_remove(self.__filename)

    #### Place manipulation ####

    def change_marking(self, places: list[Place]):
        """
        Change the marking of the listed places.
        """
        for place in places:
            self[place] = place.to_net_file()
        if self.__filename:
            self._write_net_file()

    def update_marking(self):
        """
        Update the marking.
        """
        self._init_places()
        if self.__filename:
            self._write_net_file()

    #### Base methods ####
    def __add__(self, net):
        for k, v in net.items():
            self[k] = v
        return self

    def __sub__(self, net):
        for k in net.keys():
            self.pop(k)
        return self
