
from itertools import count
from copy import deepcopy


class Place():

    __p_counter = count(0)

    def __init__(self, p_name: str = None,
                 p_mark: int = None,
                 label: str = None) -> None:
        if p_name == None:
            self.__name = "p"+str(next(self.__p_counter))
        else:
            self.__name = p_name
            self.__p_counter = next(self.__p_counter)
        self.__marking = int(p_mark) if p_mark else 0
        self.__label = label

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, value: str):
        self.__name = value

    @property
    def marking(self) -> int:
        return self.__marking

    @marking.setter
    def marking(self, value: int) -> int:
        self.__marking = value

    @property
    def label(self) -> str | None:
        return self.__label

    @label.setter
    def label(self, value: str):
        self.__label = value

    def is_marked(self) -> bool:
        return True if self.__marking != 0 else False

    def to_net_file(self) -> str:
        label_ = ""
        if self.__label:
            label_ = ":" + \
                ("{"+self.__label +
                 "}") if "/" in self.__label and self.__label[0] != "{" else ""
        pl_name = "{"+self.__name + \
            "}" if (
                "/" in self.__name and self.__name[0] != "{") else self.__name
        return "pl "+pl_name+label_+" (" + str(self.__marking) + ")" + "\n"

    def __str__(self) -> str:
        return self.__name + ((":"+self.__label) if self.__label else "") + " (" + str(self.__marking) + ")"


class Transition():

    __t_counter = count(0)

    def __init__(self, name: str = None,
                 inputs: list[str] = None,
                 outputs: list[str] = None) -> None:
        if name == None:
            self.__name = "t"+str(next(Transition.__t_counter))
        else:
            self.__name = name
            next(Transition.__t_counter)
        self.__inputs = inputs if inputs else []
        self.__outputs = outputs if outputs else []

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, value: str):
        self.__name = value

    @property
    def inputs(self) -> list[str]:
        return self.__inputs

    @property
    def outputs(self) -> list[str]:
        return self.__outputs

    def add_input(self, place) -> None:
        if isinstance(place, str):
            self.__inputs.append(place)
        elif isinstance(place, Place):
            self.__inputs.append(place.name)
        elif isinstance(place, list):
            for p in place:
                self.add_input(p)

    def add_output(self, place) -> None:
        if isinstance(place, str):
            self.__outputs.append(place)
        elif isinstance(place, Place):
            self.__outputs.append(place.name)
        elif isinstance(place, list):
            for p in place:
                self.add_output(p)

    def clear_input(self) -> None:
        self.__inputs = []

    def clear_output(self) -> None:
        self.__outputs = []

    def pop_input(self, place) -> None:
        if isinstance(place, str):
            self.__inputs.remove(place)
        elif isinstance(place, Place):
            self.__inputs.remove(place.name)
        elif isinstance(place, list):
            for p in place:
                self.pop_input(p)

    def pop_output(self, place) -> None:
        if isinstance(place, str):
            self.__outputs.remove(place)
        elif isinstance(place, Place):
            self.__outputs.remove(place.name)
        elif isinstance(place, list):
            for p in place:
                self.pop_output(p)

    def to_net_file(self) -> str:
        in_p = ["{"+place+"}" if ("/" in place and place[0] != "{")
                else place for place in self.__inputs]
        out_p = [
            "{"+place+"}" if ("/" in place and place[0] != "{") else place for place in self.__outputs]
        tr_name = "{"+self.__name + \
            "}" if (
                "/" in self.__name and self.__name[0] != "{") else self.__name
        return "tr "+tr_name+" "+" ".join(in_p)+" -> "+" ".join(out_p) + "\n"

    def __str__(self) -> str:
        in_p = [place for place in self.__inputs]
        out_p = [place for place in self.__outputs]
        return self.__name+" ("+", ".join(in_p)+" -> "+", ".join(out_p)+")"


class PetriNet():

    __n_counter = count(0)

    def __init__(self, name: str = None, net_file: str = None) -> None:
        if name == None:
            self.__name = "N"+str(next(self.__n_counter))
        else:
            self.__name = name
            self.__n_counter = next(self.__n_counter)

        self.__places: dict[str, Place] = {}
        self.__labelled_places: dict[str, Place] = {}
        self.__transitions: dict[str, Transition] = {}
        self.__priorities: dict[Transition, list[Transition]] = {}
        if net_file:
            self._init_from_file(net_file)

        self.__marking = []
        self.__history = []
        for pl in self.__places.keys():
            if self.__places[pl].marking == 1:
                self.__marking.append(self.__places[pl].name)

    @property
    def name(self) -> str:
        return self.__name

    @property
    def places(self) -> dict[str, Place]:
        return self.__places

    @places.setter
    def places(self, value: dict[str, Place]):
        self.__places = value

    def add_label_to_place(self, place: str, label: str) -> None:
        self.__places[place].label = label
        self.__labelled_places[label] = self.__places[place]

    def get_place_from_label(self, label) -> Place | None:
        return self.__labelled_places.get(label, None)

    @property
    def marking(self) -> list[str]:
        return self.__marking

    @marking.setter
    def marking(self, value):
        self.__marking = value

    @property
    def transitions(self) -> dict[str, Transition]:
        return self.__transitions

    @transitions.setter
    def transitions(self, value: dict[str, Transition]):
        self.__transitions = value

    @property
    def priorities(self) -> dict[Transition, list[Transition]]:
        return self.__priorities

    @priorities.setter
    def priorities(self, value: dict[Transition, list[Transition]]):
        self.__priorities = value

    @property
    def history(self) -> list[Transition]:
        return self.__history

    def reset_marking(self) -> None:
        self.__marking = []
        for pl in self.__places.keys():
            if self.__places[pl].marking == 1:
                self.__marking.append(pl)

    def _init_from_file(self, net_file):
        net_type = net_file[-4:]
        if net_type == ".net":
            self._parse_net(net_file)
        elif net_type == ".ndr":
            self._parse_ndr(net_file)
        else:
            print("Unrecognized net format: "+net_file)
            print("Expected: .net, .ndr")
            quit()

    def _parse_net(self, net_file):
        with open(net_file, "r") as file:
            for line in file:
                line = line.replace('\n', '').split(" ")
                if line[0] == "pl":
                    # check for label
                    label = None
                    if ":" in line:
                        label = line.pop(line.index(":")+1)
                        label = label.replace('{', '').replace('}', '')
                        line.remove(":")
                    elif ":" in line[1]:
                        line[1], label = line[1].split(":")
                    if label not in self.__labelled_places:
                        if len(line) <= 2:
                            p_mark = 0
                        else:
                            p_mark = int(line[2][1:-1])
                        p_name = line[1].replace('{', '').replace('}', '')
                        new_place = Place(
                            p_name, p_mark, label)
                        self.__places[p_name] = new_place
                        if label:
                            if label in self.__labelled_places.keys():
                                raise ValueError(
                                    "label "+label+" already present in net"+self.__name)
                            self.__labelled_places[label] = new_place
                    else:
                        if len(line) > 2:
                            p_mark = int(line[2][1:-1])
                            self.__labelled_places[label].marking += p_mark
                elif line[0] == "tr":
                    i_split = line.index("->")
                    tr_name = line[1].replace('{', '').replace('}', '')
                    self.__transitions[tr_name] = Transition(
                        tr_name, line[2:i_split], line[i_split+1:])
                    self.__priorities[self.__transitions[tr_name]] = []
                elif line[0] == "pr":
                    try:
                        lesser_tr = line[-1].replace('{', '').replace('}', '')
                        self.__priorities[self.__transitions[lesser_tr]
                                          ] += [self.__transitions[tr_name.replace('{', '').replace('}', '')] for tr_name in line[1:-2]]
                    except:
                        print("_______Error parsing priority___________")
                        print(line)
                        print(self.__transitions[line[-1]])

    def _parse_ndr(self, net_file) -> None:
        with open(net_file, "r") as file:
            for line in file:
                line = line.split(" ")
                if line[0] == "p":
                    print(line[3], line[4])
                elif line[0] == "t":
                    print(line[3])
                elif line[0] == "e":
                    print(line[1], line[2])
        raise NotImplementedError(
            "Not implemented. If you need, ask or try it yourself! ;) ")

    def add_place(self, place, marking: int = 0) -> None:
        if isinstance(place, str):
            place = Place(place, marking)
            self.__places[place.name] = place
        elif isinstance(place, Place):
            self.__places[place.name] = place
            if place.label:
                self.__labelled_places[place.label] = place
        elif isinstance(place, list):
            for p in place:
                self.add_place(p)
        self.reset_marking()

    def pop_place(self, place) -> None:
        if isinstance(place, str):
            del self.__places[place]
        elif isinstance(place, Place):
            del self.__places[place.name]
        elif isinstance(place, list):
            for p in place:
                self.pop_place(p)
        self.reset_marking()

    def get_index(self, name: str) -> int:
        for i, place in enumerate(self.places):
            if place.name == name:
                return i
        for i, tr in enumerate(self.transitions):
            if tr.name == name:
                return i
        return -1

    def add_transition(self, transition) -> None:
        if isinstance(transition, str):
            transition = Transition(transition)
            self.__transitions[transition.name] = transition
        elif isinstance(transition, Transition):
            self.__transitions[transition.name] = transition
        elif isinstance(transition, list):
            for tr in transition:
                self.add_transition(tr)

    def pop_transition(self, transition) -> None:
        if isinstance(transition, str):
            del self.__transitions[transition]
        elif isinstance(transition, Place):
            del self.__transitions[transition.name]
        elif isinstance(transition, list):
            for tr in transition:
                self.pop_transition(tr)

    def set_marking(self, marking: list[str]) -> None:
        self.__marking = marking

    def no_marking(self) -> None:
        for i in range(len((self.__places))):
            self.__places[i].marking = 0

    def update_marking(self, p_to_mark: list[Place]) -> None:
        if isinstance(p_to_mark, Place):
            self.__places[self.__places.index(p_to_mark)].mark()
        else:
            for i, place in enumerate(self.__places):
                if place in p_to_mark:
                    self.__places[i].mark()

    def new_marking(self, p_to_mark: list[Place]) -> None:
        self.no_marking()
        self.update_marking(p_to_mark)

    def enabled(self) -> list[Transition]:
        enabled_tr = []
        for tr in self.__transitions.keys():
            inputs_marked = all(
                [pl in self.__marking if "?-1" not in pl else True for pl in self.__transitions[tr].inputs])
            no_forbidden = not any(
                pl[:-3] in self.__marking if "?-1" in pl else False for pl in self.__transitions[tr].inputs)
            if inputs_marked and no_forbidden:
                enabled_tr.append(self.__transitions[tr])
        return enabled_tr

    def firable(self) -> list[Transition]:
        enabled_tr = self.enabled()
        firable_tr: list[Transition] = []
        for tr in enabled_tr:
            if not any(tr in enabled_tr for tr in self.__priorities.get(tr, [])):
                firable_tr.append(tr)
        return firable_tr

    def fire(self, transition) -> bool:
        firable_tr = self.firable()
        if isinstance(transition, str):
            transition = self.__transitions[transition]
        if isinstance(transition, Transition):
            try:
                if transition in firable_tr:
                    for pl in transition.inputs:
                        if "?-1" not in pl:
                            self.__marking.remove(pl)
                            self.__places[pl].marking -= 1
                    for pl in transition.outputs:
                        self.__marking.append(pl)
                        self.__places[pl].marking += 1
                    self.__history.append(transition)
                    return True
                else:
                    return False
            except:
                if ValueError:
                    raise ValueError("Tried to add/take a token from "+pl +
                                     " when firing "+transition.name+", but the place does not exist.")
        elif isinstance(transition, list):
            for tr in transition:
                while res := self.fire(tr):
                    pass
            return res
        return False

    def undo(self, n: int = 1) -> bool:
        """
        Undo n transitions. One by default. -1 undoes all transitions.
        Returns False if the history is empty before undoing the n transitions, True otherwise.
        """
        if self.__history == []:
            return False
        else:
            if n == -1:
                n = len(self.__history)
            for i in range(n):
                if self.__history == []:
                    return False
                else:
                    tr = self.__history.pop(-1)
                    for pl in tr.outputs:
                        self.unmark(self.places[pl])
                    for pl in tr.inputs:
                        self.mark(self.places[pl])
            return True

    def mark(self, place: Place):
        self.places[place.name].marking = 1
        self.reset_marking()

    def unmark(self, place: Place):
        self.places[place.name].marking = 0
        self.reset_marking()

    def is_marked(self, place: Place) -> bool:
        return self.__places[place.name].is_marked()

    def add_net_suffix(self, suffix: str):
        old_net = deepcopy(self)
        for pl_name, place in old_net.places.items():
            new_place = deepcopy(place)
            new_place.name = suffix+"__"+place.name
            self.__places[new_place.name] = new_place
            self.__places.pop(pl_name)
        self.__priorities = {}
        for tr_name, tr in old_net.transitions.items():
            new_tr = deepcopy(tr)
            new_tr.name = "t__"+suffix+"__"+tr.name[3:]
            self.__transitions[new_tr.name] = new_tr
            self.__transitions.pop(tr_name)
            for input in tr.inputs:
                new_tr.add_input(suffix+"__"+input)
                new_tr.pop_input(input)
            for output in tr.outputs:
                new_tr.add_output(suffix+"__"+output)
                new_tr.pop_output(output)
        for tr, prio_trs in old_net.priorities.items():
            self.__priorities[self.__transitions["t__"+suffix+"__"+tr.name[3:]]] = [
                self.__transitions["t__"+suffix+"__"+prio_tr.name[3:]] for prio_tr in prio_trs]
        self.__history = []
        self.reset_marking()

    def __str__(self) -> str:
        n_str = "Net: "+self.__name+"\n"
        p_str = " - Places: \n    " + \
            ", \n    ".join([self.__places[place].__str__()
                            for place in self.__places])+"\n"
        t_str = " - Transitions: \n    " + \
            ", \n    ".join([self.__transitions[tr].__str__()
                            for tr in self.__transitions])
        return n_str+p_str+t_str
