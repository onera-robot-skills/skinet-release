from robot_language import *
import itertools

SEP = "__"

class PLACE_SUFFIX():
    REQUEST_START = "r"
    REQUEST_INTERRUPT = "r_int"
    IDLE = "i"
    EXECUTING = "e"
    RESULT = "x"

class TRANS_SUFFIX():
    TRANSITION = "t"

def add_p(net, name: str, marking: int = 0, label: str = None):
    label_str = ":"+label if label else ""
    net.write("pl " + name + label_str + " (" + str(marking) + ")\n")


def res_token(res, resources, resources_net):
    """
    Returns token location in the resource
    """
    i_res = resources.index(res)
    i = resources_net[i_res].index(1)
    return resources[i_res][1][i]


def generate_transits(
    net,
    Skillset,
    tr_type,
    guard,
    effects,
    postcond,
    in_place,
    t_name_init,
    t_out,
    eff_res,
    eff_states,
):
    t_generated = []
    if guard != None:  # If guard exists...
        res_grd = []
        state_grd = []
        missing_res_grd = []
        missing_eff_grd = set()
        if tr_type == "ev":
            if type(guard) == ResourceStateEqExpr:
                for res in guard.used_resources():
                    state_grd.append(guard.state)
                    res_grd.append(res)
            else:
                for res in guard.used_resources():
                    res_grd.append(res)
                    for state_i in res.states:
                        if state_i in str(guard):
                            strg = str(guard)
                            strstate = strg[
                                strg.index(state_i)
                                - 3: strg.index(state_i)
                                + len(state_i)
                            ]
                            if strstate[:2] == "==":
                                state_grd.append(strstate[3:])

                            else:
                                state_grd.append("")
            for res in res_grd:
                if res not in eff_res:
                    missing_eff_grd.add(res)
        elif tr_type == "pre":
            for g_i in guard:
                for res in set(g_i.used_resources()):
                    res_grd.append(res)
                    for state_i in res.states:
                        if state_i in str(g_i):
                            strg = str(g_i)
                            strstate = strg[
                                strg.index(state_i)
                                - 3: strg.index(state_i)
                                + len(state_i)
                            ]
                            if strstate[:2] == "==":
                                state_grd.append(strstate[3:])

                            else:
                                state_grd.append("")
                            if res not in eff_res:
                                missing_eff_grd.add(res)
        for res in eff_res:
            if res not in res_grd:
                missing_res_grd.append(res)
        for i, res in enumerate(res_grd):
            if res in eff_res and res in missing_eff_grd:
                missing_eff_grd.remove(res)
        if missing_res_grd == []:  # For perfect guard
            if tr_type == "ev":
                solutions = expr_all_solutions(Skillset, guard)
            elif tr_type == "pre":
                solutions = expr_all_solutions_conjunction(Skillset, guard)
            for n, solution in enumerate(solutions):
                t_in = ""
                t_out_missing_eff = ""
                if in_place != "":
                    t_in += " " + in_place
                t_name = t_name_init + "__" + str(n)
                for re in solution:
                    res, state = re.to_rl()[1:-1].replace("== ", "").split(" ")
                    if res+"__"+state not in t_in:
                        for missing_res in missing_eff_grd:
                            if res == missing_res.name and res+"__"+state not in t_out_missing_eff:
                                t_out_missing_eff += " " + res+"__"+state
                        t_in += " " + res+"__"+state
                transit = "tr " + t_name + t_in + t_out + t_out_missing_eff
                t_generated.append(t_name)
                net.write(transit + "\n")
        else:  # For missing guard on some resources
            if tr_type == "ev":
                solutions = expr_all_solutions(Skillset, guard)
            elif tr_type == "pre":
                solutions = expr_all_solutions_conjunction(Skillset, guard)
            c = 0
            for n, solution in enumerate(solutions):
                t_in = ""
                t_out_missing_eff = ""
                if in_place != "":
                    t_in += " " + in_place
                for re in solution:
                    res, state = re.to_rl()[1:-1].replace("== ", "").split(" ")
                    if res+"__"+state not in t_in:  # sometimes the same state is given more than once
                        for missing_res in missing_eff_grd:
                            if res == missing_res.name and res+"__"+state not in t_out_missing_eff:
                                t_out_missing_eff += " " + res+"__"+state
                        t_in += " " + res+"__"+state
                possible_state = []
                for j, effect in enumerate(effects):
                    if effect.resource in missing_res_grd:
                        res = effect.resource
                        index = eff_res.index(res)
                        ok_transits = res.transitions + [
                            (eff_states[index], eff_states[index])
                        ]
                        ok_states = []
                        for state in res.states:
                            if (state, eff_states[index]) in ok_transits:
                                ok_states.append(res.name+"__"+state)
                        possible_state.append(ok_states)
                if postcond != None:
                    solution = expr_all_solutions(Skillset, postcond)[0]
                    postcond_res = set(postcond.used_resources())
                    for res in postcond_res:
                        if res.name in [r.name for r in missing_res_grd]:
                            index = [r.name for r in eff_res].index(res.name)
                            ok_transits = res.transitions + [
                                (eff_states[index], eff_states[index])
                            ]
                            ok_states = []
                            for state in res.states:
                                if (state, eff_states[index]) in ok_transits:
                                    ok_states.append(res.name+"__"+state)
                            possible_state.append(ok_states)

                product = list(itertools.product(*possible_state))  # (x)Rn
                for j, sol in enumerate(product):
                    t_in_mssng_grd = ""
                    t_name = t_name_init + "__" + str(c)
                    for state in sol:
                        t_in_mssng_grd += " " + state
                    transit = (
                        "tr "
                        + t_name
                        + t_in
                        + t_in_mssng_grd
                        + t_out
                        + t_out_missing_eff
                    )
                    t_generated.append(t_name)
                    net.write(transit + "\n")
                    c += 1
    if guard == None:  # If no guard, use (x)Rn
        possible_state = []
        for j, effect in enumerate(effects):
            res = effect.resource
            index = eff_res.index(res)
            ok_transits = res.transitions + \
                [(eff_states[index], eff_states[index])]
            ok_states = []
            for state in res.states:
                if (state, eff_states[index]) in ok_transits:
                    ok_states.append(res.name+"__"+state)
            possible_state.append(ok_states)
        if postcond != None:
            solution = expr_all_solutions(Skillset, postcond)[0]
            for re in solution:
                elem = re.to_dict()["RSeq"]
                res = elem["resource"]
                index = eff_res.index(res)
                ok_transits = res.transitions + \
                    [(eff_states[index], eff_states[index])]
                ok_states = []
                for state in res.states:
                    if (state, eff_states[index]) in ok_transits:
                        ok_states.append(res.name+"__"+state)
                possible_state.append(ok_states)
        product = list(itertools.product(*possible_state))  # (x)Rn
        for n, sol in enumerate(product):
            t_in = ""
            if in_place != "":
                t_in += " " + in_place
            t_name = t_name_init + "__" + str(n)
            for j, state in enumerate(sol):
                t_in += " " + state
            transit = "tr " + t_name + t_in + t_out
            t_generated.append(t_name)
            net.write(transit + "\n")
    return t_generated


def write_events_transit(net, model, ref: dict = None, mark_request: bool = True):
    """
    Returns transitions in/out with guard and effect on the resource
    """
    Skillset = model.skillsets[0]
    events = Skillset.events
    t_events = []
    if ref == {}:
        return t_events
    if ref == None:
        ref = dict()
        for event in events:
            ref[event.name] = 0
    for i, event in enumerate(events):  # Event transitions
        if event.name in ref.keys():
            name, guard, effects = event.name, event.guard, event.effects
            t_name_init = "t__" + name
            request_place = "r__event__" + name
            if mark_request:
                add_p(net, request_place, 1)
            else:
                add_p(net, request_place, 0)
            in_place = request_place
            t_out = " -> " + request_place
            eff_states = []
            eff_res = []
            for effect in effects:  # Looking for effects
                t_out += " " + effect.resource.name+"__"+effect.state
                eff_states.append(effect.state)
                eff_res.append(effect.resource)
            tr_type = "ev"
            t_events.append(
                generate_transits(
                    net,
                    Skillset,
                    tr_type,
                    guard,
                    effects,
                    None,
                    in_place,
                    t_name_init,
                    t_out,
                    eff_res,
                    eff_states,
                )
            )
    t_events = sum(t_events, [])
    return t_events


def create_skill_net(net, model, t_events):
    Skillset = model.skillsets[0]
    skills = Skillset.skills
    resources_dict = {res.name: res for res in Skillset.resources}
    t_notprio = []
    t_invariants = []

    # Preconditions & Start are treated like the events algorithm
    # But with failures on top
    for i, skill in enumerate(skills):  # Event transitions
        name, preconditions, start = skill.name, skill.preconditions, skill.start
        t_name_init = TRANS_SUFFIX.TRANSITION + SEP + name + SEP + "start"
        add_p(net, PLACE_SUFFIX.IDLE + SEP + name, 1)
        request_place = PLACE_SUFFIX.REQUEST_START + SEP + name
        add_p(net, request_place, 1)
        in_place = PLACE_SUFFIX.IDLE + SEP + name + " " + request_place
        add_p(net, PLACE_SUFFIX.EXECUTING + SEP + name, 0)
        t_out = " -> " + PLACE_SUFFIX.EXECUTING + SEP + name + " " + request_place
        start_states = []
        start_res = []
        for effect in start:  # Looking at start effects
            t_out += " " + effect.resource.name+SEP+effect.state
            start_states.append(effect.state)
            start_res.append(effect.resource)
        guard = []
        guard_res = []
        guard_failure = []
        guard_name = []
        # guard_fail_grd = []
        for pre in preconditions:
            g_i = pre.guard
            guard.append(g_i)
            # guard_failure.append(pre.failure)
            guard_name.append(pre.name)
            g_i_res = []
            for res in set(g_i.used_resources()):
                g_i_res.append(res)
            guard_res.append(g_i_res)

            tr_type_fail = "ev"
            fail_name = name + "__pre_fail__" + pre.name
            t_name_init_fail = "t__" + name + "__pre_fail__" + pre.name
            add_p(net, "x__" + name + "__pre_fail__" + pre.name, 0)
            t_reset = (
                "tr t__reset__"
                + name
                + "__pre_fail__"
                + pre.name
                + " x__"
                + name
                + "__pre_fail__"
                + pre.name
                + " -> "
                + PLACE_SUFFIX.IDLE+SEP+name
            )
            t_notprio.append("t__reset__" + name + "__pre_fail__" + pre.name)
            net.write(t_reset + "\n")
            in_place_fail = PLACE_SUFFIX.IDLE+SEP+name + " " + request_place
            t_out_fail = " -> " + PLACE_SUFFIX.RESULT+SEP+name + "__pre_fail__" + pre.name + " " + request_place

            t_notprio.append(
                generate_transits(
                    net,
                    Skillset,
                    tr_type_fail,
                    ResourceNotExpr(g_i),
                    [],
                    None,
                    in_place_fail,
                    t_name_init_fail,
                    t_out_fail,
                    [],
                    [],
                )
            )

        tr_type = "pre"
        t_notprio.append(
            generate_transits(
                net,
                Skillset,
                tr_type,
                guard,
                start,
                None,
                in_place,
                t_name_init,
                t_out,
                start_res,
                start_states,
            )
        )

        # Validate
        tr_val_fail = "t__"+name+"__val_fail__0"
        tr_val_fail_reset = "t__reset__"+name+"__val_fail__0"
        x_fail = "x__"+name+"__val_fail"
        add_p(net, x_fail, 0)
        net.write("tr "+tr_val_fail+" "+PLACE_SUFFIX.IDLE+SEP+name+" " +
                  request_place+"-> "+x_fail+" "+request_place+"\n")
        net.write("tr "+tr_val_fail_reset+" "+x_fail+" " +
                  request_place+"-> "+PLACE_SUFFIX.IDLE+SEP+name+" "+request_place+"\n")
        t_notprio.append(tr_val_fail)
        t_notprio.append(tr_val_fail)

        # Invariants & Exits (int,succ,fail) are made just like events.
        # However, Invariants failures transitions have a higher priority above
        # everything else, as the skill exit needs to be instant.

        name, invariants = skill.name, skill.invariants
        guard = []
        guard_res = []
        guard_failure = []
        guard_name = []
        # guard_fail_grd = []
        for inv in invariants:
            g_i = inv.guard
            guard.append(g_i)
            guard_failure.append(inv.failure)
            guard_name.append(inv.name)
            g_i_res = []
            fail_states = []
            fail_res = []
            for res in set(g_i.used_resources()):
                g_i_res.append(res)
            guard_res.append(g_i_res)

            if inv.failure != []:
                tr_type_fail = "ev"
                fail_name = name + "__inv_fail__" + inv.name
                t_name_init = "t__" + name + "__inv_fail__" + inv.name
                add_p(net, "x__" + name + "__inv_fail__" + inv.name, 0)
                t_reset = (
                    "tr t__reset__"
                    + name
                    + "__inv_fail__"
                    + inv.name
                    + " x__"
                    + name
                    + "__inv_fail__"
                    + inv.name
                    + " -> "
                    + PLACE_SUFFIX.IDLE+SEP+name
                )
                t_notprio.append("t__reset__" + name +
                                 "__inv_fail__" + inv.name)
                net.write(t_reset + "\n")
                in_place = PLACE_SUFFIX.EXECUTING+SEP+name
                t_out = " -> x__" + fail_name
                for fail in inv.failure:
                    t_out += " " + str(fail.resource)+"__"+fail.state
                    fail_states.append(fail.state)
                    fail_res.append(fail.resource)

                t_invariants.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type_fail,
                        ResourceNotExpr(g_i),
                        inv.failure,
                        None,
                        in_place,
                        t_name_init,
                        t_out,
                        fail_res,
                        fail_states,
                    )
                )
            else:
                tr_type_fail = "ev"
                fail_name = name + "__inv_fail__" + inv.name
                t_name_init = "t__" + name + "__inv_fail__" + inv.name
                add_p(net, "x__" + name + "__inv_fail__" + inv.name, 0)
                t_reset = (
                    "tr t__reset__"
                    + name
                    + "__inv_fail__"
                    + inv.name
                    + " x__"
                    + name
                    + "__inv_fail__"
                    + inv.name
                    + " -> "
                    + PLACE_SUFFIX.IDLE+SEP+name
                )
                t_notprio.append("t__reset__" + name +
                                 "__inv_fail__" + inv.name)
                net.write(t_reset + "\n")
                in_place = PLACE_SUFFIX.EXECUTING+SEP+name
                t_out = " -> x__" + fail_name
                for fail in inv.failure:
                    t_out += " " + str(fail.resource)+"__"+fail.state
                    fail_states.append(fail.state)
                    fail_res.append(fail.resource)

                t_invariants.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type_fail,
                        ResourceNotExpr(g_i),
                        inv.failure,
                        None,
                        in_place,
                        t_name_init,
                        t_out,
                        fail_res,
                        fail_states,
                    )
                )

        tr_type = "pre"

        # Successes
        for success_i in skill.success:
            t_name_init = "t__" + name + "__success__" + success_i.name
            add_p(net, "x__" + name + "__success__" + success_i.name, 0)
            in_place = PLACE_SUFFIX.EXECUTING+SEP+name
            t_out = " -> x__" + name + "__success__" + success_i.name
            t_reset = (
                "tr t__reset__"
                + name
                + "__success__"
                + success_i.name
                + " x__"
                + name
                + "__success__"
                + success_i.name
                + " -> "
                + PLACE_SUFFIX.IDLE+SEP+name
            )
            t_notprio.append("t__reset__" + name +
                             "__success__" + success_i.name)
            net.write(t_reset + "\n")
            success_states = []
            success_res = []
            for effect in success_i.effects:
                t_out += " " + effect.resource.name+"__"+effect.state
                success_states.append(effect.state)
                success_res.append(resources_dict[effect.resource.name])
            if success_i.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, success_i.postcondition)
                if len(solution) > 1:
                    print("More than one solution at postcondition:", success_i.info)
                    print("Not yet supported. Quitting.")
                    quit()
                else:
                    solution = solution[0]
                    for re in solution:
                        elem = re.to_dict()["RSeq"]
                        success_res.append(resources_dict[elem["resource"]])
                        success_states.append(elem["state"])
                        t_out += " " + elem["resource"]+"__"+elem["state"]

            t_notprio.append(
                generate_transits(
                    net,
                    Skillset,
                    tr_type,
                    guard,
                    success_i.effects,
                    success_i.postcondition,
                    in_place,
                    t_name_init,
                    t_out,
                    success_res,
                    success_states,
                )
            )

        # Failures
        for failure_i in skill.failure:
            t_name_init = "t__" + name + "__failure__" + failure_i.name
            add_p(net, "x__" + name + "__failure__" + failure_i.name, 0)
            in_place = PLACE_SUFFIX.EXECUTING+SEP+name
            t_out = " -> x__" + name + "__failure__" + failure_i.name
            t_reset = (
                "tr t__reset__"
                + name
                + "__failure__"
                + failure_i.name
                + " x__"
                + name
                + "__failure__"
                + failure_i.name
                + " -> "
                + PLACE_SUFFIX.IDLE+SEP+name
            )
            t_notprio.append("t__reset__" + name +
                             "__failure__" + failure_i.name)
            net.write(t_reset + "\n")
            failure_states = []
            failure_res = []
            for effect in failure_i.effects:
                t_out += " " + effect.resource.name+"__"+effect.state
                failure_states.append(effect.state)
                failure_res.append(effect.resource)
            if failure_i.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, failure_i.postcondition)
                if len(solution) > 1:
                    print("More than one solution at postcondition:", failure_i.info)
                    print("Not yet supported. Quitting.")
                    quit()
                else:
                    solution = solution[0]
                    for re in solution:
                        elem = re.to_dict()["RSeq"]
                        failure_res.append(elem["resource"])
                        failure_states.append(elem["state"])
                        t_out += " " + elem["resource"]+"__"+elem["state"]

            t_notprio.append(
                generate_transits(
                    net,
                    Skillset,
                    tr_type,
                    guard,
                    failure_i.effects,
                    failure_i.postcondition,
                    in_place,
                    t_name_init,
                    t_out,
                    failure_res,
                    failure_states,
                )
            )

        # Interrupt
        if skill.interrupt.info != None:
            t_name_init = "t__" + name + "__interrupt"
            add_p(net, "x__" + name + "__interrupt", 0)
            in_place = PLACE_SUFFIX.EXECUTING+SEP+name
            t_out = " -> x__" + name + "__interrupt"
            t_reset = (
                "tr t__reset__"
                + name
                + "__interrupt"
                + " x__"
                + name
                + "__interrupt -> "
                + PLACE_SUFFIX.IDLE+SEP+name
            )
            t_notprio.append("t__reset__" + name + "__interrupt")
            net.write(t_reset + "\n")
            int_states = []
            int_res = []
            for effect in skill.interrupt.effects:
                t_out += " " + effect.resource.name+"__"+effect.state
                int_states.append(effect.state)
                int_res.append(effect.resource)
            if skill.interrupt.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, skill.interrupt.postcondition)
                if len(solution) > 1:
                    print("More than one solution at postcondition:",
                          skill.interrupt.info)
                    print("Not yet supported. Quitting.")
                    quit()
                else:
                    solution = solution[0]
                    for re in solution:
                        elem = re.to_dict()["RSeq"]
                        int_res.append(elem["resource"])
                        int_states.append(elem["state"])
                        t_out += " " + elem["resource"]+"__"+elem["state"]

            t_notprio.append(
                generate_transits(
                    net,
                    Skillset,
                    tr_type,
                    guard,
                    skill.interrupt.effects,
                    skill.interrupt.postcondition,
                    in_place,
                    t_name_init,
                    t_out,
                    int_res,
                    int_states,
                )
            )

    # Adding priorities to invariant exits
    t_notprio.append(t_events)
    t_notprio_flat = []
    for tr in t_notprio:
        if type(tr) == list:
            for t_i in tr:
                t_notprio_flat.append(t_i)
        else:
            t_notprio_flat.append(tr)
    tr_inv = ""
    t_invariants = sum(t_invariants, [])
    for t_i in t_invariants:
        tr_inv += " " + t_i
    for tr_notprio in t_notprio_flat:
        net.write("pr" + tr_inv + " > " + tr_notprio + "\n")
    return 0


def get_exits(net_name, skills):
    exits = [[] for skill in skills]
    skillnames = [skill.name for skill in skills]
    with open(net_name + ".net", "r") as net_file:
        net_lines = net_file.readlines()
    for line in net_lines:
        if "pl x__" in line:
            exit_name = line[len("pl "): line.index(" (0)")]
            for name in skillnames:
                if "x__" + name + "__failure__" in exit_name:
                    exits[skillnames.index(name)].append(exit_name)
                elif "x__" + name + "__inv_fail__" in exit_name:
                    exits[skillnames.index(name)].append(exit_name)
                elif "x__" + name + "__interrupt" in exit_name:
                    exits[skillnames.index(name)].append(exit_name)
                elif "x__" + name + "__success__" in exit_name:
                    exits[skillnames.index(name)].append(exit_name)
                elif "x__" + name + "__pre_fail__" in exit_name:
                    exits[skillnames.index(name)].append(exit_name)
                elif "x__" + name + "__val_fail" in exit_name:
                    exits[skillnames.index(name)].append(exit_name)
    return exits, skillnames


def create_skill_net_exitless(net, model, t_events):
    Skillset = model.skillsets[0]
    skills = Skillset.skills
    t_notprio = []
    t_invariants = []

    # Preconditions & Start are treated like the events algorithm
    # But with failures on top
    for i, skill in enumerate(skills):  # Event transitions
        name, preconditions, start = skill.name, skill.preconditions, skill.start
        t_name_init = "t__" + name + "__start"
        request_place = PLACE_SUFFIX.REQUEST_START + SEP + name
        add_p(net, PLACE_SUFFIX.IDLE + SEP + name, 1)
        in_place = PLACE_SUFFIX.IDLE + SEP + name + " " + request_place
        add_p(net, PLACE_SUFFIX.EXECUTING + SEP + name, 0)
        t_out = " -> " + PLACE_SUFFIX.EXECUTING + SEP + name
        add_p(net, request_place, 1)
        start_states = []
        start_res = []
        for effect in start:  # Looking at start effects
            t_out += " " + effect.resource.name+"__"+effect.state
            start_states.append(effect.state)
            start_res.append(effect.resource)
        t_out += " " + request_place
        guard = []
        guard_res = []
        guard_failure = []
        guard_name = []
        # guard_fail_grd = []
        for pre in preconditions:
            g_i = pre.guard
            guard.append(g_i)
            # guard_failure.append(pre.failure)
            guard_name.append(pre.name)
            g_i_res = []
            for res in set(g_i.used_resources()):
                g_i_res.append(res)
            guard_res.append(g_i_res)

            tr_type_fail = "ev"
            fail_name = name + "__pre_fail__" + pre.name
            t_name_init_fail = "t__" + name + "__pre_fail__" + pre.name
            in_place_fail = PLACE_SUFFIX.IDLE+SEP+name + " " + request_place
            t_out_fail = " -> " + PLACE_SUFFIX.IDLE+SEP+name + " " + request_place

            t_notprio.append(
                generate_transits(
                    net,
                    Skillset,
                    tr_type_fail,
                    ResourceNotExpr(g_i),
                    [],
                    None,
                    in_place_fail,
                    t_name_init_fail,
                    t_out_fail,
                    [],
                    [],
                )
            )

        tr_type = "pre"
        t_notprio.append(
            generate_transits(
                net,
                Skillset,
                tr_type,
                guard,
                start,
                None,
                in_place,
                t_name_init,
                t_out,
                start_res,
                start_states,
            )
        )

        # Validate
        tr_val_fail = "t__"+name+"__val_fail__0"
        net.write("tr "+tr_val_fail+" "+PLACE_SUFFIX.IDLE+SEP+name + " " +
                  request_place+" -> "+PLACE_SUFFIX.IDLE+SEP+name + " "+request_place+"\n")
        t_notprio.append(tr_val_fail)

        # Invariants & Exits (int,succ,fail) are made just like events.
        # However, Invariants failures transitions have a higher priority above
        # everything else, as the skill exit needs to be instant.

        name, invariants = skill.name, skill.invariants
        guard = []
        guard_res = []
        guard_failure = []
        guard_name = []
        # guard_fail_grd = []
        for inv in invariants:
            g_i = inv.guard
            guard.append(g_i)
            guard_failure.append(inv.failure)
            guard_name.append(inv.name)
            g_i_res = []
            fail_states = []
            fail_res = []
            for res in set(g_i.used_resources()):
                g_i_res.append(res)
            guard_res.append(g_i_res)

            if inv.failure != []:
                tr_type_fail = "ev"
                fail_name = name + "__inv_fail__" + inv.name
                t_name_init = "t__" + name + "__inv_fail__" + inv.name

                in_place = PLACE_SUFFIX.EXECUTING+SEP+name
                t_out = " -> " + PLACE_SUFFIX.IDLE+SEP+name
                for fail in inv.failure:
                    t_out += " " + str(fail.resource)+"__"+fail.state
                    fail_states.append(fail.state)
                    fail_res.append(fail.resource)

                t_invariants.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type_fail,
                        ResourceNotExpr(g_i),
                        inv.failure,
                        None,
                        in_place,
                        t_name_init,
                        t_out,
                        fail_res,
                        fail_states,
                    )
                )
            else:
                tr_type_fail = "ev"
                fail_name = name + "__inv_fail__" + inv.name
                t_name_init = "t__" + name + "__inv_fail__" + inv.name

                in_place = PLACE_SUFFIX.EXECUTING+SEP+name
                t_out = " -> " + PLACE_SUFFIX.IDLE+SEP+name
                for fail in inv.failure:
                    t_out += " " + str(fail.resource)+"__"+fail.state
                    fail_states.append(fail.state)
                    fail_res.append(fail.resource)

                t_invariants.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type_fail,
                        ResourceNotExpr(g_i),
                        inv.failure,
                        None,
                        in_place,
                        t_name_init,
                        t_out,
                        fail_res,
                        fail_states,
                    )
                )

        tr_type = "pre"

        # Successes
        for success_i in skill.success:
            t_name_init = "t__" + name + "__success__" + success_i.name
            in_place = PLACE_SUFFIX.EXECUTING+SEP+name
            t_out = " -> " + PLACE_SUFFIX.IDLE+SEP+name

            success_states = []
            success_res = []
            for effect in success_i.effects:
                t_out += " " + effect.resource.name+"__"+effect.state
                success_states.append(effect.state)
                success_res.append(effect.resource)
            if success_i.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, success_i.postcondition)
                for sol in solution:
                    postcond_success_res = []
                    postcond_success_states = []
                    postcond_t_out = ""
                    for re in sol:
                        elem = re.to_dict()["RSeq"]
                        #!temp
                        if len(re.used_resources()) > 1:
                            print(re.used_resources())
                            quit()
                        postcond_success_res.append(re.used_resources()[0])
                        postcond_success_states.append(elem["state"])
                        postcond_t_out += " " + \
                            elem["resource"]+"__"+elem["state"]
                    t_notprio.append(
                        generate_transits(
                            net,
                            Skillset,
                            tr_type,
                            guard,
                            success_i.effects,
                            success_i.postcondition,
                            in_place,
                            t_name_init,
                            t_out+postcond_t_out,
                            success_res+postcond_success_res,
                            success_states+postcond_success_states,
                        )
                    )
            else:
                t_notprio.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type,
                        guard,
                        success_i.effects,
                        success_i.postcondition,
                        in_place,
                        t_name_init,
                        t_out,
                        success_res,
                        success_states,
                    )
                )

        # Failures
        for failure_i in skill.failure:
            t_name_init = "t__" + name + "__failure__" + failure_i.name
            in_place = PLACE_SUFFIX.EXECUTING+SEP+name
            t_out = " -> " + PLACE_SUFFIX.IDLE+SEP+name

            failure_states = []
            failure_res = []
            for effect in failure_i.effects:
                t_out += " " + effect.resource.name+"__"+effect.state
                failure_states.append(effect.state)
                failure_res.append(effect.resource)
            if failure_i.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, failure_i.postcondition)
                for sol in solution:
                    postcond_failure_res = []
                    postcond_failure_states = []
                    postcond_t_out = ""
                    for re in sol:
                        elem = re.to_dict()["RSeq"]
                        postcond_failure_res.append(re.used_resources()[0])
                        postcond_failure_states.append(elem["state"])
                        postcond_t_out += " " + \
                            elem["resource"]+"__"+elem["state"]
                    t_notprio.append(
                        generate_transits(
                            net,
                            Skillset,
                            tr_type,
                            guard,
                            failure_i.effects,
                            failure_i.postcondition,
                            in_place,
                            t_name_init,
                            t_out+postcond_t_out,
                            failure_res+postcond_failure_res,
                            failure_states+postcond_failure_states,
                        )
                    )
            else:
                t_notprio.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type,
                        guard,
                        failure_i.effects,
                        failure_i.postcondition,
                        in_place,
                        t_name_init,
                        t_out,
                        failure_res,
                        failure_states,
                    )
                )

        # Interrupt
        if skill.interrupt.info != None:
            t_name_init = "t__" + name + "__interrupt"
            in_place = PLACE_SUFFIX.EXECUTING+SEP+name
            t_out = " -> " + PLACE_SUFFIX.IDLE+SEP+name

            int_states = []
            int_res = []

            for effect in skill.interrupt.effects:
                t_out += " " + effect.resource.name+"__"+effect.state
                int_states.append(effect.state)
                int_res.append(effect.resource)
            if skill.interrupt.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, skill.interrupt.postcondition)
                for sol in solution:
                    postcond_interrupt_res = []
                    postcond_interrupt_states = []
                    postcond_t_out = ""
                    for re in sol:
                        elem = re.to_dict()["RSeq"]
                        #!temp
                        if len(re.used_resources()) > 1:
                            print(re.used_resources())
                            quit()
                        postcond_interrupt_res.append(re.used_resources()[0])
                        postcond_interrupt_states.append(elem["state"])
                        postcond_t_out += " " + \
                            elem["resource"]+"__"+elem["state"]
                    t_notprio.append(
                        generate_transits(
                            net,
                            Skillset,
                            tr_type,
                            guard,
                            skill.interrupt.effects,
                            skill.interrupt.postcondition,
                            in_place,
                            t_name_init,
                            t_out+postcond_t_out,
                            postcond_interrupt_res+int_res,
                            postcond_interrupt_states+int_states,
                        )
                    )
            else:
                t_notprio.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type,
                        guard,
                        skill.interrupt.effects,
                        skill.interrupt.postcondition,
                        in_place,
                        t_name_init,
                        t_out,
                        int_res,
                        int_states,
                    )
                )

    # Adding priorities to invariant exits
    t_notprio.append(t_events)
    t_notprio_flat = []
    for tr in t_notprio:
        if type(tr) == list:
            for t_i in tr:
                t_notprio_flat.append(t_i)
        else:
            t_notprio_flat.append(tr)
    tr_inv = ""
    if t_invariants != []:
        t_invariants = sum(t_invariants, [])
        for t_i in t_invariants:
            tr_inv += " " + t_i
        for tr_notprio in t_notprio_flat:
            net.write("pr" + tr_inv + " > " + tr_notprio + "\n")
    return 0

def create_skill_net_mission(net, model, t_events):
    Skillset = model.skillsets[0]
    skills = Skillset.skills
    t_notprio: dict[str, list] = {skill: [] for skill in skills}
    t_int: dict[str, list] = {skill: [] for skill in skills}
    t_invariants = []

    # Preconditions & Start are treated like the events algorithm
    # But with failures on top
    for i, skill in enumerate(skills):  # Event transitions
        name, preconditions, start = skill.name, skill.preconditions, skill.start
        t_name_init = TRANS_SUFFIX.TRANSITION + SEP + name + SEP + "start"
        idle_place = PLACE_SUFFIX.IDLE + SEP + name
        add_p(net, idle_place, 1)
        request_start_place = PLACE_SUFFIX.REQUEST_START+SEP+name
        add_p(net, request_start_place, 0)
        in_place = idle_place + " " + request_start_place
        running_place = PLACE_SUFFIX.EXECUTING+SEP+name
        add_p(net, running_place, 0)
        t_out = " -> "+running_place

        start_states = []
        start_res = []
        for effect in start:  # Looking at start effects
            t_out += " " + effect.resource.name+SEP+effect.state
            start_states.append(effect.state)
            start_res.append(effect.resource)
        guard = []
        guard_res = []
        guard_failure = []
        guard_name = []
        # guard_fail_grd = []
        for pre in preconditions:
            g_i = pre.guard
            guard.append(g_i)
            # guard_failure.append(pre.failure)
            guard_name.append(pre.name)
            g_i_res = []
            for res in set(g_i.used_resources()):
                g_i_res.append(res)
            guard_res.append(g_i_res)

            tr_type_fail = "ev"
            fail_name = name + SEP + "pre_fail" + SEP + pre.name
            t_name_init_fail = TRANS_SUFFIX.TRANSITION + \
                SEP + name + SEP + "pre_fail" + SEP + pre.name
            add_p(net, "x" + SEP + name + SEP + "pre_fail" + SEP + pre.name, 0)
            in_place_fail = idle_place + " " + request_start_place
            t_out_fail = " -> x" + SEP + name + SEP + \
                "pre_fail" + SEP + pre.name + " " + idle_place

            t_notprio[skill] += generate_transits(
                net,
                Skillset,
                tr_type_fail,
                ResourceNotExpr(g_i),
                [],
                None,
                in_place_fail,
                t_name_init_fail,
                t_out_fail,
                [],
                [],
            )

        tr_type = "pre"
        t_notprio[skill] += generate_transits(
            net,
            Skillset,
            tr_type,
            guard,
            start,
            None,
            in_place,
            t_name_init,
            t_out,
            start_res,
            start_states,
        )

        # Validate
        tr_val_fail = TRANS_SUFFIX.TRANSITION + SEP+name+SEP + "val_fail"+SEP+"0"
        x_fail = "x" + SEP + name + SEP + "val_fail"
        add_p(net, x_fail, 0)
        net.write("tr "+tr_val_fail+" "+idle_place+" " +
                  request_start_place+" -> "+x_fail+" "+idle_place+"\n")
        t_notprio[skill].append(tr_val_fail)

        # Already running
        already_running_tr = TRANS_SUFFIX.TRANSITION + \
            SEP + name + SEP + "already_running" + SEP + "0"
        already_running_exit = PLACE_SUFFIX.RESULT+SEP+name+SEP+"already_running"
        add_p(net, already_running_exit, 0)
        net.write("tr "+already_running_tr+" "+running_place +
                  " "+request_start_place+" -> "+running_place+" "+already_running_exit+"\n")

        # Invariants & Exits (int,succ,fail) are made just like events.
        # However, Invariants failures transitions have a higher priority above
        # everything else, as the skill exit needs to be instant.

        name, invariants = skill.name, skill.invariants
        guard = []
        guard_res = []
        guard_failure = []
        guard_name = []
        # guard_fail_grd = []
        for inv in invariants:
            g_i = inv.guard
            guard.append(g_i)
            guard_failure.append(inv.failure)
            guard_name.append(inv.name)
            g_i_res = []
            fail_states = []
            fail_res = []
            for res in set(g_i.used_resources()):
                g_i_res.append(res)
            guard_res.append(g_i_res)

            if inv.failure != []:
                tr_type_fail = "ev"
                fail_name = name + SEP + "inv_fail" + SEP + inv.name
                t_name_init = TRANS_SUFFIX.TRANSITION + SEP + \
                    name + SEP + "inv_fail" + SEP + inv.name
                add_p(net, "x" + SEP + name + SEP +
                      "inv_fail" + SEP + inv.name, 0)
                in_place = running_place
                t_out = " -> x" + SEP + fail_name + " "+idle_place
                for fail in inv.failure:
                    t_out += " " + str(fail.resource)+SEP+fail.state
                    fail_states.append(fail.state)
                    fail_res.append(fail.resource)

                t_invariants.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type_fail,
                        ResourceNotExpr(g_i),
                        inv.failure,
                        None,
                        in_place,
                        t_name_init,
                        t_out,
                        fail_res,
                        fail_states,
                    )
                )
            else:
                tr_type_fail = "ev"
                fail_name = name + SEP + "inv_fail" + SEP + inv.name
                t_name_init = TRANS_SUFFIX.TRANSITION + SEP + \
                    name + SEP + "inv_fail" + SEP + inv.name
                add_p(net, "x" + SEP + name + SEP +
                      "inv_fail" + SEP + inv.name, 0)
                in_place = running_place
                t_out = " -> x" + SEP + fail_name + " "+idle_place
                for fail in inv.failure:
                    t_out += " " + str(fail.resource)+SEP+fail.state
                    fail_states.append(fail.state)
                    fail_res.append(fail.resource)

                t_invariants.append(
                    generate_transits(
                        net,
                        Skillset,
                        tr_type_fail,
                        ResourceNotExpr(g_i),
                        inv.failure,
                        None,
                        in_place,
                        t_name_init,
                        t_out,
                        fail_res,
                        fail_states,
                    )
                )

        tr_type = "pre"

        # Successes
        for success_i in skill.success:
            t_name_init = TRANS_SUFFIX.TRANSITION + SEP + \
                name + SEP + "success" + SEP + success_i.name
            add_p(net, "x" + SEP + name + SEP +
                  "success" + SEP + success_i.name, 0)
            in_place = running_place
            t_out = " -> x" + SEP + name + SEP + "success" + \
                SEP + success_i.name + " "+idle_place
            success_states = []
            success_res = []
            for effect in success_i.effects:
                t_out += " " + effect.resource.name+SEP+effect.state
                success_states.append(effect.state)
                success_res.append(effect.resource)
            if success_i.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, success_i.postcondition)
                for sol in solution:
                    postcond_success_res = []
                    postcond_success_states = []
                    postcond_t_out = ""
                    for re in sol:
                        elem = re.to_dict()["RSeq"]
                        #!temp
                        if len(re.used_resources()) > 1:
                            print(re.used_resources())
                            quit()
                        postcond_success_res.append(re.used_resources()[0])
                        postcond_success_states.append(elem["state"])
                        postcond_t_out += " " + \
                            elem["resource"]+"__"+elem["state"]
                    t_notprio[skill] += generate_transits(
                        net,
                        Skillset,
                        tr_type,
                        guard,
                        success_i.effects,
                        success_i.postcondition,
                        in_place,
                        t_name_init,
                        t_out+postcond_t_out,
                        success_res+postcond_success_res,
                        success_states+postcond_success_states,
                    )
            else:
                t_notprio[skill] += generate_transits(
                    net,
                    Skillset,
                    tr_type,
                    guard,
                    success_i.effects,
                    success_i.postcondition,
                    in_place,
                    t_name_init,
                    t_out,
                    success_res,
                    success_states,
                )

        # Failures
        for failure_i in skill.failure:
            t_name_init = TRANS_SUFFIX.TRANSITION + SEP + \
                name + SEP + "failure" + SEP + failure_i.name
            add_p(net, "x" + SEP + name + SEP +
                  "failure" + SEP + failure_i.name, 0)
            in_place = running_place
            t_out = " -> x" + SEP + name + SEP + "failure" + \
                SEP + failure_i.name + " " + idle_place
            failure_states = []
            failure_res = []
            for effect in failure_i.effects:
                t_out += " " + effect.resource.name+SEP+effect.state
                failure_states.append(effect.state)
                failure_res.append(effect.resource)
            if failure_i.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, failure_i.postcondition)
                for sol in solution:
                    postcond_failure_res = []
                    postcond_failure_states = []
                    postcond_t_out = ""
                    for re in sol:
                        elem = re.to_dict()["RSeq"]
                        postcond_failure_res.append(re.used_resources()[0])
                        postcond_failure_states.append(elem["state"])
                        postcond_t_out += " " + \
                            elem["resource"]+"__"+elem["state"]
                    t_notprio[skill] += generate_transits(
                        net,
                        Skillset,
                        tr_type,
                        guard,
                        failure_i.effects,
                        failure_i.postcondition,
                        in_place,
                        t_name_init,
                        t_out+postcond_t_out,
                        failure_res+postcond_failure_res,
                        failure_states+postcond_failure_states,
                    )
            else:
                t_notprio[skill] += generate_transits(
                    net,
                    Skillset,
                    tr_type,
                    guard,
                    failure_i.effects,
                    failure_i.postcondition,
                    in_place,
                    t_name_init,
                    t_out,
                    failure_res,
                    failure_states,
                )

        # Interrupt
        if skill.interrupt.info != None:
            request_int_place = PLACE_SUFFIX.REQUEST_INTERRUPT+SEP+name
            add_p(net, request_int_place, 0)
            t_name_init = TRANS_SUFFIX.TRANSITION + SEP + name + SEP + "interrupt"
            add_p(net, "x" + SEP + name + SEP + "interrupt", 0)
            in_place = running_place + " " + request_int_place
            t_out = " -> x" + SEP + name + SEP + "interrupt" + " " + idle_place
            int_states = []
            int_res = []
            for effect in skill.interrupt.effects:
                t_out += " " + effect.resource.name+SEP+effect.state
                int_states.append(effect.state)
                int_res.append(effect.resource)
            if skill.interrupt.postcondition != None:
                solution = expr_all_solutions(
                    Skillset, skill.interrupt.postcondition)
                t_int[skill] = []
                for sol in solution:
                    postcond_interrupt_res = []
                    postcond_interrupt_states = []
                    postcond_t_out = ""
                    for re in sol:
                        elem = re.to_dict()["RSeq"]
                        #!temp
                        if len(re.used_resources()) > 1:
                            print(re.used_resources())
                            quit()
                        postcond_interrupt_res.append(re.used_resources()[0])
                        postcond_interrupt_states.append(elem["state"])
                        postcond_t_out += " " + \
                            elem["resource"]+"__"+elem["state"]
                    t_int[skill] += generate_transits(
                        net,
                        Skillset,
                        tr_type,
                        guard,
                        skill.interrupt.effects,
                        skill.interrupt.postcondition,
                        in_place,
                        t_name_init,
                        t_out+postcond_t_out,
                        postcond_interrupt_res+int_res,
                        postcond_interrupt_states+int_states,
                    )
            else:
                t_int[skill] = generate_transits(
                    net,
                    Skillset,
                    tr_type,
                    guard,
                    skill.interrupt.effects,
                    skill.interrupt.postcondition,
                    in_place,
                    t_name_init,
                    t_out,
                    int_res,
                    int_states,
                )

            # Not running
            claim_int_request_tr = TRANS_SUFFIX.TRANSITION + \
                SEP + name + SEP + "not_running" + SEP + "0"
            x_not_running = "x" + SEP + name + SEP + "not_running"
            add_p(net, x_not_running, 0)
            net.write("tr "+claim_int_request_tr+" "+idle_place +
                      " "+request_int_place+" -> "+idle_place+" "+x_not_running+"\n")

            for tr in t_notprio[skill]:
                if "__start__" in tr or "__val_fail__" in tr:
                    net.write("pr "+claim_int_request_tr+" > "+tr+"\n")

    # Adding priorities to invariant exits
    t_notprio["events"] = t_events
    t_notprio_flat = sum(t_notprio.values(), [])
    t_ints = sum(t_int.values(), [])
    t_invariants = sum(t_invariants, [])
    tr_prio_inv = " ".join(t_invariants)
    tr_prio_int = " ".join(t_ints)

    # invariants priority over others
    if t_invariants != []:
        for tr_notprio in t_notprio_flat:
            net.write("pr " + tr_prio_inv + " " +
                      tr_prio_int + " > " + tr_notprio + "\n")
        for skill in skills:
            for tr in t_int[skill]:
                net.write("pr " + tr_prio_inv + " > " + tr + "\n")
    else:
        for tr_notprio in t_notprio_flat:
            net.write("pr " + tr_prio_int + " > " + tr_notprio + "\n")

    return 0
