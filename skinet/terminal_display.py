from robot_language import *
from os.path import join, dirname


def show_mission_help():
    """
    Shows help for SkiNet Mission.
    """
    print()
    tool.start_phase("SkiNet, Help!")
    print(
        "\n--- SkiNet Mission Net Generation and Verification ---\n",
        "\n",
        "This tool provides a graphical and analytical tool to build and verify the feasability of a skill-based missions.\n",
        "Forbidden words in skills/resources names:  _retry_, _ite_, _x_.\n",
        "\n",
        "Usage: python3 skinet.py infile.rl [options]"
        "\n",
        "Options:\n",
        " -h   : shows this message.\n",
        " -d   : debug mode, shows how mission and sequences were parsed.\n",
        " -ops : show the available combinations/operations.\n",
        " -set : show the available skills/resources/events of the skillset.\n",
        ' -f   : specify a mission (ie. -f "skill1 -> skill2").\n',
        "\n",
        " --load mission_file.rlm:mission_name : load a mission or sequence from a .rlm file.\n",
        "\n",
        "Developper: Baptiste Pelletier (ONERA-DTIS)\n",
        "Contact:    baptiste.pelletier@onera.fr\n",
        "\n",
        "Thank you for using SkiNet!! Have fun!\n",
    )
    quit()


def show_map_help():
    """
    Shows help for SkiNet Map.
    """
    print()
    tool.start_phase("SkiNet, Help!")
    print(
        "\n--- SkiNet Properties Map Generation ---\n",
        "\n",
        "This tool provides generates a compressed .json file from a skillset and a ktz file.\n",
        "It contains a hashmap of the properties valid on the markings of the skillset net.\n",
        "Depending on the size of the ktz file, generation and compression of the map can take a while.\n",
        "\n",
        "Usage: python3 skinet.py infile.rl [options]"
        "\n",
        "Options:\n",
        " -h   : shows this message.\n",
        "\n",
        "Developper: Baptiste Pelletier (ONERA-DTIS)\n",
        "Contact:    baptiste.pelletier@onera.fr\n",
        "\n",
        "Thank you for using SkiNet!! Have fun!\n",
    )
    quit()


def show_live_help():
    """
    Shows help for SkiNet Live.
    """
    print()
    tool.start_phase("SkiNet, Help!")
    print(
        "\n--- SkiNet Live Net Runtime Verification ---\n",
        "\n",
        "This tool provides a live Petri net player to preview the execution of the net.\n",
        "Manual mode is loaded directly from Python and skinet_live.py file.\n",
        "Live mode is loaded via ROS and the skinet_live package, given in /src.\n",
        "\n",
        "Usage (manual mode): python3 skinet_live.py infile.rl\n"
        "Usage (live mode): ros2 run skinet_live skinet_live infile.rl [options]\n"
        "\n",
        "Options:\n",
        " -h   : shows this message.\n",
        " -l   : loads a .json file containing the properties map for runtime monitoring.\n",
        " -m   : (live mode only) enter the name of the skillset manager node.\n",
        "\n",
        "Developper: Baptiste Pelletier (ONERA-DTIS)\n",
        "Contact:    baptiste.pelletier@onera.fr\n",
        "\n",
        "Thank you for using SkiNet!! Have fun!\n",
    )
    quit()


def show_multi_live_help():
    """
    Shows help for SkiNet Multi Live.
    """
    print()
    tool.start_phase("SkiNet, Help!")
    print(
        "\n--- SkiNet Multi-Robot Live Net Runtime Verification ---\n",
        "\n",
        "This tool provides a live Petri net player to preview the execution of multiple robot nets.\n",
        "Input file is a .rlm (robot language multi) file. Check example for syntax.\n"
        "Manual mode is loaded directly from Python and skinet_multi_live.py file.\n",
        "Live mode is yet to be implemented.\n",
        "\n",
        "Usage (manual mode): python3 skinet_multi_live.py infile.rlm\n"
        "Usage (live mode): TBD\n"
        "\n",
        "Options:\n",
        " -h    : shows this message.\n",
        " -t    : state-space exploration timeout.\n",
        " -safe : blocks transition firing if considered unsafe with regards to safety properties in .rlm file.\n",
        "\n",
        "Developper: Baptiste Pelletier (ONERA-DTIS)\n",
        "Contact:    baptiste.pelletier@onera.fr\n",
        "\n",
        "Thank you for using SkiNet!! Have fun!\n",
    )
    quit()


def mission_terminal_outputs(model, params):
    """
    Shows optional elements such as skillset elements and available operations.
    """
    show_ops, show_set = params

    if show_set:
        tool.start_phase("Skillset")
        name = model.skillsets
        Skillset = model.get_skillset(str(name[0]))
        name_skillset = str(name[0])
        # Create resources
        resources = Skillset.resources
        # Create events
        events = Skillset.events
        # Create skills
        skills = Skillset.skills

        skillnames = [skill.name for skill in skills]
        resnames = [res.name for res in resources]
        eventnames = [evt.name for evt in events]
        max_width = 5 + max(max(len(x) for x in skillnames), len("Skills:"))

        max_length = max(len(skillnames), len(resnames), len(eventnames))
        min_length = min(len(skillnames), len(resnames), len(eventnames))

        for i in range(min_length, max_length + 1):
            if len(skillnames) <= i:
                skillnames.append("")
            if len(resnames) <= i:
                resnames.append("")
            if len(eventnames) <= i:
                eventnames.append("")

        print("---- Skillset " + name_skillset + " ----\n") if show_set else 0
        print(
            "\n".join(
                [
                    "{:<{}} {:{}} {:{}}".format(
                        skill, max_width, res, max_width, ev, max_width
                    )
                    for skill, res, ev in zip(["Skills:"], ["Resources:"], ["Events:"])
                ]
            )
        )

        print(
            "\n".join(
                [
                    "  {:<{}} {:{}} {:{}}".format(
                        skill, max_width, res, max_width, ev, max_width
                    )
                    for skill, res, ev in zip(skillnames, resnames, eventnames)
                ]
            )
        )
        print()

    if show_ops:
        tool.start_phase("SkiNet Mission Operations")
        print("---- Available operations----\n")
        print("  " + "composition    = Sk1 -> Sk2")
        print("  " + "choice         = Sk1 + Sk2")
        print("  " + "concurrent     = Sk1 | Sk2")
        print("  " + "if then else   = Sk1 _ite_ Sk2")
        print("  " + "race           = Sk1 _x_ Sk2")
        print("  " + "retry          = _retry_(Sk)")
        print("  " + "negation       = _err_(Sk)")
        print()


class bcolors:
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def print_color(text, type):
    """
    Print and make it pretty!
    Choices: PURPLE, BLUE, CYAN, GREEN, YELLOW, RED, ENDC, BOLD, UNDERLINE
    """
    if type == "PURPLE":
        print(f"{bcolors.PURPLE}"+text+f"{bcolors.ENDC}")
    if type == "BLUE":
        print(f"{bcolors.BLUE}"+text+f"{bcolors.ENDC}")
    if type == "CYAN":
        print(f"{bcolors.CYAN}"+text+f"{bcolors.ENDC}")
    if type == "GREEN":
        print(f"{bcolors.GREEN}"+text+f"{bcolors.ENDC}")
    if type == "YELLOW":
        print(f"{bcolors.YELLOW}"+text+f"{bcolors.ENDC}")
    if type == "RED":
        print(f"{bcolors.RED}"+text+f"{bcolors.ENDC}")
    if type == "BOLD":
        print(f"{bcolors.BOLD}"+text+f"{bcolors.ENDC}")
    if type == "UNDERLINE":
        print(f"{bcolors.UNDERLINE}"+text+f"{bcolors.ENDC}")


def show_CTL_help():
    """
    Prints help when typing user defined CTL properties.
    """
    print(
        "This tool does not check the syntax of your properties yet. Please use muse by itself in order to check them, thank you.\n",
        "\n",
        "Options:\n",
        " quit   : quit and save the formulae.mmc file.\n",
        " del n  : deletes the last n user properties, only the last one if no n specified.\n",
        " delall : deletes all the user properties.\n",
        " ops    : shows CTL & SkiNet operators.\n",
    )


def show_skinet_mmc():
    """
    Prints available CTL and SkiNet operators for Muse.
    """
    with open(join(dirname(__file__), "skinet.mmc"), "r") as mmc_file:
        for line in mmc_file.readlines():
            print(line[:-1])
