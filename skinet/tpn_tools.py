from time import sleep
from skinet.skillset_net import SkillsetNet
from skinet.terminal_display import print_color
from skinet.mission_tools import remove_number
from skinet.tina_tools import run_script
from os.path import expanduser, exists
from os import makedirs
from shutil import copy


def labelize_ndr(ndr_file, skillnames, eventnames, empty=False, no_event=False, no_reset=False):
    """
    Labelizes the ndr file for merging places with mission net.
    empty: if True, removes tokens from skill idle places e_skill.
    no_event: if True, removes event transitions.
    """
    labelled_ndr = open(ndr_file + "_labelled.ndr", "w")
    evt_tr = ["t__"+ev for ev in eventnames]

    with open(ndr_file+".ndr", "r") as original_file:
        for line in original_file:
            if "p" == line[0]:
                if empty:
                    split_l = line.split(" ")
                    if split_l[3][2:] in skillnames and split_l[4] == "1":
                        split_l[4] = "0"
                        line = " ".join(split_l)
                line = line[:-1] + " " + line.split(" ")[3] + " e \n"
                labelled_ndr.write(line)
            elif no_event:
                if "t" == line[0]:
                    if no_reset and "t__reset__" not in line:
                        split_l = line.split(" ")
                        name = remove_number(split_l[3][2:])
                        if name not in eventnames:
                            labelled_ndr.write(line)
                elif "e" == line[0]:
                    if no_reset and "t__reset__" not in line:
                        split_l = line.split(" ")
                        tr1 = remove_number(split_l[1])
                        tr2 = remove_number(split_l[2])
                        if tr1 not in evt_tr and tr2 not in evt_tr:
                            labelled_ndr.write(line)
            else:
                labelled_ndr.write(line)
    labelled_ndr.close()


def eventless_ndr(ndr_file, skillnames, eventnames, empty=False):
    """
    Removes events from the ndr file.
    empty: if True, removes tokens from skill idle places e_skill.
    """
    eventless_ndr = open(ndr_file + "_eventless.ndr", "w")
    evt_tr = ["t__"+ev for ev in eventnames]

    with open(ndr_file+".ndr", "r") as original_file:
        for line in original_file:
            if "p" == line[0]:
                if empty:
                    split_l = line.split(" ")
                    if split_l[3][2:] in skillnames and split_l[4] == "1":
                        split_l[4] = "0"
                        line = " ".join(split_l)
                eventless_ndr.write(line)
            if "t" == line[0]:
                split_l = line.split(" ")
                name = remove_number(split_l[3][2:])
                if name not in eventnames:
                    eventless_ndr.write(line)
            elif "e" == line[0]:
                split_l = line.split(" ")
                tr1 = remove_number(split_l[1])
                tr2 = remove_number(split_l[2])
                if tr1 not in evt_tr and tr2 not in evt_tr:
                    eventless_ndr.write(line)
    eventless_ndr.close()


def extract_events_tr(net_file, eventnames):
    events_tr = []
    with open(net_file+".net", "r") as net:
        for line in net:
            if "tr " == line[0:3]:
                split_l = line.split(" ")
                name = remove_number(split_l[1][2:])
                if name in eventnames:
                    events_tr.append(line)
    return events_tr


def write_tpn_file(mission_file, path, name_skillset):
    tpn_file = open(mission_file+"_chain.tpn", "w")
    tpn_file.write("load "+path+"/"+name_skillset+"_labelled.ndr\n")
    tpn_file.write("load "+mission_file+".net\n")
    tpn_file.write("chain 2\n")
    tpn_file.close()


def pretty_print_output(output: str):
    output = output[output.index("FALSE")+8:]
    c = 0
    n = 0
    while n != output.count("}"):
        if output[c] == "}":
            n += 1
        c += 1
    output = output[:c].split(" ")
    print("\n".join(output))


def analyse_mission(mission_file, mission_tr, print_out=False):
    print_color("Looking for success: [] -(success)", "CYAN")
    success_formula = "[] -(" + '{' + "__success__.2" + '}' + ")"
    output = run_script("selt "+mission_file +
                        "_chain.ktz -s -f "+'"' + success_formula + '"')
    if "FALSE" in str(output):
        print_color("Success was reached!", "GREEN")
        if print_out:
            print("Steps were:")
            pretty_print_output(str(output))
        print()
    elif "TRUE" in str(output):
        print_color("Mission unsuccesful.", "RED")
        i = 0
        output = "FALSE"
        while "FALSE" in str(output) and i < len(mission_tr):
            formula = "[] -(" + '{' + mission_tr[i][0] + ".2" + '}' + ")"
            output = run_script("selt "+mission_file +
                                "_chain.ktz -s -f "+'"' + formula + '"')
            if "FALSE" in str(output):
                print_color(
                    formula+": "+mission_tr[i][2][0]+"->"+mission_tr[i][1]+" OK", "GREEN")
                i += 1
            elif "TRUE" in str(output):
                print_color(
                    formula+": "+mission_tr[i][2][0]+"->"+mission_tr[i][1]+" FAILED", "RED")
                print("    "+mission_tr[i][2][0]+" was not reached.")


def fuse_nets(multi_rl: dict, safety_prop: list):
    path = expanduser("~")+"/.skinet_nets/skinet_live"
    if not exists(path):
        makedirs(path)
    fused_net = path+"/fused.tpn"
    check_robots = set([])
    for safety in safety_prop:
        for robot in safety["robots"]:
            check_robots.add(robot)
    n = 1
    robot_index = dict()
    with open(fused_net, "w") as tpn_file:
        for robot in multi_rl.keys():
            if robot in check_robots:
                src_path = multi_rl[robot]["ndr_eventless"]
                dst_path = path+"/"+robot+".ndr"
                copy(src_path, dst_path)
                tpn_file.write("load "+dst_path+"\n")
                robot_index[robot] = n
                n += 1
        tpn_file.write("chain "+str(len(check_robots)))
    return path, fused_net, robot_index


def update_net_marking(path: str, robot: str, net: SkillsetNet):
    with open(path+"/"+robot+".ndr", "r") as ndr_file:
        prev_file = ndr_file.readlines()
    with open(path+"/"+robot+".ndr", "w") as ndr_file:
        for line in prev_file:
            if line[0] == "p":
                line = line.split(" ")
                if line[3] in net.marking:
                    line[4] = "1"
                else:
                    line[4] = "0"
                ndr_file.write(" ".join(line))
            else:
                ndr_file.write(line)
