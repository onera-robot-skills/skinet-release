
from itertools import count
import time
from pexpect import spawn
from json import dump, load
from robot_language import *
from skinet.petri_net import PetriNet, Transition, Place
from skinet.petri_net_file import PetriNetFile
from skinet.net_def import PLACE_SUFFIX, SEP, TRANS_SUFFIX, res_token, add_p, create_skill_net_exitless, create_skill_net_mission, write_events_transit
from os import remove as os_remove
import skinet.tina_tools as tina_tools
from skinet.mission_tools import remove_number


class SkillState():
    """
    Possible skill states:\n
    IDLE: Skill can be started\n
    RUNNING: Skill is running\n
    ENDED: Skill has ended (only with exit places)\n
    """
    IDLE = "IDLE"
    RUNNING = "RUNNING"
    ENDED = "ENDED"


class SkillTransition():
    """
    Skill transitions:\n
    start, interrupt, invariant failure, validate failure, success, failure, precondition failure
    """
    SKILL_START = "start"
    SKILL_INTERRUPT = "interrupt"
    SKILL_INV_FAIL = "inv_fail"
    SKILL_VAL_FAIL = "val_fail"
    SKILL_SUCCESS = "success"
    SKILL_FAILURE = "failure"
    SKILL_PRE_FAIL = "pre_fail"


class SkillsetNet(PetriNet, SkillState, SkillTransition):
    """
    A class for the Petri net model of a robot-language skillset.\n
    Simulation of the skillset, as well as analysis and model-checking can be performed with the class methods.\n
    \n
     Optional arguments:\n
        - name: name of the skillset net (useful if dealing with robots with identical skillsets)\n
        - rl_model: input '.rl' file from which the skillset net will be generated.\n
        - net_file: generate from a '.net' file containing a skillset net.\n
        If no name is specified, the name in rl_model or net_file, or a default 'SkN' name will be given.

    Example: robot_SkN = SkillsetNet(name='robot',rl_model='robot.rl')
    """

    __n_counter = count(0)

    def __init__(self, name: str = None, rl_model: str = None, net_file: str = None, suffix: str = None, for_mission: bool = False):

        if type(name) == Model:
            rl_model = name
            name = None

        if name == None:
            self.__name = "SkN"+str(next(self.__n_counter))
        else:
            self.__name = name
            self.__n_counter = next(self.__n_counter)

        self.__suffix = ""
        if suffix:
            self.__suffix = suffix+"__"

        self.places: dict[str, Place] = {}
        self.transitions: dict[str, Transition] = {}

        tool.set_verbosity(2)
        tool.start_phase("Parsing")
        tree, self.__model_init = parse_file(rl_model)
        # Resolve
        if tool.is_ok:
            tool.start_phase("Resolve")
            self.__model_init.resolve()
        self.__model = self.__model_init.skillsets[0]
        if name == None:
            self.__name = str(self.__model)

        self._for_mission = for_mission
        self.__resources = self.get_resources()

        if not net_file:
            self._init_net_file(with_exits=for_mission)
            super().__init__(self.__name, self.__name+".net")
        else:
            super().__init__(self.__name, net_file)
            print("SkillsetNet "+self.__name+" created from file: "+net_file)

        if suffix:
            self.add_net_suffix(suffix)

        self.__net_file = PetriNetFile(net=self)
        self.reset_marking()

        self.__event_request_places: dict[str, Place] = {}
        self.__skill_request_places: dict[str, Place] = {}
        self.__events = self.get_events()
        self.__skills = self.get_skills()

        self.__ktz_file = None
        self.__muse_child = None
        self.__selt_child = None

        self.__event_types: dict[str, str] = {}

        if not net_file:
            os_remove(self.__name+".net")

    def _init_net_file(self, with_exits=False) -> None:
        file_name_ = self.__name+".net"
        temp_file_ = open(file_name_, "w")
        # Create resource states places
        resources = []
        resources_net = []
        net_places = []
        res_places = []
        for res in self.__model.resources:
            states = list(res.states)
            resources.append([res.name, states])
            init_mark = [0 for i in range(len(states))]
            init_mark[states.index(res.initial)] = 1
            resources_net.append(init_mark)

        for res in resources:
            res_place = []
            for state in res[1]:
                net_places.append(res[0]+"__"+state)
                res_place.append(res[0]+"__"+state)
            res_places.append(res_place)

        # Get initial marking of resources
        net_marking_init = []
        for res in resources:
            net_marking_init.append(
                res[0]+"__"+res_token(res, resources, resources_net))
        # Add to net file
        for place in net_places:
            if place in net_marking_init:
                add_p(temp_file_, place, 1)
            else:
                add_p(temp_file_, place, 0)

        if not with_exits:
            t_events = write_events_transit(temp_file_, self.__model_init)
            create_skill_net_exitless(temp_file_, self.__model_init, t_events)
        else:
            t_events = write_events_transit(
                temp_file_, self.__model_init, mark_request=False)
            create_skill_net_mission(temp_file_, self.__model_init, t_events)

        temp_file_.close()

    @property
    def name(self) -> str:
        return self.__name

    @property
    def events(self) -> List[Transition]:
        return self.__events

    @property
    def model(self) -> SkillSet:
        return self.__model

    @property
    def net_file(self) -> PetriNetFile:
        return self.__net_file

    @property
    def ktz_file(self) -> str:
        return self.__ktz_file

    @property
    def resources(self) -> dict[str, list[Place]]:
        return self.__resources

    @property
    def events(self) -> dict[str, list[Transition]]:
        return self.__events

    @property
    def skills(self) -> dict:
        return self.__skills

    @property
    def muse_child(self) -> spawn:
        return self.__muse_child

    @property
    def selt_child(self) -> spawn:
        return self.__selt_child

    @property
    def event_types(self) -> dict[str, str]:
        return self.__event_types

    def __str__(self) -> str:
        """
        Pretty prints the SkillsetNet content.
        """
        i_str = "SkillsetNet "+self.__name+" is: \n"
        m_str = " - Model: "+str(self.__model)+"\n"
        r_str = " - Resources: "+str(self.__resources)+"\n"
        e_str = " - Events: "+str(self.__events)+"\n"
        s_str = " - Skills: "+str(self.__skills)+"\n"
        n_str = "Net: "+self.__name+"\n"
        return i_str+m_str+r_str+e_str+s_str+n_str

    def get_resources(self) -> dict[str, list[str]]:
        """
        Returns a dictionary of resources and their respective states.
        """
        res_places: dict[str, list[str]] = {}
        for res in self.__model.resources:
            res_places[res.name] = []
            for state in res.states:
                res_places[res.name].append(state)
        return res_places

    def get_events(self) -> dict[str, list[Transition]]:
        """
        Returns a dictionary with events transitions.
        """
        events_tr: dict[str, list[Transition]] = {}
        transits = self.transitions.keys()
        for ev in self.__model.events:
            events_tr[ev.name] = []
            for tr in transits:
                if "t__"+self.__suffix+ev.name+"__" in tr:
                    events_tr[ev.name].append(self.transitions[tr])
            self.__event_request_places[ev.name] = self.places[self.__suffix +
                                                               "r__event__"+ev.name]
        return events_tr

    def get_events_tr(self) -> list[Transition]:
        events_tr = []
        for list_tr in self.__events.values():
            events_tr += list_tr
        return events_tr

    def get_skills(self) -> dict:
        """
        Returns a dictionary with skills places and transitions.
        """
        skills_tr = dict()
        sk_actions = [getattr(SkillTransition, elem) for elem in vars(
            SkillTransition) if elem.startswith("SKILL_")]
        for sk in self.__model.skills:
            sk_dict = dict()
            sk_dict["places"] = []

            sk_dict["inputs"] = dict()
            sk_dict["outputs"] = dict()
            for input_ in sk.input:
                sk_dict["inputs"][input_.name] = input_.type
            for output_ in sk.output:
                sk_dict["outputs"][output_.name] = output_.type

            for pl in self.places.keys():
                if pl == self.__suffix+PLACE_SUFFIX.EXECUTING + SEP+sk.name:
                    sk_dict["places"].append(pl)
                    sk_dict["executing"] = pl
                elif pl == self.__suffix+PLACE_SUFFIX.IDLE + SEP + sk.name:
                    sk_dict["places"].append(pl)
                    sk_dict["idle"] = pl
                elif pl == self.__suffix+"r__"+sk.name:
                    sk_dict["request"] = self.__suffix+"r__"+sk.name
                    self.__skill_request_places[sk.name] = self.places[pl]
                elif any(self.__suffix+"x__"+sk.name+"__"+sk_action in pl for sk_action in sk_actions):
                    sk_dict["places"].append(pl)

            for sk_action in sk_actions:
                sk_dict[sk_action] = []

            sk_dict["exits"] = {}
            transit = self.transitions.keys()
            for tr in transit:
                for sk_action in sk_actions:
                    if "t__" + self.__suffix + sk.name + "__" + sk_action in tr:
                        if "__start" not in tr:
                            if "__val_fail__" in tr:
                                exit = "val_fail"
                            else:
                                if self.__suffix != "":
                                    exit = "__".join(tr.split("__")[3:-1])
                                else:
                                    exit = "__".join(tr.split("__")[2:-1])
                            if exit not in sk_dict["exits"].keys():
                                sk_dict["exits"][exit] = [tr]
                            else:
                                sk_dict["exits"][exit].append(tr)
                        sk_dict[sk_action].append(self.transitions[tr])
            skills_tr[sk.name] = sk_dict

            sk_dict["is_interruptible"] = "interrupt" in sk_dict["exits"].keys()

        return skills_tr

    def skill_state(self, skill: str) -> str:
        """
        Get the current state of a skill.
        """
        for pl in self.__skills[skill]["places"]:
            if self.places[pl].marking == 1:
                if pl[:3] == self.__suffix+PLACE_SUFFIX.IDLE + SEP:
                    return self.IDLE
                elif pl[:3] == self.__suffix+PLACE_SUFFIX.EXECUTING + SEP:
                    return self.RUNNING
                elif pl[:3] == self.__suffix+PLACE_SUFFIX.RESULT + SEP:
                    return self.ENDED

    def res_state(self, res: str) -> str:
        """
        Get the current state of a resource.
        """
        for pl in self.__resources[res]:
            if self.places[self.__suffix+res+"__"+pl].marking == 1:
                return pl

    def get_skillset_state(self) -> list[str]:
        """
        Returns the current skillset marking as a list of strings.
        """
        return self.marking

    def is_active(self, elem: str) -> bool:
        if elem in self.__event_request_places.keys():
            return self.is_marked(self.__event_request_places[elem])
        elif elem in self.__skill_request_places.keys():
            return self.is_marked(self.__skill_request_places[elem])
        else:
            raise ValueError(elem+" not in skillset.")

    ######## Event Type ########

    def set_event_type(self, type: str, event: str = None) -> None:
        """
        Sets a new type to an event. Sets to all events if no event is specified.
        """
        if event == None:
            self.__event_types[type] = list(self.__events.keys())
        else:
            if type not in self.__event_types.keys():
                self.__event_types[type] = set([])
            self.__event_types[type].add(event)

    def save_event_types(self, json_file: str = "event_types.json") -> None:
        """
        Saves event types as a json file.
        """
        for type in self.__event_types.keys():
            self.__event_types[type] = list(self.__event_types[type])
        with open(self.__name+"__"+json_file, "w") as jfile:
            dump(self.__event_types, jfile, indent=4, sort_keys=True)

    def load_event_types(self, json_file: str = "event_types.json") -> str:
        """
        Loads event types from a json file. 
        """
        with open(json_file, "r") as jfile:
            self.__event_types = load(jfile)
        for type in self.__event_types.keys():
            self.__event_types[type] = set(self.__event_types[type])

    ######## Skillset Actions ########

    def can_fire_skill(self, skill: str, action: str = "start", suffix: str = "") -> bool:
        """
        Checks if a skill action can be made, with:
            - action: start, interrupt, success...
            - suffix: name of the action (for successes, failures...)
        Transition can fired if True is returned, False otherwise.
        """
        firable_tr = self.firable()
        suffix = "__"+suffix if suffix != "" else ""
        goal_tr = "t__"+self.__suffix+skill+"__"+action+suffix
        for tr in firable_tr:
            if goal_tr in tr.name:
                return True
        return False

    def can_fire_event(self, event: str) -> bool:
        """
        Checks if an event can be fired.
        Transition can be fired if True is returned, False otherwise.
        """
        firable_tr = self.firable()
        goal_tr = "t__"+self.__suffix+event+"__"
        for tr in firable_tr:
            if goal_tr in tr.name:
                return True
        return False

    def fire_skill(self, skill: str, action: str = "start", suffix: str = "") -> bool:
        """
        Fires a skill, with:
            - action: start, interrupt, success...
            - suffix: name of the action (for successes, failures...)
        Transition was successfully fired if True is returned, False otherwise.
        """
        firable_tr = self.firable()
        goal_tr = "t__"+self.__suffix+skill+"__"+action+"__"+suffix
        for tr in firable_tr:
            if goal_tr in tr.name:
                self.fire(tr)
                # for pl in tr.inputs:
                #     self.marking.remove(pl)
                #     self.places[pl].marking -= 1
                # for pl in tr.outputs:
                #     self.marking.append(pl)
                #     self.places[pl].marking += 1
                self.history.append(tr)
                return True
        return False

    def fire_event(self, event: str) -> bool:
        """
        Fires an event.
        Transition was successfully fired if True is returned, False otherwise.
        """
        firable_tr = self.firable()
        goal_tr = "t__"+self.__suffix+event+"__"
        for tr in firable_tr:
            if goal_tr in tr.name:
                self.fire(tr)
                # for pl in tr.inputs:
                #     self.marking.remove(pl)
                #     self.places[pl].marking -= 1
                # for pl in tr.outputs:
                #     self.marking.append(pl)
                #     self.places[pl].marking += 1
                self.history.append(tr)
                return True
        return False

    def execute_path(self, path):
        """
        Fires a series of skills actions and/or events.
        Returns True if the whole sequence has been fired successfully.
        Otherwise, returns False, with no actions fired.
        """
        result = False
        n_tr = 0
        if type(path) == dict:
            path = list(path.values())
        if type(path) == list:
            for transition in path:
                skill_action = transition["action"]
                if skill_action["type"] == "event":
                    name = skill_action["name"]
                    print("Triggering: ", name)
                    result = self.fire_event(name)
                    if not result:
                        self.undo(n_tr)
                        break
                    else:
                        n_tr += 1
                else:
                    name = skill_action["name"]
                    action = skill_action["action"]
                    suffix = skill_action["suffix"]
                    print("Firing: ", name, action, suffix)
                    result = self.fire_skill(name, action, suffix)
                    if not result:
                        self.undo(n_tr)
                        break
                    else:
                        n_tr += 1
        else:
            raise TypeError("Path of type "+type(path)+" cannot be used.")
        return result

    ######## Skillset Modifications ########

    def _disable(self, elem: str):
        request_place = ""
        if elem in self.__event_request_places.keys():
            request_place = self.__event_request_places[elem]
        elif elem in self.__skill_request_places.keys():
            request_place = self.__skill_request_places[elem]
        else:
            raise ValueError(elem+" not in skillset.")

        self.unmark(request_place)

    def _enable(self, elem: str):
        request_place = ""
        if elem in self.__event_request_places.keys():
            request_place = self.__event_request_places[elem]
        elif elem in self.__skill_request_places.keys():
            request_place = self.__skill_request_places[elem]
        else:
            raise ValueError(elem+" not in skillset.")

        self.mark(request_place)

    def remove_event(self, event=None, ktz_reload: bool = True) -> None:
        """
        Remove a specific event by unmarking its request place.
        If no event is specified, removes all events.
         - ktz_reload: choose whether to reload muse and selt (default True).
        """
        if event == None:
            events = list(self.__events.keys())
        elif type(event) == str:
            events = [event]
        else:
            events = list(event)
        for event in events:
            self._disable(event)

        self.__net_file.update_marking()
        if self.__muse_child != None and ktz_reload:
            self.start_muse_child()
        if self.__selt_child != None and ktz_reload:
            self.start_selt_child()

    def remove_skill(self, skill=None, ktz_reload: bool = True) -> None:
        """
        Remove a specific skill by unmarking its request place.
        If no skill is specified, removes all skills.
         - ktz_reload: choose whether to reload muse and selt (default True).
        """
        if skill == None:
            skills = list(self.__skills.keys())
        elif type(skill) == str:
            skills = [skill]
        else:
            skills = list(skill)
        for skill in skills:
            self._disable(skill)

        self.__net_file.update_marking()
        if self.__muse_child != None and ktz_reload:
            self.start_muse_child()
        if self.__selt_child != None and ktz_reload:
            self.start_selt_child()

    def restore_event(self, event=None, ktz_reload: bool = True) -> None:
        """
        Restore a specific event and rewrites the net.
        If no event is specified, restores all events.
         - ktz_reload: choose whether to reload muse and selt (default True).
        """
        dt = time.time_ns()
        if event == None:
            events = list(self.__events.keys())
        elif type(event) == str:
            events = [event]
        else:
            events = list(event)
        for event in events:
            self._enable(event)

        self.__net_file.update_marking()

        if self.__muse_child != None and ktz_reload:
            self.start_muse_child()
        if self.__selt_child != None and ktz_reload:
            self.start_selt_child()

    def restore_skill(self, skill=None, ktz_reload: bool = True) -> None:
        """
        Restore a specific event and rewrites the net.
        If no event is specified, restores all events.
         - ktz_reload: choose whether to reload muse and selt (default True).
        """
        if skill == None:
            skills = list(self.__skills.keys())
        elif type(skill) == str:
            skills = [skill]
        else:
            skills = list(skill)
        for skill in skills:
            self._enable(skill)

        self.__net_file.update_marking()
        if self.__muse_child != None and ktz_reload:
            self.start_muse_child()
        if self.__selt_child != None and ktz_reload:
            self.start_selt_child()

    def remove_event_type(self, type: str = None, ktz_reload: bool = True) -> None:
        """
        Remove a specific event type and rewrites the net.
        If no event type is specified, removes all events.
         - ktz_reload: choose whether to reload muse and selt (default True).
        """
        if type == None:
            self.remove_event(ktz_reload=ktz_reload)
        else:
            try:
                self.remove_event(
                    self.__event_types[type], ktz_reload=ktz_reload)
            except Exception as e:
                print(e)
                if e == KeyError:
                    return None

    def restore_event_type(self, type: str = None, ktz_reload: bool = True) -> None:
        """
        Restores event type that has been deleted. 
        If no event type is specified, restores all events.
         - ktz_reload: choose whether to reload muse and selt (default True).
        """
        if type == None:
            self.restore_event(ktz_reload=ktz_reload)
        else:
            try:
                self.restore_event(
                    self.__event_types[type], ktz_reload=ktz_reload)
            except Exception as e:
                print(e)
                if e == KeyError:
                    return None

    ######## Tina Analysis ########

    def generate_ktz_(self, net_file_path) -> None:
        """
        Generates a Kripke structure from a .net file.
        """
        tina_tools.ktz_make(net_file_path)
        self.__ktz_file = net_file_path[:-3]+"ktz"

    def start_muse_child(self, debug: bool = False) -> None:
        """
        Start a new muse spawn by first creating a temporary .net file, generating a
        kripke structure and loading it into muse.
        Deletes the .net file once the muse process has finished loading.
        """
        if self.__muse_child != None:
            self.__muse_child.sendline("quit;")
            del (self.__muse_child)
        if self.__selt_child != None:
            self.__selt_child.close()

        temp_file_ = self.__net_file.to_net_file()
        self.generate_ktz_(temp_file_)
        # Start process
        self.__muse_child = spawn(
            "muse -prelude "+tina_tools.SKINET_MMC+" -s "+self.__ktz_file, timeout=None)
        self.__muse_child.delaybeforesend = 0.0
        self.__muse_child.delayafterclose = 0.0
        self.__muse_child.delayafterterminate = 0.0
        loaded = False
        while not loaded:
            for line in self.__muse_child:
                line = line.decode("utf-8")
                print(line) if debug else 0
                if "loaded" in str(line):
                    loaded = True
                    break
        os_remove(temp_file_)

    def start_selt_child(self) -> None:
        """
        Start a new muse spawn by first creating a temporary .net file, generating a
        kripke structure and loading it into muse.
        Deletes the .net file once the muse process has finished loading.
        """
        if self.__selt_child != None:
            self.__selt_child.close()
        temp_file_ = self.__net_file.to_net_file()
        self.generate_ktz_(temp_file_)
        # Start process
        self.__selt_child = spawn(
            "selt -s "+self.__ktz_file, timeout=None)
        self.__selt_child.delaybeforesend = 0.0
        loaded = False
        while not loaded:
            for line in self.__selt_child:
                line = line.decode("utf-8")
                if "loaded" in str(line):
                    loaded = True
                    break
        os_remove(temp_file_)

    def close_muse_child(self) -> None:
        """
        Close the muse spawn explicitly.
        """
        if self.__muse_child != None:
            self.__muse_child.close()
            self.__muse_child = None

    def close_selt_child(self) -> None:
        """
        Close the muse spawn explicitly.
        """
        if self.__selt_child != None:
            self.__selt_child.close()
            self.__selt_child = None

    def ask_muse(self, request: str, debug: bool = False) -> List[int]:
        """
        Send CTL request to muse child.
        Returns a list of states as integers (or an integer for transition requests).
        Returns -1 if request contains a typing error.
        """
        if request[-1] != ";":
            request += ";"
        if self.__muse_child == None:
            self.start_muse_child()
        self.__muse_child.sendline(request)
        last_line = ""
        for line in self.__muse_child:
            line = line.decode("utf-8")
            print(line.replace("\n", "")) if debug else 0
            if ("it : states" in last_line) or ("it : transitions" in last_line):
                break
            if "typing error" in line:
                return -1
            last_line = line
        return eval(line)

    def ask_selt(self, request: str, debug: bool = False) -> List[int]:
        """
        Send LTL request to selt child.
        Returns a list of states as integers (or an integer for transition requests).
        Returns -1 if request contains a typing error.
        """
        if request[-1] != ";":
            request += ";"
        if self.__selt_child == None:
            self.start_selt_child()
        self.__selt_child.sendline(request)
        last_line = ""
        for line in self.__selt_child:
            line = line.decode("utf-8")
            if debug:
                print(line.replace("\n", ""))
            if "FALSE" in line:
                return False
            elif "TRUE" in line:
                return True
            if "typing error" in line:
                return -1
            last_line = line
        return eval(line)

    def get_deadlocks(self) -> List[int]:
        """
        Get deadlock states as a list of integers.
        """
        return self.ask_muse("-<T>T;")

    def get_state_id(self, state: List[str] = None):
        """
        Get the integer id of states with the desired marking.
        Returns the current state id if no state is given.
        Returns a list of ids if multiple states are found.
        Returns -1 if none are found.
        """
        if state == None:
            state = self.get_skillset_state()
        return self.ask_muse(" /\ ".join(state))

    def get_path(self, target_state: int, from_state: int = None) -> dict:
        """
        Get the path from target_state to from_state.
        Returns -1 if target state was not found.
        Returns {} if no path was found.
        """
        if from_state == None:
            from_state = self.get_state_id()[0]

        output = tina_tools.run_script(
            "pathto "+str(target_state)+" -from "+str(from_state)+" "+self.__ktz_file)
        if "cannot find target node" in output[1]:
            return -1
        output = output[0][2:-5].split(" ")
        path = dict()
        for i in range(len(output)//2):
            transit = dict()
            transit["id"] = output[2*i]
            transit["action"] = self.get_action(output[2*i+1])
            path[i] = transit

        return path

    def get_skill_path_muse(self, request: str):
        """
        Get the skill paths needed to reach the states in the results of the CTL request.
        """
        if self.__muse_child is not None:
            self.__muse_child.close()
        self.remove_event()

        result = self.ask_muse(request)
        skill_paths = []
        for state in result:
            skill_paths.append(self.get_path(state))
        self.restore_event()
        return skill_paths

    def get_action(self, transition: str) -> dict:
        """
        Gets the action represented by a given net transition as a dictionary.
        """
        action = dict()
        action_name = remove_number(transition[3:])
        for event in self.__events.keys():
            if action_name == event:
                action["type"] = "event"
                action["name"] = event
                action["transition"] = transition
        for skill in self.__skills.keys():
            if skill in action_name:
                action["type"] = "skill"
                action["name"] = skill
                specific_action = action_name.replace(
                    skill+"__", "").split("__") + [""]
                action["action"] = specific_action[0]
                action["suffix"] = specific_action[1]
                action["transition"] = transition
        return action

    def get_dead_transitions(self) -> List[str]:
        """
        Returns all dead transitions.
        """
        dead_tr = []
        for tr in self.transitions.values():
            if self.ask_muse("<"+tr.name+">T") == []:
                dead_tr.append(tr)
        return dead_tr

    def get_skill_liveness(self, skill: str = None):
        """
        Checks if a skill can become inoperable.
        Runs CTL query "-AF EF i_skill".
        If None, checks all skills.
        Returns a dictionary with skill name(s) as key, dead states as values.
        Returns None if the skill doesn't exist.
        """
        if skill != None and skill not in self.__skills.keys():
            return None
        if skill == None:
            skills = self.__skills.keys()
        else:
            skills = [skill]
        dead_states = dict()
        for skill in skills:
            dead_states[skill] = self.ask_muse(
                "-AF EF "+self.__suffix+PLACE_SUFFIX.EXECUTING + SEP+skill)
        return dead_states

    def get_useless_elements(self) -> List[str]:
        """
        Looks at dead transitions and checks whether all transitions of a skillset element are dead.
        This means the element is useless and can never happen in the skillset.
        """
        dead_tr = [tr.name for tr in self.get_dead_transitions()]
        possibly_useless_skills = dict()
        possibly_useless_events = dict()
        definitely_useless = dict()
        definitely_useless["events"] = []
        definitely_useless["skills"] = []

        for transit in dead_tr:
            action = self.get_action(transit)
            if action["type"] == "skill":
                possibly_useless_skills[action["name"]
                                        ] = self.__skills[action["name"]]
            else:
                possibly_useless_events[action["name"]] = [
                    tr for tr in action["transition"]]
        for event, list_tr in possibly_useless_events.items():
            if all(tr in dead_tr for tr in list_tr):
                definitely_useless["events"].append(event)
        for skill, skill_dict in possibly_useless_skills.items():
            for elem, skilltr in skill_dict.items():
                if skilltr != [] and type(skilltr) != set:
                    if type(skilltr) != dict:
                        if type(skilltr[0]) == Transition:
                            if elem == "inv_fail":
                                invariants = dict()
                                for tr in skilltr:
                                    tr_name = tr.name
                                    inv_name = remove_number(
                                        tr_name[tr_name.index("__inv_fail__")+2:])
                                    invariants[inv_name] = [
                                        tr.name for tr in skilltr if inv_name in tr.name]
                                for inv, inv_tr in invariants.items():
                                    if all(tr in dead_tr for tr in inv_tr):
                                        useless_elem = dict()
                                        useless_elem["name"] = skill
                                        useless_elem["suffix"] = inv
                                        definitely_useless["skills"].append(
                                            useless_elem)
                            else:
                                if all(tr.name in dead_tr for tr in skilltr):
                                    useless_elem = dict()
                                    useless_elem["name"] = skill
                                    useless_elem["suffix"] = elem
                                    definitely_useless["skills"].append(
                                        useless_elem)

        return definitely_useless

    ###### Net file manipulation ######

    def to_sub_net_file(self) -> PetriNetFile:
        """
        Creates a net to be used in other nets by adding the skillset name on places and transitions.
        Removes all marking from request places.
        Returns the net file content.
        """
        self.remove_skill()
        return self.__net_file

    def make_labelized_net_file(self, dir: str = "", w_events: bool = False) -> str:
        """
        labelizes a net.
        Adds label to places, as well as the skillset name in front of them.
        Returns the net file name.
        """
        resources_states = []
        for place in self.places.keys():
            place_split = place.split("__")
            if place_split[1] not in ["e", "i", "r", "r_int"]:
                resources_states.append(place)

        with open(dir+"/"+self.__name+"_labelled.net", "w") as labelled_file:
            for line in self.__net_file.to_str().split("\n"):
                if line[:2] == "pl":
                    line = line.split(" ")
                    if line[1] in resources_states:
                        labelled_file.write(
                            line[0]+" "+line[1]+":"+line[1]+" "+line[2]+"\n")
                    else:
                        init_token = "(0)"
                        if ("__r__event__" in line[1] and w_events) or "__i__" in line[1]:
                            init_token = "(1)"
                        labelled_file.write(
                            line[0]+" "+line[1]+":"+line[1]+" "+init_token+"\n")
                elif line[:2] == "tr":
                    line = line.split(" ")
                    if "t__"+self.__suffix+"reset__" not in line[1]:
                        labelled_file.write(" ".join(line)+"\n")
                else:
                    if "t__"+self.__suffix+"reset__" not in line:
                        labelled_file.write(line+"\n")
        return dir+"/"+self.__name+"_labelled.net"

    def play_skillset(self, auto: bool = False, sleep: float = 0):
        """
        Trivial player for the Mission net.
        """
        def dim(s:str):
            return Style.DIM+s+Style.NORMAL
        def bright(s:str):
            return Style.BRIGHT+s+Style.NORMAL
        def danger(s:str):
            return Fore.RED+bright(s)+Style.RESET_ALL
        def warn(s:str):
            return Fore.YELLOW+s+Style.RESET_ALL

        step = 0
        max_len_res   = str(2 + max(len(res) for res in self.__resources.keys()) + max(len(state) for state in self.__resources.values()))
        max_len_state   = str(2 + max([len(state) for states in self.__resources.values() for state in states]))
        max_len_skills = str(2 + max(len(sk) for sk in self.__skills.keys()))
        max_len_events = str(2 + max(len(ev) for ev in self.__events.keys()))

        max_lines = max(len(self.__resources.keys()),len(self.__skills.keys()),len(self.__events.keys()))

        lst_res = list(self.__resources.keys())+[""]*(max_lines-len(self.__resources.keys()))
        lst_skills = list(self.__skills.keys())+[""]*(max_lines-len(self.__skills.keys()))
        lst_events = list(self.__events.keys())+[""]*(max_lines-len(self.__events.keys()))
        
        while self.firable() != []:
            print(" -- "+str(step)+" -- ")
            interesting_pl = []
            for pl in self.marking:
                pl = pl.split("__")
                if PLACE_SUFFIX.IDLE in pl:
                    interesting_pl.append(pl[-1]+".idle")
                elif PLACE_SUFFIX.EXECUTING in pl:
                    interesting_pl.append(pl[-1]+".executing")
                else:
                    if PLACE_SUFFIX.REQUEST_START not in pl:
                        interesting_pl.append(".".join(pl))

            # print("  marking: " +
            #       ", ".join(interesting_pl))
            firable_tr = self.firable()
            print()
            
            prmpt = (" {:<"+max_len_res+"} {:>"+max_len_state+"} | {:<"+max_len_skills+"} {:>4} | {:<"+max_len_events+"} {:>5} ")
            print(prmpt.format("RESOURCES","","SKILLS","","EVENTS",""))
            print(prmpt.format("","","","","",""))
            for line in range(max_lines):
                # res
                res = lst_res[line]
                res_state = "" if res == "" else [x.split(".")[1] for x in interesting_pl if res+"." in x][0]
                # skill
                skill = lst_skills[line]
                skill_state = ""
                if skill != "":
                    skill_state = "IDLE" if skill+".idle" in interesting_pl else "EXEC"
                    if any("t__"+skill+"__pre_fail__" in tr.name for tr in firable_tr):
                        skill_state = dim("PRE!")
                    if any("t__"+skill+"__inv_fail__" in tr.name for tr in firable_tr):
                        skill_state = danger("INV!")
                # event
                event = lst_events[line]
                event_state = ""
                if event != "":
                    event_state = "READY" if any(tr in firable_tr for tr in self.__events[event]) else dim("-----")
                
                print(prmpt.format(lst_res[line],res_state,lst_skills[line],skill_state,lst_events[line],event_state))
            print()

            print("  nb enabled: "+str(len(self.enabled())))
            print(Style.BRIGHT+"  firable: "+Style.NORMAL)
            possible_choices = []
            pre_fails = {skill:[] for skill in self.__skills.keys()}
            for tr in firable_tr:
                i = len(possible_choices)
                tr_name = ".".join(tr.name.split("__")[1:-1])
                # print(" {:>4} - {:<}".format(str(i),tr_name))

                # skill
                if ".start" in tr_name:
                    skill = tr_name.split(".")[0]
                    print(" {:>4} - start {:<}".format(str(i),skill))
                    possible_choices.append(tr)
                elif ".val_fail" in tr_name:
                    pass
                elif ".pre_fail" in tr_name:
                    skill,_,pre = tr_name.split(".")
                    pre_fails[skill].append(pre)
                elif ".inv_fail" in tr_name:
                    skill,_,inv = tr_name.split(".")
                    print((" {:>4} -"+danger(" {:<}: invariant {} not met!")).format(str(i),skill,inv))
                    possible_choices.append(tr)
                # event
                else:
                    if tr_name in self.__events.keys():
                        print(warn(" {:>4} - trigger {:<}".format(str(i),tr_name)))
                    else:
                        print(" {:>4} - {:<}".format(str(i),tr_name))
                    possible_choices.append(tr)
            print()
            for skill in pre_fails.keys():
                if (pre_l:=pre_fails[skill]) != []:
                    print(dim((" - {:<"+max_len_skills+"}: precondition {} not met.").format(skill,", ".join(pre_l))))
            print()
            
            choice = None
            if not auto:
                while not isinstance(choice, int):
                    try:
                        choice = input("?")
                        if choice == "q" or choice == "quit":
                            print("Bye!")
                            exit()
                        else:
                            choice = int(choice)
                    except ValueError:
                        pass
            else:
                choice = 0
            if choice >= 0:
                tr = possible_choices[int(choice)]
                print("     "+bright("<fire>")+" "+".".join(tr.name.split("__")[1:-1])+" "+dim(tr.name))
                print()
                self.fire(tr)
                step += 1
            else:
                if self.history != []:
                    while choice != 0 and self.history != []:
                        print("     <undo>"+self.history[-1].name)
                        self.undo()
                        choice += 1
                        step -= 1
                else:
                    print("     nothing to undo.")
            time.sleep(sleep)
        print(" -- "+str(step)+" -- DEADLOCK --")
        interesting_pl = [pl for pl in self.marking]
        print("  mark: " +
              " ".join(self.marking))
