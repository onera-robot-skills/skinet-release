from os.path import exists, expanduser, join, dirname
from robot_language import *
from skinet.terminal_display import print_color
from skinet.runtime_tools import generate_state_map, get_ktz_state_with_muse
from skinet.tina_tools import SKINET_MMC
from skinet.net_def import PLACE_SUFFIX, SEP
from pexpect import spawn, expect
from skinet.skillset_net import SkillsetNet
from json import load
from gzip import open as g_open
import curses
from time import sleep
from re import findall
from skinet.tina_tools import read_muse_child, run_script


def flatten(xss):
    return [x for xs in xss for x in xs]


def parse_rlf_file(robot_lang_fusion_file: str):
    """
    Loads and parses the robot language fusion file.
    Returns:
     - rl_files: list of .rl files to be parsed
     - robot_names: renaming of robots (useful when using multiple times the same .rl file)
     - map_files: properties map generated with SkiNet Map.
     - map_names: to match with robot_names
    """

    rl_files = []
    robot_names = []
    safety_prop = []
    node_names = dict()
    check_robots = set([])

    with open(robot_lang_fusion_file, "r") as rlf_file:
        rlf_str = rlf_file.readlines()

    for line in rlf_str:
        if "#" in line:
            line = line[:line.index("#")]
        line = line.replace(" ", "")
        line = line.replace('\n', "")
        if line[:len("skillset")] == "skillset":
            line = line[len("skillset"):]
            if ":" in line:
                line = line.split(":")
                robot_names.append(line[0])
                rl_files.append(line[1])
            else:
                robot_names.append("")
                rl_files.append(line)

        elif line[:len("Safety_prop")] == "Safety_prop":
            safety_lines = "".join(rlf_str)
            safety_lines = safety_lines[safety_lines.index("Safety_prop"):]
            safety_lines = safety_lines[safety_lines.index(
                "{")+1:safety_lines.index("}")]
            safety_lines = safety_lines.split('\n')
            for line in safety_lines:
                if "#" in line:
                    line = line[:line.index("#")]
                if line != "":
                    tab = 0
                    while line[tab] == " " and tab < len(line)-1:
                        tab += 1
                    line = line[tab:]
                    if line == " ":
                        line = ""
                if line != "":
                    safety = dict()
                    line = line.split(" ")
                    while "" in line:
                        line.remove("")
                    if ":" not in line or line[0] == ":":
                        print_color(
                            "Ill typed Safety property: format should be 'name : property'.", "RED")
                        quit()
                    safety["name"] = line[0]
                    robots = set([])
                    for elem in line[2:]:
                        if ":" in elem:
                            elem = elem.replace("(", "")
                            elem = elem.replace(")", "")
                            elem = elem.split(":")
                            robots.add(elem[0])
                            check_robots.add(elem[0])
                    safety["robots"] = list(robots)
                    safety["prop"] = " ".join(line[2:])
                    safety_prop.append(safety)
        elif line[:len("Node_names")] == "Node_names":
            nodes_str = "".join(rlf_str)
            nodes_str = nodes_str[nodes_str.index("Node_names"):]
            nodes_str = nodes_str[nodes_str.index("{")+1:nodes_str.index("}")]
            nodes_str = nodes_str.split('\n')
            for line in nodes_str:
                if line != "":
                    line = line.split(" ")
                    while "" in line:
                        line.remove("")
                    line = line[0]
                    if ":" not in line or line[0] == ":":
                        print_color(
                            "Ill typed node name: format should be 'name : node_name'.", "RED")
                        quit()
                    line = line.split(":")
                    node_names[line[0]] = line[1]
    check_robots = list(check_robots)
    return rl_files, robot_names, safety_prop, check_robots, node_names


def create_multi_dict(rl_files: list, robot_names: list, node_names: list, check_robots: list) -> dict:
    """
    Parses robot language files in rl_files and generates a dictionary which contains,
    for each skillset, all the useful elements to be used by SkiNet Live.
    The input robot_names will be there to adjust the naming based on user preferences.
    """
    multi_rl = dict()

    for i, rl_file in enumerate(rl_files):

        dict_i = dict()
        tool.set_verbosity(3)
        tree, model = parse_file(rl_file)
        # Resolve
        if tool.is_ok:
            model.resolve()

        name = model.skillsets
        Skillset = model.get_skillset(str(name[0]))
        name_skillset = str(name[0])
        # Create resources
        resources = Skillset.resources
        # Create events
        events = Skillset.events
        # Create skills
        skills = Skillset.skills

        # Check and create path
        path = expanduser("~")+"/.skinet_nets/"+name_skillset+"_skinet"
        if not exists(path):
            print_color(
                "Path " + path + " does not exist, '.ndr' net file must be created first.", "ORANGE")
            quit()
        else:
            dict_i["path"] = path
            print_color("Skillset SkiNet folder found for " +
                        name_skillset+" at "+path, "GREEN")
            dict_i["net_file"] = path+"/"+name_skillset+".net"
            dict_i["ndr_file"] = path+"/"+name_skillset+".ndr"
            dict_i["ktz"] = path+"/"+name_skillset+".ktz"
            dict_i["map"] = path+"/"+name_skillset+"_map.json"

            dict_i["eventless"] = False
            if exists(path+"/"+name_skillset+"_eventless.ktz"):
                dict_i["eventless"] = True
                dict_i["ktz_eventless"] = path+"/" + \
                    name_skillset+"_eventless.ktz"
                dict_i["ndr_eventless"] = path+"/" + \
                    name_skillset+"_eventless.ndr"
                dict_i["map_eventless"] = path+"/" + \
                    name_skillset+"_eventless_map.json"

        dict_i["skills"] = [skill.name for skill in skills]
        dict_i["resources"] = [res.name for res in resources]
        dict_i["res_states"] = [list(res.states) for res in resources]
        dict_i["events"] = [evt.name for evt in events]

        dict_i["skillset_name"] = name_skillset
        robot_name = name_skillset
        if robot_names[i] != "":
            robot_name = robot_names[i]
        dict_i["name"] = robot_name

        dict_i["net"] = SkillsetNet(name=robot_name, rl_model=rl_file)

        if robot_name not in check_robots:
            if dict_i["eventless"]:
                dict_i["muse_eventless"] = spawn(
                    "muse -prelude "+SKINET_MMC+" -s "+dict_i["ktz_eventless"], timeout=None)
                dict_i["muse_eventless"].delaybeforesend = 0.0

        if robot_name in node_names.keys():
            dict_i["node"] = node_names[robot_name] + \
                "/"+dict_i["skillset_name"]+"_skillset"
        else:
            dict_i["node"] = "/"+dict_i["skillset_name"] + \
                "/"+dict_i["skillset_name"]+"_skillset"

        multi_rl[robot_name] = dict_i

    return multi_rl

# def get_maps(multi_rl): ### deprecated
#     """
#     Loads property maps as asked in the .rlf file. Returns the updated multi_rl dict,
#     as well as a list of all robots that had a map loaded, in has_maps.
#     """
#     has_maps = []
#     for robot_name in multi_rl.keys():
    # with g_open(multi_rl[robot_name]["map"],"rb") as json_file:
    #     ktz_map = load(json_file)
    # state_map = generate_state_map(ktz_map)
    # multi_rl[robot_name]["ktz_map"] = ktz_map
    # multi_rl[robot_name]["state_map"] = state_map
    # if multi_rl[robot_name]["eventless"]:
    #     with g_open(multi_rl[robot_name]["map_eventless"],"rb") as json_file:
    #         ktz_map = load(json_file)
    #     multi_rl[robot_name]["ktz_map_eventless"] = ktz_map
    #     multi_rl[robot_name]["state_map_eventless"] = generate_state_map(ktz_map)
    #     has_maps.append(robot_name)

    # return multi_rl,has_maps


def get_state_from_lines(lines):
    i = 0
    print(lines)
    return 0


def read_selt_output(stdscr, lines, cols, prop, selt):
    col_start = len("Sending: "+prop+"... ")
    is_false = False
    end_reached = False
    selt_output = []
    for line in selt:
        if end_reached and str(line) == "b'\\r\\n'":
            return state
        line = line.decode("utf-8").replace('\n', '').replace('\r', '')
        selt_output.append(line)
        if "typing error" in line:
            stdscr.addstr(lines-4, col_start, line, curses.color_pair(4))
            return state
        elif "TRUE" in line:
            stdscr.addstr(lines-4, col_start, "True", curses.color_pair(3))
            return state
        elif "FALSE" in line:
            stdscr.addstr(lines-4, col_start, "False", curses.color_pair(5))
            is_false = True
        if is_false and "accepting" in line:
            state = get_state_from_lines(selt_output)
            end_reached = True


def write_current_state(prop, marking):
    marking = "(" + " /\ ".join(marking) + ")"
    prop = prop.replace("_now_", marking)
    return prop


def check_skills(prop, skills):
    prop = prop.replace(";", "")
    prop_elem = prop.split(" ")
    for i, elem in enumerate(prop_elem):
        if elem in skills:
            prop_elem[i] = PLACE_SUFFIX.IDLE+SEP+elem
            prop = " ".join(prop_elem)
    return prop+";"


def get_path_to(state, from_state, ktz_file):
    output = run_script("pathto "+str(state)+" -from " +
                        str(from_state)+" "+ktz_file)[0]
    return output[2:-3]


def read_muse_output(stdscr, lines, cols, prop, from_state, muse_child, ktz_file):
    tr_path = [[], []]
    output = read_muse_child(muse_child)
    if output == 0:
        stdscr.addstr(lines-4, 0, " "*(cols-1), curses.color_pair(6))
        stdscr.addstr(lines-4, 0, "Typing error.", curses.color_pair(4))
        return [], -1, tr_path
    output = output[output.index("it : states")+1][1:-1].split(",")
    if output == ['']:
        stdscr.addstr(lines-4, 0, " "*(cols-1), curses.color_pair(6))
        stdscr.addstr(lines-4, 0, "No path found.", curses.color_pair(4))
        return [], -1, tr_path
    elif from_state in output:
        stdscr.addstr(lines-4, 0, " "*(cols-1), curses.color_pair(6))
        stdscr.addstr(lines-4, 0, "Already on state!!", curses.color_pair(6))
        return [], -1, tr_path
    else:
        state = output[0]
        transits = run_script("pathto "+str(state)+" -from " +
                              str(from_state)+" "+ktz_file)[0][3:-6].split(" ")
        tr_path = transits[1::2]
        stdscr.addstr(lines-4, 0, " "*(cols-1), curses.color_pair(6))
        stdscr.addstr(lines-4, 0, "Path found: ", curses.color_pair(6))
        stdscr.addstr(lines-4, len("Path found: "),
                      " > ".join(tr_path), curses.color_pair(1))

    return transits, state, tr_path


""" DEPRECATED
def check_property(stdscr,lines,cols,multi_rl):
    loop = True
    curses.echo()
    prompt = "Property check (robot_name:prop): "
    tr_path = []
    while loop:
        stdscr.addstr(lines-5,0,prompt+(cols-len(prompt)-1)*" ",curses.color_pair(2))
        stdscr.refresh()
        line = stdscr.getstr(lines-5,len(prompt)).decode("utf-8") 
        if str(line) == 'q' or str(line) == '':
            loop = False
        else:
            original_line = line
            if ":" not in line:
                stdscr.addstr(lines-4,0," "*(cols-1),curses.color_pair(6))
                stdscr.addstr(lines-4,0,"Error syntax, missing ':'.",curses.color_pair(4))
            else:
                line = line.split(':')
                if line[0] not in multi_rl.keys() and line[0] != "all":
                    stdscr.addstr(lines-4,0," "*(cols-1),curses.color_pair(6))
                    stdscr.addstr(lines-4,0,"Error syntax, unrecognized robot '"+line[0]+"'.",curses.color_pair(4))
                elif line[0] == "all":
                    original_line = original_line[original_line.index(":")+len("reach "):]
                    elems = findall(r'\(.*?\)', original_line)
                    paths = []
                    for elem in elems:
                        elem = elem[1:-1]
                        if ":" not in elem:
                            stdscr.addstr(lines-4,0," "*(cols-1),curses.color_pair(6))
                            stdscr.addstr(lines-4,0,"Error syntax, missing ':' at: "+elem,curses.color_pair(4))
                        else:
                            robot, prop = elem.split(":")
                            prop = "reach "+prop+";"
                            paths.append(reach(stdscr,lines,cols,multi_rl[robot],prop))
                            stdscr.addstr(lines-4,0," "*(cols-1),curses.color_pair(6))
                            stdscr.addstr(lines-4,0,"Path found!",curses.color_pair(6))
                    tr_path = flatten(paths)
                else:
                    prop = line[1]
                    if "selt" not in multi_rl[line[0]].keys():
                        stdscr.addstr(lines-4,0," "*(cols-1),curses.color_pair(6))
                        stdscr.addstr(lines-4,0,"Error, robot '"+line[0]+"' was not initialized for verification. Check .rlf file.",curses.color_pair(4))
                    else:
                        from_state = 0
                        if "reach " in prop:
                            tr_path = reach(stdscr,lines,cols,multi_rl[line[0]],prop)

    return tr_path
"""


def check_property(stdscr, lines, cols, multi_rl):
    loop = True
    curses.echo()
    prompt = "Property check (robot_name:prop): "
    line = -1
    while loop:
        stdscr.addstr(lines-5, 0, prompt+(cols-len(prompt)-1)
                      * " ", curses.color_pair(2))
        stdscr.refresh()
        line = stdscr.getstr(lines-5, len(prompt)).decode("utf-8")
        if str(line) == 'q' or str(line) == '':
            loop = False
            line = -1
        else:
            if ":" not in line:
                stdscr.addstr(lines-4, 0, " "*(cols-1), curses.color_pair(6))
                stdscr.addstr(
                    lines-4, 0, "Error syntax, missing ':'.", curses.color_pair(4))
            else:
                loop = False
    if line != -1:
        line = line.replace(";", "")
    return line


def reach(stdscr, lines, cols, robot, prop: str) -> str:
    """
    For a property of type reach:prop;
    Returns the transition path to take to reach said property.
    """
    prop = prop[prop.index("reach")+len("reach"):]
    prop = check_skills(prop, robot["skills"])
    stdscr.addstr(lines-4, 0, " "*(cols-1), curses.color_pair(6))
    stdscr.addstr(lines-4, 0, "Looking for path to: " +
                  prop.replace(";", "")+" ... ", curses.color_pair(6))
    stdscr.refresh()
    from_state = get_ktz_state_with_muse(
        robot["muse_eventless"], robot["net"].marking)
    if prop[-1] != ";":
        prop += ";"
    robot["muse_eventless"].sendline(prop)
    transits, state, tr_path = read_muse_output(
        stdscr, lines, cols, prop, from_state, robot["muse_eventless"], robot["ktz_eventless"])
    tr_path_out = []
    for tr in tr_path:
        tr_path_out.append([robot["name"], tr])
    return tr_path_out


def pp_tr(tr: str):
    return ":".join(tr.split("__")[1:-1])


def print_safety(stdscr, lines, cols, line_0, col_start, multi_rl, safety_prop, multi_firable, current_muse, current_forbidden_marking, check_robots, robot_index):
    line = line_0+2
    max_col = max([len(prop["name"]) for prop in safety_prop])

    for prop in safety_prop:
        stdscr.addstr(line, col_start+1, "-" *
                      (len(prop["name"])+6), curses.color_pair(1))
        stdscr.addstr(line+1, col_start+2, prop["prop"], curses.color_pair(0))

        stdscr.addstr(line, col_start+1+3, prop["name"], curses.color_pair(5))
        stdscr.addstr(line, col_start+max_col+10, '⚙', curses.color_pair(5))

        prmpt = " - Unsafe transitions: "
        stdscr.addstr(line+2, col_start+2, prmpt, curses.color_pair(1))
        stdscr.addstr(line+3, col_start+5, "computing...",
                      curses.color_pair(5))
        line += 5
    stdscr.refresh()

    for i in range(line_0+1, line+1):
        stdscr.addstr(i, col_start+1, " "*(cols-col_start),
                      curses.color_pair(1))

    line = line_0+2
    unsafe_tr, safe_tr = check_safe_transitions(
        safety_prop, multi_rl, multi_firable, current_muse, current_forbidden_marking, check_robots, robot_index)
    stdscr.refresh()
    for prop in safety_prop:
        prop["unsafe_tr"] = unsafe_tr[prop["name"]]
        prop["safe_tr"] = safe_tr[prop["name"]]
        prop["status"] = check_safety_prop(prop["prop"], multi_rl)
        stdscr.addstr(line, col_start+1, "-" *
                      (len(prop["name"])+6), curses.color_pair(1))
        stdscr.addstr(line+1, col_start+2, prop["prop"], curses.color_pair(0))

        stdscr.addstr(line, col_start+1+3, prop["name"], curses.color_pair(5))
        stdscr.addstr(line, col_start+max_col+10, '⚙', curses.color_pair(5))

        prmpt = " - Unsafe transitions: "
        stdscr.addstr(line+2, col_start+2, prmpt, curses.color_pair(1))
        stdscr.addstr(line+3, col_start+5, "computing...",
                      curses.color_pair(5))

        if prop["status"]:
            stdscr.addstr(line, col_start+1+3,
                          prop["name"], curses.color_pair(6))
            stdscr.addstr(line, col_start+max_col+10,
                          '✓', curses.color_pair(3))
        else:
            stdscr.addstr(line+2, col_start+2, len(prmpt)
                          * " ", curses.color_pair(1))
            stdscr.addstr(line+2, col_start+2,
                          " - Safe transitions: ", curses.color_pair(1))
            stdscr.addstr(line, col_start+1+3,
                          prop["name"], curses.color_pair(4))
            stdscr.addstr(line, col_start+max_col+10,
                          'x', curses.color_pair(4))

        stdscr.addstr(line+3, col_start+5, " " *
                      (cols-col_start-2-len(prmpt)), curses.color_pair(1))

        if prop["status"]:
            current_line = ""
            i = 0
            for tr in prop["unsafe_tr"]:
                tr = pp_tr(tr)
                current_line += " "+tr
                if len(current_line) > cols-col_start-5:
                    i += 1
                    current_line = tr
                stdscr.addstr(line+i+3, col_start+5,
                              current_line, curses.color_pair(5))
        else:
            current_line = ""
            i = 0
            for tr in prop["safe_tr"]:
                tr = pp_tr(tr)
                current_line += " "+tr
                if len(current_line) > cols-col_start-5:
                    i += 1
                    current_line = tr
                stdscr.addstr(line+i+3, col_start+5,
                              current_line, curses.color_pair(6))

        # stdscr.addstr(line+3,col_start+5," ".join(prop["unsafe_tr"]),curses.color_pair(5))
        line += 5+i


def check_safe_transitions(safety_prop, multi_rl, multi_firable, current_muse, current_forbidden_marking, check_robots, robot_index):
    """
    For each firable transition:
        - Fire in SkillsetNet: tr
        - Get new marking
        - Check safety proposition, save value
        - Revert to original marking in SkillsetNet: undo
    Return the list of all 
    """
    unsafe_tr = dict()
    safe_tr = dict()
    for prop in safety_prop:
        unsafe_tr[prop["name"]] = []
        safe_tr[prop["name"]] = []
    n = 0
    for i, to_fire in enumerate(multi_firable):
        robot = multi_rl[to_fire[0]]
        tr = to_fire[2]
        ev_tr = robot["net"].get_events_tr()
        for t in tr:
            if t.name not in ev_tr:
                markings = dict()
                muse_marking = []
                robot["net"].fire(t)
                markings[robot["name"]] = robot["net"].marking
                for name in multi_rl.keys():
                    if name != robot["name"]:
                        markings[name] = multi_rl[name]["net"].marking
                for robot_name, marking in markings.items():
                    if robot_name in check_robots:
                        for elem in marking:
                            muse_marking.append(
                                "{"+elem+"."+str(robot_index[robot_name])+"}")
                already_unsafe = False
                already_safe = False
                if current_muse != -1:
                    state = get_ktz_state_with_muse(current_muse, muse_marking)
                    if state != -1:
                        for prop in safety_prop:
                            if state in current_forbidden_marking[prop["name"]]:
                                unsafe_tr[prop["name"]].append(
                                    robot["name"]+":"+t.name)
                                already_unsafe = True
                            else:
                                safe_tr[prop["name"]].append(
                                    robot["name"]+":"+t.name)
                                already_safe = True
                for prop in safety_prop:
                    is_safe = check_safety_prop_from_markings(
                        prop["prop"], markings)
                    if not is_safe and not already_unsafe:
                        unsafe_tr[prop["name"]].append(
                            robot["name"]+":"+t.name)
                    elif is_safe and not already_safe:
                        safe_tr[prop["name"]].append(robot["name"]+":"+t.name)
                n += 1
                robot["net"].undo()
    return unsafe_tr, safe_tr


def check_safety_prop(prop: str, multi_rl):

    resource = "i"
    formula = prop.split(" ")
    for i, elem in enumerate(formula):
        front_par, back_par = "", ""
        while "(" in elem or ")" in elem:
            if "(" == elem[0]:
                front_par += "("
                elem = elem[1:]
            if ")" == elem[-1]:
                back_par += ")"
                elem = elem[:-1]
        if ":" in elem:
            if len(elem.split(":")) == 2:
                robot, state = elem.split(":")
                if state in multi_rl[robot]["net"].marking:
                    formula[i] = "True"
                else:
                    formula[i] = "False"
            else:
                robot, resource, state = elem.split(":")
                if resource+"__"+state in multi_rl[robot]["net"].marking:
                    formula[i] = "True"
                else:
                    formula[i] = "False"
            formula[i] = front_par + formula[i] + back_par

    formula = " ".join(formula)

    return eval(formula)


def check_safety_prop_from_markings(prop: str, markings):

    formula = prop.split(" ")
    for i, elem in enumerate(formula):
        front_par, back_par = "", ""
        while "(" == elem[0]:
            front_par += "("
            elem = elem[1:]
        while ")" == elem[-1]:
            back_par += ")"
            elem = elem[:-1]
        if ":" in elem:
            if len(elem.split(":")) == 2:
                robot, state = elem.split(":")
                if state in markings[robot]:
                    formula[i] = "True"
                else:
                    formula[i] = "False"
            else:
                robot, resource, state = elem.split(":")
                if resource+"__"+state in markings[robot]:
                    formula[i] = "True"
                else:
                    formula[i] = "False"
            formula[i] = front_par + formula[i] + back_par

    formula = " ".join(formula)
    return eval(formula)


def check_path_safety(stdscr, lines, cols, multi_rl, tr_path, safety_prop, multi_firable, current_muse, current_forbidden_marking, check_robots, robot_index):
    # check
    safe = True
    blocking_prop = []
    to_undo = dict()
    unsafe_tr = dict()
    for prop in safety_prop:
        unsafe_tr[prop["name"]] = [
            tr.split(":")[1] for tr in prop["unsafe_tr"]]
    for robot in multi_rl.keys():
        to_undo[robot] = 0
    for transit in tr_path:
        robot, tr = transit
        for prop in safety_prop:
            if tr in unsafe_tr[prop["name"]]:
                blocking_prop.append(prop["name"])
        if blocking_prop != []:
            stdscr.addstr(lines-2, 0, "Cannot fire. "+tr+" blocked by: " +
                          ", ".join(blocking_prop), curses.color_pair(4))
            safe = False
            break
        else:
            multi_rl[robot]["net"].fire(tr)
            to_undo[robot] += 1
            multi_firable_temp = []
            n = 0
            for robot in multi_rl.keys():
                firable = multi_rl[robot]["net"].firable()
                multi_firable_temp.append(
                    [robot, [n, n+len(firable)], firable])
                n += len(firable)
            # for prop in safety_prop:
                # unsafe_trs = check_safe_transitions(safety_prop,multi_rl,multi_firable,current_muse,current_forbidden_marking,check_robots,robot_index)
                # unsafe_trs = check_safe_transitions(prop["prop"],multi_rl,multi_firable_temp)
                # unsafe_tr[prop["name"]] = [tr.split(":")[1] for tr in unsafe_trs]

    for robot in multi_rl.keys():
        multi_rl[robot]["net"].undo(to_undo[robot])
    return safe


def print_property(stdscr, lines, cols, max_hist, line_0, multi_rl):
    """
    To be threaded.
    """
    tr_path = []

    while True:

        ### Previous path ###
        if tr_path != []:
            stdscr.addstr(line_0, 0, " "*(cols-1), curses.color_pair(1))
            stdscr.addstr(line_0, 0, "Previous path: ", curses.color_pair(6))
            stdscr.addstr(line_0, len("Previous path: "), " > ".join(
                [tr[0]+":"+tr[1] for tr in tr_path]), curses.color_pair(1))
        line_0 += len("Previous path: " +
                      " > ".join([tr[0]+":"+tr[1] for tr in tr_path]))//cols

        ### Get property ###
        new_tr_path = check_property(stdscr, lines, cols, multi_rl)
        if new_tr_path != []:
            tr_path = new_tr_path
        stdscr.refresh()
