from pexpect import spawn
from time import time
from skinet.terminal_display import print_color, show_CTL_help, show_skinet_mmc
from skinet.mission_tools import remove_number
from skinet.petri_net import PetriNet
from skinet.net_def import PLACE_SUFFIX, SEP
from os.path import join, dirname

SKINET_MMC = join(dirname(__file__), "skinet.mmc")


def read_selt_child(child):
    output = []
    for line in child:
        line = str(line)[2:-5]
        output.append(line)
        if "TRUE" in line or "FALSE" in line:
            break
    return output


def read_muse_child(child):
    output = []
    last_line = ""
    for line in child:
        line = str(line)[2:-5]
        output.append(line)
        if "it : states" in last_line:
            break
        if "typing error" in line:
            return 0
        last_line = line
    return output


def run_script(script, stdin=None):
    """
    Run a script in the console
    """
    import subprocess

    proc = subprocess.Popen(
        ["bash", "-c", script],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE,
    )
    stdout, stderr = proc.communicate()
    # if proc.returncode:
    #     raise ScriptException(proc.returncode, stdout, stderr, script)
    return str(stdout), str(stderr)


class ScriptException(Exception):
    def __init__(self, returncode, stdout, stderr, script):
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr
        Exception().__init__("Error in script")


def ndr_make(net_name):
    """
    Make the .net file and generate the .ndr file (may need cleaning)
    """
    ndr_make_script = (
        "ndrio " + net_name + ".net -NET " + net_name + ".ndr -ndr -graphplace"
    )
    run_script(ndr_make_script)


def ndr_clean(net_name, res_places, net_marking_init, skillnames, exits, eventnames):
    """
    Clean the aesthetic of the net by sorting resources, skills and events
    """
    with open(net_name + ".ndr", "r") as ndr_file:
        ndr_lines = ndr_file.readlines()
    line = 0
    ndr_places = []
    ndr_clean = []
    ndr_clean_in = []
    ndr_clean_out = []
    ndr_skillexit = []
    # Sorting places, transitions and arcs ----------------------
    for i in range(len(ndr_lines)):
        if ndr_lines[i][:2] == "p ":  # Places
            if "x__" in ndr_lines[i]:
                ndr_skillexit.append(ndr_lines[i])
            else:
                ndr_places.append(ndr_lines[i])
        elif ndr_lines[i][:2] == "t ":  # Transitions
            ndr_clean_in.append(ndr_lines[i])
        else:  # Arcs
            ndr_clean_out.append(ndr_lines[i])
    # Resources ----------------------
    offset_x = 600.0
    offset_y = 0.0
    jump_y = 50.0
    next_jump_y = 25.0
    y = offset_y
    x = offset_x
    for i in range(len(res_places)):
        y += next_jump_y
        for j in range(len(res_places[i])):
            ij_res = list(res_places[i])[j]
            for line in ndr_places:
                if " "+ij_res+" " in line:
                    ndr_places.pop(ndr_places.index(line))
                    y += jump_y
                    coord = str(x) + " " + str(y) + " "
                    if ij_res in net_marking_init:
                        mark = " 1 e \n"
                    else:
                        mark = " 0 e \n"
                    ndr_clean.append("p " + coord + ij_res + mark)
    y_end_res = y + jump_y
    # Skills ----------------------
    offset_x = 400.0
    offset_y = 0.0
    jump_y = 60.0
    jump_y_t = 35.0
    next_jump_y = 50.0
    next_jump_y_t = 20.0
    y = offset_y
    y_t = offset_y
    x = offset_x
    x_t = 100
    ndr_clean_tr = []
    y_previous = y
    for i, skill in enumerate(skillnames):
        y_skill = y_previous
        y_t = y_previous
        exit = []
        for line in ndr_skillexit:
            if skill in line:
                exit.append(line[line.index("x__"): line.index(" 0 n")])
        ndr_clean.append("p " + str(x) + " " + str(y_skill) +
                         " " + PLACE_SUFFIX.REQUEST_START + SEP + skill + " 1 n\n")
        y_skill += jump_y
        ndr_clean.append("p " + str(x) + " " + str(y_skill) +
                         " " + PLACE_SUFFIX.IDLE + SEP + skill + " 1 n\n")
        y_skill += jump_y
        ndr_clean.append("p " + str(x) + " " + str(y_skill) +
                         " " + PLACE_SUFFIX.EXECUTING + SEP + skill + " 0 n\n")
        y_skill += jump_y
        if exits != []:
            for ex in exits[i]:
                ndr_clean.append("p " + str(x) + " " +
                                 str(y_skill) + " " + ex + " 0 n\n")
                y_skill += jump_y
        for line in ndr_clean_in:
            if skill in line:
                name_tr = line[line.index("t__"): line.index("c 0") - 1]
                # Skill transitions
                ndr_clean_tr.append(
                    "t "
                    + str(x_t)
                    + " "
                    + str(y_t)
                    + " "
                    + name_tr
                    + " w 0 w n {} ne\n"
                )
                y_t += jump_y_t
        y_previous = max(y_t, y_skill) + next_jump_y
    for skill in skillnames:
        c = 0
        while c < len(ndr_clean_in):
            line = ndr_clean_in[c]
            if skill in line:
                ndr_clean_in.pop(ndr_clean_in.index(line))
            else:
                c += 1
    # External events ----------------------
    next_jump_y = 60
    y_t = y_end_res+100
    x = 600
    event_y_loc = {}
    for event in eventnames:
        ndr_clean.append("p " + str(x+100) + " " + str(y_t) +
                         " " + "r__event__" + event + " 1 e\n")
        event_y_loc[event] = y_t
        y_t += next_jump_y
    y_t = y_end_res+200
    for line in ndr_clean_in:
        name_tr = line[line.index("t__"): line.index("c 0") - 1]
        event = name_tr.split("__")[1]
        ndr_clean_tr.append(
            "t " + str(x) + " " + str(event_y_loc[event]) + " " + name_tr + " ne 0 w n {} ne\n"
        )
    # Write clean net ----------------------
    ndr_new = ndr_clean + ndr_clean_tr + ndr_clean_out
    with open(net_name + ".ndr", "w") as ndr_newfile:
        for line in ndr_new:
            ndr_newfile.write(line)


def ndr_open(net_name):
    """
    Open the .ndr net in the graphical tool nd of Tina
    """
    run_script("nd " + net_name + ".ndr")


def ktz_make(net_name, tpn=False, do_print=False):
    if not tpn:
        if net_name[-4:] == ".net":
            ktz_make_script = "tina -R " + \
                net_name + " " + net_name[:-4] + ".ktz"
        else:
            ktz_make_script = "tina -R " + net_name + ".ndr " + net_name + ".ktz"
    else:
        ktz_make_script = "tina -R " + net_name + ".tpn " + net_name + ".ktz"
    output = run_script(ktz_make_script)
    if do_print:
        print_color(
            "Generating .ktz file... (This may take a few minutes)", "BOLD")
        print("\n".join(output[0][2:-3].split("\\n"))[1:])
        print_color("Success!", "GREEN")
        print()


def Analysis_check(net_name, resources, exits, skillnames, eventnames):
    """
    Start LTL/CTL Check on the .ktz file. ktz file must have been produced first using the -ktz argument.
    """
    print_color("Analysis of the skillset net.", "BOLD")
    LTL_verif(net_name, resources, exits, skillnames, eventnames)


def wait_ktz_loaded(selt_child, muse_child):
    loaded = False
    while not loaded:
        for line in selt_child:
            line = str(line)[2:-5]
            print_color(line, "CYAN")
            if "loaded" in str(line):
                loaded = True
                break

    loaded = False
    while not loaded:
        for line in muse_child:
            line = str(line)[2:-5]
            print_color(line, "CYAN")
            if "loaded" in str(line):
                loaded = True
                break


def LTL_verif(net_name, resources, exits, skillnames, eventnames):

    print_color("Loading .ktz file, this can take a few seconds...", "BOLD")

    # Start processes
    selt_child = spawn("selt "+net_name+".ktz", timeout=None)
    selt_child.delaybeforesend = 0.0
    muse_child = spawn("muse -prelude "+SKINET_MMC+" -s " +
                       net_name+".ktz", timeout=None)
    muse_child.delaybeforesend = 0.0

    wait_ktz_loaded(selt_child, muse_child)

    print_color("Loading successful!", "GREEN")

    choice = input("Basic check: [all/dead/live/safe/skill/deadset/quit]")
    while (choice != "quit"):
        if choice == "":
            choice = "all"
        if choice == "dead" or choice == "all":
            # deadlock
            print_color("\nNo deadlock: [] -dead", "CYAN")
            check_deadlock(net_name, muse_child)
        if choice == "live" or choice == "all":
            # dead transitions
            print_color("\nTransition is never fired: [] - t", "CYAN")
            transits = get_transits(net_name)
            dead_tr = []
            if transits != []:
                for tr in transits:
                    formula = "[] -" + tr + ""
                    selt_child.sendline(formula+";")
                    output = read_selt_child(selt_child)
                    if "TRUE" in output:
                        dead_tr.append(tr)
                if dead_tr == []:
                    print_color("No dead transition found.", "GREEN")
                else:
                    print_color(str(len(dead_tr)) +
                                " dead transitions found.", "BOLD")
                    # check for useless elements
                    useless = get_useless_elements(
                        net_name, dead_tr, skillnames, eventnames)
                    if useless["skills"] == []:
                        print_color("No useless elements in skills.", "GREEN")
                    else:
                        for skill_action in useless["skills"]:
                            print_color(
                                skill_action+" is never triggered.", "YELLOW")
                    if useless["events"] == []:
                        print_color("No useless elements in events.", "GREEN")
                    else:
                        for event_action in useless["events"]:
                            print_color(
                                event_action+" is never triggered.", "YELLOW")
                    if useless["skills"] == [] and useless["events"] == []:
                        print("Dead transitions due to net generation. Ignore them.")
            else:
                print_color(
                    "No transitions in net. Verify the input files.", "YELLOW")
                quit()
        if choice == "safe" or choice == "all":
            # Checking invariants
            print_color(
                "\nNo more than one token per resource and skills: [] (p1 + p2 + ...) = 1", "CYAN")
            for res in resources:
                formula = "[] ( " + res[0]+"__"+res[1][0]
                for state in res[1][1:]:
                    formula += " + " + res[0]+"__"+state
                formula += " = 1 );"
                print_color(res[0], "BOLD")
                print(formula)
                selt_child.sendline(formula)
                output = read_selt_child(selt_child)
                if "TRUE" in output:
                    print_color("TRUE", "GREEN")
                else:
                    print_color("FALSE", "YELLOW")
                    print(output)
            for i, skill in enumerate(skillnames):
                formula = "[] ( "+PLACE_SUFFIX.IDLE+SEP+skill+" + "+PLACE_SUFFIX.EXECUTING+SEP+skill
                if exits != []:
                    for exit in exits[i]:
                        formula += " + " + exit
                formula += " = 1 );"
                print_color(skill, "BOLD")
                print(formula)
                selt_child.sendline(formula)
                output = read_selt_child(selt_child)
                if "TRUE" in output:
                    print_color("TRUE", "GREEN")
                else:
                    print_color("FALSE", "YELLOW")
                    print(output)

            print_color(
                "\nNo more than one token per place: [] -( p1 >= 2 \/ p2 >= 2 \/ ... )", "CYAN")
            place_names = get_places(net_name)
            if place_names != []:
                formula = "[] -(" + place_names[0] + " >= 2"
                for place in place_names:
                    formula += " \/ " + place + " >= 2"
                formula += ")"
                selt_child.sendline(formula+";")
                output = read_selt_child(selt_child)
                if "TRUE" in output:
                    print_color("TRUE", "GREEN")
                else:
                    print_color("FALSE", "YELLOW")
                    print(output)
            else:
                print_color(
                    "No places in net. Verify the input files.", "YELLOW")
                quit()
        if choice == "skill":
            ok = False
            while not ok:
                print_color(
                    "Skill will never eventually die: -AF EF "+PLACE_SUFFIX.EXECUTING+SEP+"skill", "CYAN")
                skill = input("Which skill to check:")
                if skill in skillnames:
                    ok = True
                    transits, final_state = check_skill_deadlock(
                        net_name, skill, muse_child)
                    if final_state != -1:
                        for tr in transits[:-1:2]:
                            print(
                                tr + " -> " + ".".join(transits[transits.index(tr)+1].split("__")[1:-1]))
                        print_color("Final state: "+transits[-1], "BOLD")
                        print()
                else:
                    print_color(
                        "Skill name is wrong, choose between:", "YELLOW")
                    print(skillnames)
        if choice == "all":
            print_color(
                "\nSkill will never eventually die: -AF EF "+PLACE_SUFFIX.EXECUTING+SEP+"skill", "CYAN")
            previous_state = -1
            for skill in skillnames:
                print_color(skill, "BOLD")
                transits, final_state = check_skill_deadlock(
                    net_name, skill, muse_child)
                if final_state != -1 and final_state != previous_state:
                    previous_state = final_state
                    transits[0] = "0"
                    for tr in transits[:-1:2]:
                        print(
                            tr + " -> " + ".".join(transits[transits.index(tr)+1].split("__")[1:-1]))
                    print_color("Final state: "+str(final_state), "BOLD")
                    print()
                elif final_state != -1 and final_state == previous_state:
                    print_color("Identical as previous (" +
                                str(previous_state)+").", "BOLD")
                    print()
        if choice == "deadset" or choice == "all":
            print_color(
                "\nSkillset will never eventually die: -AF EF ("+PLACE_SUFFIX.EXECUTING+SEP+"sk1 \/ "+PLACE_SUFFIX.EXECUTING+SEP+"sk2 \/ ...)", "CYAN")
            script_CTL = "muse -prelude " + \
                SKINET_MMC + \
                " -s " + net_name + ".ktz -f "
            formula = "-AF EF ("+PLACE_SUFFIX.EXECUTING+SEP + skillnames[0]
            if len(skillnames) > 1:
                for skill in skillnames[1:]:
                    formula += " \/ "+PLACE_SUFFIX.EXECUTING+SEP + skill
            formula += ")"
            output = run_script(script_CTL + '"' +
                                formula + '"')[0].split("\\n")
            output = output[output.index("it : states")+1][1:-1].split(",")
            if output == ['']:
                print_color("TRUE", "GREEN")
            else:
                state = output[0]
                print_color("FALSE, PATH TO DEAD STATE IS:", "YELLOW")
                transits = run_script(
                    "pathto "+str(state)+" "+net_name+".ktz")[0][3:-6].split(" ")
                transits[0] = "0"
                for tr in transits[:-1:2]:
                    print(
                        tr + " -> " + ".".join(transits[transits.index(tr)+1].split("__")[1:-1]))
                print()
        if choice != "all":
            choice = input(
                "Basic check: [all/dead/live/safe/skill/deadset/quit]")
        else:
            choice = "quit"
    print("\nBye!")
    return True


def check_deadlock(net_name, muse_child):
    formula = "- <T>T;"
    muse_child.sendline(formula)
    output = read_muse_child(muse_child)
    output = output[output.index("it : states")+1][1:-1].split(", ")
    if output == ['']:
        print_color("TRUE", "GREEN")
        return [], -1
    else:
        print_color("FALSE, "+str(len(output))+" deadlocks found.", "YELLOW")
        loop = True
        while loop:
            choice = input("Would you like check them?[y/n]")
            if choice in ["y", ""]:
                looking_at_deadlocks = True
                while looking_at_deadlocks:
                    state_to_see = input(
                        "Enter state number [0 to "+str(len(output)-1)+", q to quit] : ")
                    if state_to_see != "q":
                        try:
                            state_to_see = int(state_to_see)
                            if state_to_see >= 0 and state_to_see < len(output):
                                transits = run_script(
                                    "pathto "+str(output[state_to_see])+" "+net_name+".ktz")[0][3:-6].split(" ")
                                for tr in transits[:-1:2]:
                                    print(tr + " -> " +
                                          transits[transits.index(tr)+1])
                        except:
                            if ValueError:
                                None
                    else:
                        looking_at_deadlocks = False
                        loop = False
            elif choice == "n":
                loop = False


def check_skill_deadlock(net_name, skill, muse_child):
    formula = "-AF EF "+PLACE_SUFFIX.EXECUTING+SEP+skill+";"
    muse_child.sendline(formula)
    output = read_muse_child(muse_child)
    output = output[output.index("it : states")+1][1:-1].split(", ")
    if output == ['']:
        print_color("TRUE", "GREEN")
        return [], -1
    else:
        state = output[0]
        if state == "0":  # already dead from initial state
            print_color("FALSE, skill can never start!", "RED")
            return [], -1
        else:
            print_color("FALSE, PATH TO DEAD STATE IS:", "YELLOW")
            transits = run_script("pathto "+str(state)+" " +
                                  net_name+".ktz")[0][3:-6].split(" ")
    return transits, state


def get_places(net_name):
    place_names = []
    with open(net_name + ".ndr", "r") as ndr_file:
        ndr_lines = ndr_file.readlines()
    i = 0
    while ndr_lines[i][0] == "p":
        place_names.append(ndr_lines[i].split(" ")[3])
        i += 1
    return place_names


def get_transits(net_name):
    tr_names = []
    with open(net_name + ".ndr", "r") as ndr_file:
        ndr_lines = ndr_file.readlines()
    i = 0
    while ndr_lines[i][0] != "t":
        i += 1
    while ndr_lines[i][0] == "t":
        tr_names.append(ndr_lines[i].split(" ")[3])
        i += 1
    return tr_names


def Struct_check(net_name, Skillset, res_names, res_places, exits, skillnames):
    """
    Structural analysis of the .ndr net. Stored in a .struct file.
    Semiflows are checked to determine if the loops of resources and skills
    behave properly. A missing loop may point out a problem that needs attention.
    """
    skills = Skillset.skills
    time_start = time()
    struct_make_script = "struct -v " + net_name + ".ndr " + net_name + ".struct"
    run_script(struct_make_script)
    print("Structural analysis time:" + str((time() - time_start)) + "s.")
    time_start = time()
    with open(net_name + ".struct", "r") as struct_file:
        struct = struct_file.readlines()
    semiflows_check_start = (
        "P-SEMI-FLOWS GENERATING SET ------------------------------------- \n"
    )
    i_start = struct.index(semiflows_check_start)
    semiflows_check_end = (
        "T-SEMI-FLOWS GENERATING SET ------------------------------------- \n"
    )
    i_end = struct.index(semiflows_check_end)
    struct_semiflows = struct[i_start:i_end]
    struct_semiflows = "".join(struct_semiflows)
    print_color("Invariants analysis:", "BOLD")
    # Checking resources Semiflows (states loop)
    print()
    print_color("Resources invariants:", "CYAN")
    nb_res_ok = 0
    for i, res in enumerate(res_places):
        res_semiflow = " ".join(sorted(res)) + " (1)"
        if res_semiflow in struct_semiflows:
            nb_res_ok += 1
            print("OK " + res_names[i])
        else:
            print_color("/!\ Not found:", res_semiflow, " /!\ \n", "YELLOW")
    if nb_res_ok == len(res_places):
        print_color("\nAll resources invariants ok.", "GREEN")
    # Checking skills Semiflows (e,i,x loop)
    print()
    print_color("Skills invariants:", "CYAN")
    nb_skill_ok = 0
    for i, name in enumerate(skillnames):
        skill_semiflow = PLACE_SUFFIX.IDLE+SEP+name + " "+PLACE_SUFFIX.EXECUTING+SEP+name
        if skill_semiflow in struct_semiflows:
            nb_skill_ok += 1
            print("OK " + name)
        else:
            print_color("/!\ Not found:", res_semiflow, " /!\ \n", "YELLOW")
    if nb_skill_ok == len(skills):
        print_color("\nAll skill invariants ok.\n", "GREEN")
    if nb_skill_ok == len(skills) and nb_res_ok == len(res_places):
        print_color("All semiflows OK.", "GREEN")
        print("Analysis time:", (time() - time_start), "s.")
    else:
        print_color("Invariants NOT OK.\n", "YELLOW")
        print("Analysis time:", (time() - time_start), "s.")
        choice = input("Would you like to see the list? [y/n]")
        if choice == "" or choice == "y":
            print(struct_semiflows)


def Tina_check(net_name, print_output=True):
    """
    State space analysis through tina from the .ndr net. Stored in .txt file.
    Boundedness, liveness and deadlocks can be checked.
    """
    print("Generating state space analysis... (this may take a few minutes)\n") if print_output else 0
    time_start = time()
    tina_make_script = "tina -R " + net_name + ".ndr " + net_name + ".txt -stats"
    run_script(tina_make_script)
    print("State space generation time:",
          (time() - time_start), "s.") if print_output else 0
    time_start = time()
    bound_ok = False
    not_live = False
    anomalies = []
    count_anom = 0
    with open(net_name + ".txt", "r") as tina_file:
        for line in tina_file:
            if "bounded" in line:
                print("\n------------------------------") if print_output else 0
                print("Net is:") if print_output else 0
                print("- Bounded") if print_output else 0
                bound_ok = True
            elif "not live" in line:
                print("- NOT Live") if print_output else 0
                print("- Dead transitions found:") if print_output else 0
                not_live = True
            elif "dead transition(s):" in line:
                print(line) if print_output else 0
            elif "*" in line:
                anomalies.append(line)
                count_anom += 1
            else:
                pass
    if not not_live:
        print("- Live") if print_output else 0
    if not bound_ok:
        print("- Unbounded") if print_output else 0
    # Token amount anomalies
    print("\n------------------------------") if print_output else 0
    print("Checking for places with more than one token...\n") if print_output else 0
    if count_anom == 0:
        print("OK ! One or less token per place.\n") if print_output else 0
    else:
        print(count_anom, "anomalies found:\n") if print_output else 0
        print(anomalies) if print_output else 0
    print("Analysis time:", (time() - time_start), "s.") if print_output else 0

    print("Generating ktz compact file...")
    ktz_make(net_name, do_print=True)
    print("Done!")


def get_path_to(ktz_path, state):
    pathto_script = "pathto " + str(state) + " " + ktz_path
    output = str(run_script(pathto_script)[0])
    return output[4:-3]


def create_formulae_mmc(path, skillnames):
    formulae_file = open(path+"/formulae.mmc", "w")
    print()
    print_color("formulae.mmc:", "CYAN")
    print()
    print("### SkiNet CTL Formulae File ###")
    formulae_file.write("### SkiNet CTL Formulae File ###\n")
    print("#For assistance: baptiste.pelletier@onera.fr")
    formulae_file.write("#For assistance: baptiste.pelletier@onera.fr\n")
    formulae_file.write("\n")
    print()

    print_color("#Skills deadlock", "BOLD")
    formulae_file.write("#Skills deadlock\n")
    for skill in skillnames:
        print("DSKILL "+PLACE_SUFFIX.EXECUTING+SEP+skill+";")
        formulae_file.write("DSKILL "+PLACE_SUFFIX.EXECUTING+SEP+skill+";\n")
    formulae_file.write("\n")
    print()

    print_color("#Skillset deadlock", "BOLD")
    formulae_file.write("#Skillset deadlock\n")
    formula = "DSKILL (" +PLACE_SUFFIX.EXECUTING+SEP+skillnames[0]
    if len(skillnames) > 1:
        for skill in skillnames[1:]:
            formula += " \/ " +PLACE_SUFFIX.EXECUTING+SEP+skill
    formula += ");"
    print(formula)
    formulae_file.write(formula+'\n')
    formulae_file.write("\n")
    print()
    user_formulae = []
    choice = input("Add custom properties?[y/n]")
    if choice in ["y", ""]:
        formulae_file.write("# User properties\n")
        print_color("--- User properties ---", "CYAN")
        print("Type 'help' for help, 'quit' to quit and save.")
        form = input("- ")
        while form != "quit":
            if form == "help":
                print()
                show_CTL_help()
            elif form == "delall":
                user_formulae = []
            elif form == "del":
                if len(user_formulae) > 0:
                    user_formulae.pop(-1)
                else:
                    print("No formula to delete.")
            elif form[:4] == "del ":
                try:
                    nb_del = int(form[4:])
                    if nb_del > len(user_formulae):
                        user_formulae = []
                    else:
                        user_formulae = user_formulae[:-nb_del]
                except:
                    if ValueError:
                        print("Please enter an integer.")
            elif form == "ops":
                show_skinet_mmc()
            else:
                user_formulae.append(form)

            print()
            print_color("# User properties", "BOLD")
            print(*user_formulae, sep="\n")
            print()
            form = input("- ")
    for f in user_formulae:
        formulae_file.write(f+"\n")
    print()
    print_color("Saving formulae.mmc...", "BOLD")
    formulae_file.close()
    print_color("Done.", "GREEN")
    print()


def get_useless_elements(net_file: str, dead_tr: list, skillnames: list, eventnames: list):
    possibly_useless_skills = dict()
    possibly_useless_events = dict()
    definitely_useless = dict()
    definitely_useless["events"] = []
    definitely_useless["skills"] = []
    net = PetriNet(net_file=net_file+".net")
    for tr in dead_tr:
        # print(tr)
        tr_ = tr.split("__")
        action = ".".join(tr_[1:-1])
        if tr_[1] in skillnames:
            if tr_[1] not in possibly_useless_skills.keys():
                possibly_useless_skills[action] = {"found": [tr], "ref": []}
                for ref_tr in net.transitions.keys():
                    if ref_tr.split("__")[:-1] == tr_[:-1]:
                        possibly_useless_skills[action]["ref"].append(ref_tr)
            else:
                possibly_useless_skills[action]["found"].append(tr)
        elif tr_[1] in eventnames:
            if tr_[1] not in possibly_useless_events.keys():
                possibly_useless_events[action] = {"found": [tr], "ref": []}
                for ref_tr in net.transitions.keys():
                    if ref_tr.split("__")[:-1] == tr_[:-1]:
                        possibly_useless_events[action]["ref"].append(ref_tr)
            else:
                possibly_useless_events[action]["found"].append(tr)

    for skill_action, result in possibly_useless_skills.items():
        if result["found"] == result["ref"]:
            definitely_useless["skills"].append(skill_action)
    for event_action, result in possibly_useless_events.items():
        if result["found"] == result["ref"]:
            definitely_useless["events"].append(event_action)

    return definitely_useless
