from os.path import exists, dirname
from importlib import import_module, __import__
from pkgutil import iter_modules
import curses
from unicodedata import name
import time
from skinet.skillset_net import SkillsetNet, SkillState
# from pylib.multi_tools import check_path_safety


def flatten(xss):
    return [x for xs in xss for x in xs]


def read_child(child):
    enabled_tr = []
    for line in child:
        line = str(line)[2:-5]
        if "state" in line:
            current_state = line[len("state")+1:line.index(":")]
            marking = line.split(" ")[2:]
        elif "enabled" in line:
            enabled_tr = line.split(" ")[1:]
        elif "firable:" in line:
            fired_tr = line.split(" ")[1:]
            break
    return current_state, marking, enabled_tr, fired_tr


def read_net(net_file):
    places = []
    tr_names, tr_inouts = [], []
    with open(net_file, "r") as ndr_file:
        for line in ndr_file:
            line = line.split(" ")
            if line[0] == "pl":
                place = [line[1], int(line[2][1])]
                places.append(place)
            elif line[0] == "tr":
                tr = [line[1], " ".join(line[2:])]
                tr[1] = tr[1][:-1]
                tr_names.append(tr[0])
                tr_inouts.append(tr[1])
    return places, tr_names, tr_inouts


def init_curses():
    curses.noecho()
    curses.nocbreak()
    curses.curs_set(False)
    curses.init_color(curses.COLOR_BLACK, 0, 0, 0)
    curses.init_color(curses.COLOR_CYAN, 999, 500, 0)  # is now orange
    curses.init_color(curses.COLOR_BLUE, 700, 999, 999)  # is now bright blue
    curses.init_color(curses.COLOR_MAGENTA, 500, 500, 500)  # is now grey
    curses.init_color(curses.COLOR_RED, 999, 500, 500)  # is now bright red
    curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(7, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    for i in range(10):
        curses.init_color(10+i, max(0, 999-i*50), max(0, 500-i*25), 0)
        curses.init_pair(10+i, 10+i, curses.COLOR_BLACK)
    for i in range(10):
        curses.init_color(60+i, max(0, 700-i*100),
                          max(0, 999-i*100), max(0, 999-i*100))
        curses.init_pair(60+i, curses.COLOR_BLACK, 60+i)
        curses.init_color(70+i, max(0, (700-i*100)//6),
                          max(0, (999-i*100)//6), max(0, (999-i*100)//6))
        curses.init_pair(70+i, curses.COLOR_BLACK, 70+i)
        curses.init_color(80+i, max(0, 700-i*50),
                          max(0, 999-i*50), max(0, 999-i*50))
        curses.init_pair(80+i, 80+i, curses.COLOR_BLACK)
        curses.init_color(90+i, max(0, 700-i*100),
                          max(0, 999-i*100), max(0, 999-i*100))
        curses.init_pair(90+i, curses.COLOR_BLACK, 90+i)


def print_loading_screen(stdscr, lines, cols, display_title=False, n=0):
    for i in range(1, 5):
        stdscr.addstr(i, 0, "-"*(cols), curses.color_pair(1))
        stdscr.addstr(lines-1-i, 0, "-"*(cols), curses.color_pair(1))

    start_skinet = lines//2-3
    stdscr.addstr(start_skinet-1, 0, "="*(cols-1), curses.color_pair(1))

    welcome = "[ Welcome to ]"
    if display_title:
        loading = "[ Runtime Verification & Online Model-Checking ]"
    else:
        loading = "[ Loading, please wait... ]"

    stdscr.addstr(start_skinet-1, (cols-len(welcome)) //
                  2, welcome, curses.color_pair(6))

    SkiNet = [
        "  ____                             ",
        "//   \\\\ ||      |\\\\  ||            ",
        "\\\\____  ||      ||\\\\ ||   __   ||__",
        "     \\\\ ||// || || \\\\|| //__\\\\ ||  ",
        "\\\\___// ||\\\\ || ||  \\\\| \\\\___  \\\\__",
    ]

    for i, line in enumerate(SkiNet):
        stdscr.addstr(start_skinet+i, 0, " "*(cols-1), curses.color_pair(2))
        stdscr.addstr(
            start_skinet+i, (cols-len(SkiNet[0]))//2, line, curses.color_pair(80+n % 10))
        stdscr.addstr(
            start_skinet+i, (cols-len(SkiNet[0]))//2, line, curses.color_pair(80+n % 10))

    wave = [" ", "  ", "   ", "  ", " "]
    if display_title:
        for i, line in enumerate(wave):
            stdscr.addstr(start_skinet+i, (cols-len(SkiNet[0]))//2-1-2*len(
                line)-n % 10, line, curses.color_pair(60+n % 10))
            stdscr.addstr(start_skinet+i, (cols-len(SkiNet[0]))//2-2*len(
                line)-n % 10, line, curses.color_pair(70+n % 10))
            stdscr.addstr(start_skinet+i, (cols+len(SkiNet[0]))//2+1+len(
                line)+n % 10, line, curses.color_pair(60+n % 10))
            stdscr.addstr(start_skinet+i, (cols+len(SkiNet[0]))//2+len(
                line)+n % 10, line, curses.color_pair(70+n % 10))

    stdscr.addstr(start_skinet+i+2, 0, "="*(cols-1), curses.color_pair(1))
    stdscr.addstr(start_skinet+i+2, (cols-len(loading)) //
                  2, loading, curses.color_pair(6))

    stdscr.refresh()


def print_SkiNet(stdscr, lines, cols, name_skillset, manual_mode):
    stdscr.addstr(0, 0, "-"*cols, curses.color_pair(1))
    stdscr.addstr(1, 0, "-"*cols, curses.color_pair(1))
    global title
    title = "- SkiNet - Live ----- Now listening to: "

    if type(name_skillset) == dict:
        title += " ".join(name_skillset.keys())
        title += " "
    else:
        title += name_skillset + " "

    stdscr.addstr(1, 0, title, curses.color_pair(1))
    stdscr.addstr(2, 0, "-"*cols, curses.color_pair(1))
    global heartbeat_start
    index = 16
    heartbeat_start = cols - index
    heartbeat_on = False
    if not manual_mode and len(title) + index < cols:
        stdscr.addstr(0, heartbeat_start, "|   /\       |",
                      curses.color_pair(7))
        stdscr.addstr(1, heartbeat_start, "| _/  \  /\_ |",
                      curses.color_pair(7))
        stdscr.addstr(2, heartbeat_start, "|      \/    |",
                      curses.color_pair(7))
        heartbeat_on = True
    elif manual_mode and heartbeat_start + 13 < cols:
        stdscr.addstr(1, heartbeat_start, " Manual Mode ",
                      curses.color_pair(6))

    global show_enabled
    show_enabled = False
    global start
    start = 2
    global jump
    jump = 3

    return heartbeat_on


def update_heartbeat(hearbeat_on, stdscr, lines, cols, name_skillset, n, has_died):
    global heartbeat_start, title
    if hearbeat_on:
        if not has_died:
            heartbeat = ["_", "/", "/", "\\", "\\", "\\", "/", "/", "\\", "_"]
            heartbeat_line = [1, 1, 0, 0, 1, 2, 2, 1, 1, 1]
            stdscr.addstr(0, heartbeat_start, "|   /\       |",
                          curses.color_pair(7))
            stdscr.addstr(1, heartbeat_start, "| _/  \  /\_ |",
                          curses.color_pair(7))
            stdscr.addstr(2, heartbeat_start, "|      \/    |",
                          curses.color_pair(7))
            stdscr.addstr(heartbeat_line[n % 10], heartbeat_start+n %
                          10+2, heartbeat[n % 10], curses.color_pair(6))
        else:
            heartbeat = ["_"]*10
            heartbeat_line = [1]*10
            color_blink = [4, 7, 4, 7, 4, 7, 4, 7, 4, 7]
            stdscr.addstr(0, heartbeat_start, "|            |",
                          curses.color_pair(color_blink[n % 10]))
            stdscr.addstr(1, heartbeat_start, "| __________ |",
                          curses.color_pair(color_blink[n % 10]))
            stdscr.addstr(2, heartbeat_start, "|            |",
                          curses.color_pair(color_blink[n % 10]))
            stdscr.addstr(heartbeat_line[n % 10], heartbeat_start+n %
                          10+2, heartbeat[n % 10], curses.color_pair(6))
        stdscr.refresh()


def print_state(stdscr, lines, cols, state):
    size_txt = len("| NET STATE: ")
    col_state = cols-(size_txt+len(str(state))+2)
    stdscr.addstr(3, col_state, "| NET STATE: ", curses.color_pair(1))
    stdscr.addstr(3, col_state+size_txt, state, curses.color_pair(2))
    stdscr.addstr(3, col_state+size_txt+len(str(state)),
                  " |", curses.color_pair(1))
    stdscr.addstr(4, col_state, "-"*(size_txt+len(str(state))+2),
                  curses.color_pair(1))


def print_enabled(stdscr, lines, cols, enabled):
    global start, jump, show_enabled

    show_enabled = True
    start += 3
    enabled = [tr.name for tr in enabled]
    stdscr.addstr(start, 0, "- ENABLED: ", curses.color_pair(1))
    stdscr.addstr(start+1, 2, " ".join(enabled), curses.color_pair(2))

    jump = 3 + len(str(" ".join(enabled)))//cols


def pp_tr(tr: str):
    return tr[:tr.index(">")+1]+":".join(tr.split("__")[1:-1])


def print_firable(stdscr, lines, cols, firable, multi=False, n_tr=0, name=""):
    global start, jump, show_enabled
    firable = [str(i+n_tr)+">"+tr.name for i, tr in enumerate(firable)]
    if multi:
        stdscr.addstr(start+jump-1, 0, "-"*(cols+1), curses.color_pair(1))
        stdscr.addstr(start+jump-1, cols//2 - len(name)//2 +
                      1, "["+name+"]", curses.color_pair(6))
    stdscr.addstr(start+jump, 0, "- FIRABLE: ", curses.color_pair(1))
    current_line = ""
    i = 0
    for tr in firable:
        tr = pp_tr(tr)
        current_line += " "+tr
        if len(current_line) > cols-1:
            i += 1
            current_line = tr
        stdscr.addstr(start+jump+i+1, 2, current_line, curses.color_pair(2))

    if not show_enabled:
        jump = 3 + i


def sort_markings(resnames, resstatenames, skillnames, marking):
    res_mark = []
    skill_mark = []
    for i in range(len(resnames)):
        for state in resstatenames[i]:
            if state in marking:
                res_mark.append(marking[marking.index(state)])
                marking.remove(state)
    for skill in skillnames:
        for i, state in enumerate(marking):
            if skill in state[2:]:
                skill_mark.append(state)
                marking.pop(i)
    return res_mark, skill_mark


def print_skillset_state(stdscr, lines, cols, skillset_net: SkillsetNet, resnames, resstatenames, skillnames):
    global start, jump
    sk_states = []
    sk_option = [2, ""]*len(skillnames)
    enabled = [tr.name for tr in skillset_net.enabled()]
    firable = [tr.name for tr in skillset_net.firable()]

    for i, skill in enumerate(skillnames):
        sk_states.append(skillset_net.skill_state(skill))
        if skill+"__start" in str(firable):
            if "t__reset__"+skill+"_" not in str(firable):
                sk_option[i] = [3, '✓']
            else:
                sk_option[i] = [6, '↻']
        elif skill in str(enabled) and skill not in str(firable):
            sk_option[i] = [5, '•']
        else:
            if sk_states[i] == SkillState.RUNNING:
                sk_option[i] = [6, '•']
            else:
                sk_option[i] = [4, 'x']

    stdscr.addstr(start+jump+3, 0, "- RES:", curses.color_pair(1))  # Resources
    col_start = 2
    if resnames != []:
        col_start = max([len(res)+2 for res in resnames])

    for i, res in enumerate(resnames):
        stdscr.addstr(start+jump+4+i, 2, " "+res+":", curses.color_pair(2))
        res_start = col_start
        marked_state = skillset_net.res_state(res)
        for state in resstatenames[i]:
            if state == marked_state:
                stdscr.addstr(start+jump+4+i, 2+res_start,
                              " "+state, curses.color_pair(6))
            else:
                stdscr.addstr(start+jump+4+i, 2+res_start,
                              " "+state, curses.color_pair(7))
            res_start += len(state)+1

    stdscr.addstr(start+jump+3, cols//2, "- SKILLS:",
                  curses.color_pair(1))  # Skills
    for i, skill in enumerate(skillnames):
        if sk_states[i] == SkillState.RUNNING and sk_option[i][0] == 3:
            stdscr.addstr(start+jump+4+i, cols//2+2, " "+skill +
                          ": "+sk_states[i], curses.color_pair(6))
        elif sk_states[i] == SkillState.RUNNING and sk_option[i][0] == 6:
            stdscr.addstr(start+jump+4+i, cols//2+2, " "+skill +
                          ": "+sk_states[i], curses.color_pair(6))
        elif sk_states[i] == SkillState.IDLE and sk_option[i][0] == 3:
            stdscr.addstr(start+jump+4+i, cols//2+2, " "+skill +
                          ": "+sk_states[i], curses.color_pair(2))
        elif sk_states[i] == SkillState.IDLE and sk_option[i][0] == 4:
            stdscr.addstr(start+jump+4+i, cols//2+2, " "+skill +
                          ": "+sk_states[i], curses.color_pair(7))
        elif sk_states[i] == SkillState.IDLE and sk_option[i][0] == 5:
            stdscr.addstr(start+jump+4+i, cols//2+2, " "+skill +
                          ": "+sk_states[i], curses.color_pair(7))
        elif sk_states[i] == SkillState.ENDED and sk_option[i][0] == 4:
            stdscr.addstr(start+jump+4+i, cols//2+2, " "+skill +
                          ": "+sk_states[i], curses.color_pair(7))
        elif sk_states[i] == SkillState.ENDED and sk_option[i][0] == 6:
            stdscr.addstr(start+jump+4+i, cols//2+2, " "+skill +
                          ": "+sk_states[i], curses.color_pair(2))
        stdscr.addstr(start+jump+4+i, cols-10,
                      sk_option[i][1], curses.color_pair(sk_option[i][0]))

    start = start+jump+3+max(len(resnames), len(skillnames))
    jump = 3


def print_history(stdscr, lines, cols, history, max_hist):
    global start

    line = start+1
    i = -1
    n = len(history)
    while line < start+max_hist+1 and -i < len(history)+1:
        tr = history[i]
        output = str(n+i) + ": " + tr.name + " " + \
            " ".join(tr.inputs)+" -> "+" ".join(tr.outputs)
        if cols < len(output):
            stdscr.addstr(line, 0, str(n+i)+": "+tr.name,
                          curses.color_pair(10+line-start-1))
            stdscr.addstr(line+1, 0, "  - "+" ".join(tr.inputs)+" -> " +
                          " ".join(tr.outputs), curses.color_pair(10+line-start-1))
            line += 2
        else:
            stdscr.addstr(line, 0, output, curses.color_pair(10+line-start-1))
            line += 1
        i -= 1


def print_history_multi(stdscr, lines, cols, line_0, col_start, fired_tr, multi_rl, max_hist):
    line = line_0+1
    col_start += 1
    i = -1
    n = len(fired_tr)
    while line < line_0+max_hist+1 and -i < len(fired_tr)+1:
        robot = fired_tr[i][0]
        try:
            tr = multi_rl[robot]["net"].transitions[fired_tr[i][1]]
        except:
            if KeyError:
                print(fired_tr)
                time.sleep(20)
        # + " " + " ".join(tr.inputs)+" -> "+" ".join(tr.outputs)
        output = str(n+i) + " " + robot + ": " + tr.name
        # print(output)
        # if cols-col_start < len(output):
        #     stdscr.addstr(line,col_start,str(n+i),curses.color_pair(6))
        #     stdscr.addstr(line,col_start+len(str(n+i))+1,robot+": "+str(fired_tr[i][1]),curses.color_pair(10+line-line_0-1))
        #     if len("  - "+" ".join(tr.inputs)+" -> "+" ".join(tr.outputs)) > cols - col_start:
        #         stdscr.addstr(line+1,col_start,"  - "+" ".join(tr.outputs),curses.color_pair(10+line-line_0-1))
        #         stdscr.addstr(line+2,col_start,"  -> "+" ".join(tr.outputs),curses.color_pair(10+line-line_0-1))
        #         line +=3
        #     else:
        #         stdscr.addstr(line+1,col_start,"  - "+" ".join(tr.inputs)+" -> "+" ".join(tr.outputs),curses.color_pair(10+line-line_0-1))
        #         line +=2
        # else:
        stdscr.addstr(line, col_start, str(n+i), curses.color_pair(6))
        # stdscr.addstr(line,col_start+len(str(n+i))+1,robot+": "+tr.name+" "+" ".join(tr.inputs)+" -> "+" ".join(tr.outputs),curses.color_pair(10+line-line_0-1))
        stdscr.addstr(line, col_start+len(str(n+i))+1, robot +
                      ": "+tr.name, curses.color_pair(10+line-line_0-1))
        line += 1
        i -= 1
    stdscr.refresh()


def do_user_input(stdscr, lines, cols, skillset_net: SkillsetNet):
    loop = True
    curses.echo()
    c = ''
    firable = [tr.name for tr in skillset_net.firable()]
    while loop:
        stdscr.addstr(lines-3, 0, "Fire transition: "+(cols -
                      len("Fire transition: ")-1)*" ", curses.color_pair(2))
        stdscr.refresh()
        c = stdscr.getstr(lines-3, len("Fire transition: "))
        try:
            if c == "":
                None
            elif int(c) < len(firable):
                tr = firable[int(c)]
                stdscr.addstr(lines-2, 0, "Fire "+tr +
                              " ? [y/n]", curses.color_pair(6))
                stdscr.refresh()
                choice = input("")
                print(choice)
                if choice in ["y", ""]:
                    stdscr.addstr(lines-2, 0, len("Fire "+tr +
                                  " ? [y/n]"+choice)*" ", curses.color_pair(6))
                    loop = False
                    # fire
                    skillset_net.fire(tr)
                    return True, tr
                else:
                    stdscr.addstr(lines-2, 0, len("Fire "+tr +
                                  " ? [y/n]"+choice)*" ", curses.color_pair(6))
        except ValueError:
            if c == 'q':
                loop = False
                return False, ""


def do_user_input_multi(stdscr, lines, cols, multi_rl, multi_firable, tr_path, block_safe, safety_prop, current_muse, current_forbidden_marking, check_robots, robot_index):
    loop = True
    curses.echo()
    c = ''
    firable = flatten([tr[2] for tr in multi_firable])
    firable = [tr.name for tr in firable]
    while loop:
        stdscr.addstr(lines-3, 0, "Fire transition: "+(cols -
                      len("Fire transition: ")-1)*" ", curses.color_pair(2))
        stdscr.refresh()
        c = stdscr.getstr(lines-3, len("Fire transition: "))
        try:
            if c == "":
                None
            elif int(c) < len(firable):
                n = int(c)
                tr = firable[n]
                safe = True
                if block_safe:
                    blocking_prop = []
                    for prop in safety_prop:
                        unsafe_tr = [tr.split(":")[1]
                                     for tr in prop["unsafe_tr"]]
                        if tr in unsafe_tr:
                            blocking_prop.append(prop["name"])
                    if blocking_prop != []:
                        stdscr.addstr(lines-2, 0, "Cannot fire. "+tr+" blocked by: " +
                                      ", ".join(blocking_prop), curses.color_pair(4))
                        safe = False
                if safe:
                    stdscr.addstr(lines-2, 0, "Fire "+tr +
                                  " ? [y/n]", curses.color_pair(6))
                    stdscr.refresh()
                    choice = input("")
                    print(choice)
                    if choice in ["y", ""]:
                        stdscr.addstr(
                            lines-2, 0, len("Fire "+tr+" ? [y/n]"+choice)*" ", curses.color_pair(6))
                        loop = False
                        # fire
                        i = 0
                        while not multi_firable[i][1][1] > n >= multi_firable[i][1][0]:
                            i += 1
                        target_robot = multi_firable[i][0]
                        multi_rl[target_robot]["net"].fire(tr)
                        return True, [target_robot, tr]
                    else:
                        stdscr.addstr(
                            lines-2, 0, len("Fire "+tr+" ? [y/n]"+choice)*" ", curses.color_pair(6))
        except:
            if ValueError:
                if c.decode("utf-8") == "q":
                    loop = False
                    return False, []
                elif c.decode("utf-8") == "path":
                    if tr_path != []:
                        stdscr.addstr(
                            lines-2, 0, "Fire along path? [y/n]", curses.color_pair(6))
                        stdscr.refresh()
                        choice = input("")
                        print(choice)
                        if choice in ["y", ""]:
                            stdscr.addstr(
                                lines-2, 0, "Fire along path? [y/n]", curses.color_pair(6))
                            loop = False
                            # fire
                            # if check_path_safety(stdscr,lines,cols,multi_rl,tr_path,safety_prop,multi_firable,current_muse,current_forbidden_marking,check_robots,robot_index):
                            for i, tr in enumerate(tr_path):
                                if not multi_rl[tr[0]]["net"].fire(tr[1]):
                                    return True, tr_path[:i]
                            return True, tr_path
                        else:
                            stdscr.addstr(
                                lines-2, 0, "Fire along path? [y/n]", curses.color_pair(6))
                    else:
                        stdscr.addstr(
                            lines-2, 0, "No path available.", curses.color_pair(4))


def capitalize_nth(s, n):
    if n == 0:
        return s[0].capitalize() + s[1:]
    elif n == len(s)-1:
        return s[:n] + s[n].capitalize
    else:
        return s[:n] + s[n].capitalize() + s[n+1:]


def camelize(s: str):
    s = capitalize_nth(s, 0)
    for i, c in enumerate(s):
        if c == "_":
            s = capitalize_nth(s, i+1)
    s = s.replace("_", "")
    s.upper()
    return ''.join(s)


def init_status_msgs(name_skillset, skillnames):
    skillset_status_attr = getattr(import_module(
        "._skillset_status", name_skillset+"_skillset_interfaces.msg"), "SkillsetStatus")
    status_msgs = [[skillset_status_attr, "/status"]]
    for skill in skillnames:
        camel_skill = camelize(skill)
        skill = skill.lower()
        package_name = "._skill_"+skill+"_response"
        class_name = "Skill"+camel_skill+"Response"
        topic_name = "/skill/"+skill+"/response"
        class_attr = getattr(import_module(
            package_name, name_skillset+"_skillset_interfaces.msg"), class_name)
        status_msgs.append([class_attr, topic_name])
    evt_req_attr = getattr(import_module(
        "._event_request", name_skillset+"_skillset_interfaces.msg"), "EventRequest")
    status_msgs.append([evt_req_attr, "/event_request"])
    evt_resp_attr = getattr(import_module(
        "._event_response", name_skillset+"_skillset_interfaces.msg"), "EventResponse")
    status_msgs.append([evt_resp_attr, "/event_response"])
    return status_msgs


def init_skillset_status(skillset_content):
    name_skillset, skillnames, resnames, resstatenames, eventnames = skillset_content
    with open("skillsetstatus.txt", "w") as status_file:
        status_file.write(
            "***This is an automatically generated file. DO NOT TOUCH DURING RUNTIME!!***\n")
        status_file.write(name_skillset+"\n")
        status_file.write("stamp(sec):?"+"\n")
        for res in resnames:
            status_file.write("res?\n")
        for skill in skillnames:
            status_file.write("skill?\n")


def update_skillset_status(msg, skillset_content):
    name_skillset, skillnames, resnames, resstatenames, eventnames = skillset_content
    with open("skillsetstatus.txt", "w") as status_file:
        status_file.write(
            "***This is an automatically generated file. DO NOT TOUCH DURING RUNTIME!!***\n")
        status_file.write(name_skillset+"\n")
        status_file.write("stamp(sec):"+str(msg._stamp._sec)+"\n")
        for res in msg._resources:
            status_file.write(str(res._name)+":"+str(res._state)+"\n")
        for skill in skillnames:
            skill_status = getattr(msg, "skill_"+skill)
            status_file.write(str(skill_status._name)+":" +
                              str(skill_status._is_running)+"\n")


def init_skill_response_file(name_skillset, skillnames):
    global skill_response_count
    skill_response_count = [0 for skill in skillnames]
    response_file = open("skillsresponses.txt", "w")
    response_file.write(
        "***This is an automatically generated file. DO NOT TOUCH DURING RUNTIME!!***\n")
    response_file.write(name_skillset+"\n")
    for i, skill in enumerate(skillnames):
        response_file.write(skill+":?"*3+":"+str(skill_response_count[i])+"\n")
    response_file.close()


def update_skill_response(skill_name, result):
    global skill_response_count
    response_file = open("skillsresponses.txt", "r")
    responses = response_file.readlines()
    response_file.close()
    for i, resp in enumerate(responses[2:]):
        if skill_name in resp:
            skill_response_count[i] += 1
            responses[i+2] = ":".join(result)+":" + \
                str(skill_response_count[i])+"\n"
    with open("skillsresponses.txt", "w") as response_file:
        for resp in responses:
            response_file.write(resp)


def find_skill_name(skill_name, skillnames):
    for skill in skillnames:
        if skill.lower() == skill_name:
            return skill


def get_results(msg, status_msgs, skill_result, skillnames):
    for msg_type in status_msgs[1:-2]:
        if type(msg) == msg_type[0]:
            skill_name = msg_type[1].split('/')[2]
            skill_name = find_skill_name(skill_name, skillnames)
            result = [msg._result, msg._effect]
            if result[0] == skill_result[skill_name].SUCCESS:
                result[0] = skill_name+"_success_"+msg._name
            elif result[0] == skill_result[skill_name].ALREADY_RUNNING:
                result[0] = skill_name+"_already_running_"+msg._name
            elif result[0] == skill_result[skill_name].VALIDATE_FAILURE:
                result[0] = skill_name+"_val_fail_"+msg._name
            elif result[0] == skill_result[skill_name].PRECONDITION_FAILURE:
                result[0] = skill_name+"__pre_fail__"+msg._name
            elif result[0] == skill_result[skill_name].START_FAILURE:
                result[0] = skill_name+"_start_fail_"+msg._name
            elif result[0] == skill_result[skill_name].INVARIANT_FAILURE:
                result[0] = skill_name+"__inv_fail__"+msg._name
            elif result[0] == skill_result[skill_name].INTERRUPT:
                result[0] = skill_name+"__interrupt__"+msg._name
            elif result[0] == skill_result[skill_name].FAILURE:
                result[0] = skill_name+"__failure__"+msg._name
    return result


def init_event_response_file(name_skillset):
    with open("eventresponse.txt", "w") as event_file:
        event_file.write(
            "***This is an automatically generated file. DO NOT TOUCH DURING RUNTIME!!***\n")
        event_file.write(name_skillset+"\n")
        event_file.write("?:?")


def check_init_files():
    missing_files = (not exists("skillsetstatus.txt") or
                     not exists("skillsresponses.txt") or
                     not exists("eventresponse.txt"))
    if missing_files:
        print("Probe files are missing. Start the skinet_live node first to initialize.")
        return False
    else:
        with open("skillsetstatus.txt", "r") as txt_file:
            lines = txt_file.readlines()
        if lines[2][-2] == "?":
            print(
                "Probe files are not initialized yet. Run a status update request to initialize.")
            return False
        else:
            return True


def read_status_files():
    with open("skillsetstatus.txt", "r") as txt:
        last_sk_status = txt.readlines()
    with open("eventresponse.txt", "r") as txt:
        last_evt_response = txt.readlines()
    with open("skillsresponses.txt", "r") as txt:
        last_sk_response = txt.readlines()
    return last_sk_status+last_evt_response+last_sk_response


def resources_ok(res_update, res_mark):
    for i, state in enumerate(res_update):
        if state != res_mark[i]:
            return False
    return True


def check_reset(skillset_net, fired_tr, firable):
    t_reset = []
    has_reset = True
    had_reset = False
    while has_reset:
        for tr_fr in firable:
            if "t__reset__" in tr_fr:
                t_reset.append(tr_fr)
                has_reset = False
            if "t__reset__" not in tr_fr and tr_fr == firable[-1]:
                has_reset = False
    if t_reset != []:
        skillset_net.fire(t_reset[0])
        fired_tr.append(t_reset[0])
        t_reset.pop(0)
        had_reset = True
    return had_reset


def check_reset_multi(multi_rl: dict, fired_tr):
    t_reset = []
    had_reset = False
    for robot in multi_rl.keys():
        has_reset = True
        while has_reset:
            firable = multi_rl[robot]["net"].firable()
            for tr_fr in firable:
                if "t__reset__" in tr_fr.name:
                    t_reset.append(tr_fr.name)
                    has_reset = False
                if "t__reset__" not in tr_fr.name and tr_fr == firable[-1]:
                    has_reset = False
        if t_reset != []:
            multi_rl[robot]["net"].fire(t_reset[0])
            fired_tr.append([robot, t_reset[0]])
            t_reset.pop(0)
            had_reset = True
    return had_reset


def check_stock(had_reset, skillset_net, t_stock, fired_tr, firable):
    had_stock = False
    while t_stock != []:
        for tr_fr in firable:
            if t_stock[0] in tr_fr.name:
                skillset_net.fire(tr_fr)
                fired_tr.append(tr_fr.name)
                t_stock.pop(0)
                had_stock = True
                if t_stock == []:
                    break
            elif t_stock[0] not in tr_fr and tr_fr == firable[-1]:
                t_stock.pop(0)
    return t_stock, had_stock


def check_stock_multi(had_reset, multi_rl, t_stock, fired_tr):
    had_stock = False
    while t_stock != []:
        robot, tr_name = t_stock[0]
        firable = multi_rl[robot]["net"].firable()
        for tr_fr in firable:
            if tr_name in tr_fr.name:
                multi_rl[robot]["net"].fire(tr_fr)
                fired_tr.append([robot, tr_fr.name])
                t_stock.pop(0)
                had_stock = True
                if t_stock == []:
                    break
            elif tr_name not in tr_fr.name and tr_fr == firable[-1]:
                t_stock.pop(0)
    return t_stock, had_stock, fired_tr


def search_for_diff(stdscr, lines, cols, last_update, firable, res_mark, skillset_content):
    name_skillset, skillnames, resnames, resstatenames, eventnames = skillset_content
    diff = []
    last = []
    tr_found = []
    t_stock = []
    has_died = False
    new_update = read_status_files()
    if len(new_update) == len(last_update):
        for i, line in enumerate(new_update):
            if line != last_update[i]:
                diff.append(line)
                last.append(last_update[i])
        if len(diff) > 0:
            for diff_i in diff:
                diff_i = diff_i.split(":")
                if diff_i[0] == "stamp(sec)" and len(diff) == 1:
                    if not resources_ok(new_update, resnames, res_mark):
                        stdscr.addstr(
                            lines-1, 0, "New status only but resources are wrong! Better quit now...", curses.color_pair(1))
                        has_died = True
                        stdscr.refresh()
                    else:
                        stdscr.addstr(
                            lines-1, 0, "New status only. Timestamp update.", curses.color_pair(1))
                        stdscr.refresh()
                else:
                    if len(diff_i) < 2:
                        stdscr.addstr(lines-2, 0, str(diff_i),
                                      curses.color_pair(1))
                        stdscr.addstr(
                            lines-1, 0, "Error occured. Please quit.", curses.color_pair(1))
                        break
                    elif diff_i[0] in skillnames:
                        if len(diff_i) == 2 and "True" in diff_i[1]:
                            for tr_fr in firable:
                                if "t__"+diff_i[0]+"__start__" in tr_fr:
                                    tr_found.append(tr_fr)
                                    break
                        if len(diff_i) == 5 and "True" in diff_i[3]:
                            for tr_fr in firable:
                                if "t__"+diff_i[0]+"_"+diff_i[1]+"_"+diff_i[2] in tr_fr:
                                    tr_found.append(tr_fr)
                                    break
                                elif "t__"+diff_i[0]+"_"+diff_i[1]+"_"+diff_i[2] not in tr_fr and tr_fr == firable[-1]:
                                    # precondition failures are considered junk if not firable immediately
                                    if "pre_fail" not in diff_i[1]:
                                        t_stock.append(
                                            "t__"+diff_i[0]+"_"+diff_i[1]+"_"+diff_i[2])
                    elif diff_i[0] in eventnames and diff_i[1] == "success":
                        for tr_fr in firable:
                            if "t__"+diff_i[0]+"_" in tr_fr:
                                tr_found.append(tr_fr)
                                break
            last_update = new_update
    return last_update, tr_found, t_stock, has_died


def print_properties(stdscr, lines, cols, valid_props, properties):
    global start

    line = start+1
    stdscr.addstr(line, 0, "Properties verified:", curses.color_pair(1))
    for i, p in enumerate(properties):
        if p in valid_props:
            stdscr.addstr(line+1+i, 3, p, curses.color_pair(6))
        else:
            stdscr.addstr(line+1+i, 3, p, curses.color_pair(7))

    start += len(properties)+2


def print_actions(stdscr, lines, cols, line_0, col_start):
    actions_prmpt = ["robot:elem",
                     "and, or, not",
                     "A,E + G, F, X",
                     "Example:",
                     ]
    explanations = [" elem from robot is marked",
                    " logical operators",
                    " CTL temporal operators",
                    " robot1:i_skill1 and not robot2:i_skill2",
                    ]

    stdscr.addstr(line_0, col_start, "-" *
                  (cols-col_start), curses.color_pair(1))
    stdscr.addstr(line_0, col_start+col_start//2 -
                  len("[actions]")//2, "[actions]", curses.color_pair(6))
    line_0 += 1

    col_expl = max([len(action) for action in actions_prmpt])

    for i, action in enumerate(actions_prmpt):
        stdscr.addstr(line_0, col_start+2, action, curses.color_pair(0))
        stdscr.addstr(line_0, col_start+2+col_expl,
                      explanations[i], curses.color_pair(1))
        line_0 += 1
