from queue import Queue
from typing import Tuple
from os.path import join, dirname
from skinet.tina_tools import read_muse_child, run_script, SKINET_MMC
from skinet.terminal_display import print_color
from skinet.mission_tools import print_progress
from skinet.tpn_tools import update_net_marking
from skinet.net_def import PLACE_SUFFIX, SEP
from time import time, sleep
from json import loads
from pexpect import spawn
from threading import Event
import curses


def read_properties(formula_file: str) -> list:
    """
    Read LTL properties from a formulae.mmc file and returns them in a list.
    The list is ordered the same way the formulae are ordered in the original file.
    """
    properties = []
    with open(formula_file) as formulae:
        lines = formulae.readlines()
        for i, line in enumerate(lines):
            if "#" in line:
                line = line[:line.index("#")]
            if line != "" and len(line) > 1:
                if ";" in line:
                    properties.append(line[:line.index(";")])
                else:
                    print("Missing ';' at line("+str(len(lines)-i)+"): "+line)
                    quit()
    return properties


def get_properties_states(path: str, name_skillset: str) -> Tuple[list, list]:
    """
    Read LTL properties from name_skillset.ktz and formulae.mmc.
    Returns a list of properties and a list of lists which contains, for each property,
    the list of states where the property is valid.
    """
    muse_script = "muse -prelude "+SKINET_MMC)+" "
    muse_script += path+"/"+name_skillset+".ktz"
    muse_script += " -s "+path+"/formulae.mmc"
    output = run_script(muse_script)[0]
    if "cannot open" in output:
        print_color("Error, .ktz or formulae.mmc files missing.", "RED")
        quit()
    properties = read_properties(path+"/formulae.mmc")
    output = output.replace('\\n', '')
    output = output.split("it : states")[1:]
    error_states = []
    properties_old = properties
    for i, states in enumerate(output):
        if "error" in states:
            states = states.split("MMC")
            new_states = states.copy()
            for elem in states:
                if "error" in elem:
                    error_states.append(elem.replace("'", ""))
                    new_states.remove(elem)
                    for prop in properties_old:
                        if prop in elem:
                            properties.remove(prop)
            output[i] = "".join(new_states)
    for i, states in enumerate(output):
        print_color(properties[i], "CYAN")
        print(" - "+states[states.index("]")+1:])
        if states[:2] == "[]":
            output[i] = []
            print_color(" - Always True.", "GREEN")
        else:
            output[i] = [int(s)
                         for s in states[1:states.index("]")].split(", ")]
            print(" - "+str(len(output[i]))+" states.")
    for errors in error_states:
        print_color("SKIPPED:"+errors, "RED")
    if error_states != []:
        print()
        if input("Errors found. Continue?[y/n]") not in ["y", ""]:
            quit()

    return properties, output


def get_ktz_map(ktz_file: str, save: bool = False) -> dict:
    """
    Uses the liszt package to create a .json dictionary translated from ktz_file (format .ktz).
    Returns said dictionary.
    Option save allows to save that dictionary in an uncompressed file called temp.json (False by default).
    """
    output = run_script("liszt "+ktz_file)[0]
    output = output.split('\\n')
    output = '\n'.join(output)[2:-1].replace("=", "")
    if save:
        with open("temp.json", "w") as temp_json:
            temp_json.write(output)
    return loads(output)


def get_marking_from_ktz_state(ktz_state: list, set_map: dict, index_map: dict) -> list:
    """
    With a given set_map dict containing the places of resources and skils,
    a index_map dict containing the index of those states in the ktz_state list,
    returns the equivalent marking list.
    The marking is the list of all places where ktz_state[index_map[place]] is equal to 1.
    """
    marking = []
    for elem in set_map.keys():
        list_TF = [ktz_state[i] for i in index_map[elem]]
        running_value = [set_map[elem][i]
                         for i in range(len(list_TF)) if list_TF[i] == 1][0]
        marking.append(running_value)
    return marking


def generate_state_map(ktz_map: dict) -> dict:
    """
    From a ktz_map dict, generates the state_map dict, which is the inverse of the map in ktz_map["states"].
    This map allows for quick retrieval of the state number of a given marking state written in a list of 0s and 1s.
    """
    state_map = dict()
    for state in ktz_map["states"]:
        state_map[str(state["val"])] = state["no"]
    return state_map


def get_ktz_state_with_muse(muse_child, marking: list) -> dict:
    """
    For a given ktz_map dict, state_map dict and marking list, returns ktz_map["states"][state number],
    with the state number retrieved by converting the marking into a list of 0s and 1s and using the state_map dict.
    """
    muse_child.sendline(" /\ ".join(marking)+";")
    for line in muse_child:
        line = line.decode("utf-8")
        if "[" == line[0]:
            if "[]" == line[0:2]:
                return -1
            else:
                return int(line[1:line.index("]")])


def get_ktz_state_from_marking(ktz_map: dict, state_map: dict, marking: list) -> dict:
    """
    For a given ktz_map dict, state_map dict and marking list, returns ktz_map["states"][state number],
    with the state number retrieved by converting the marking into a list of 0s and 1s and using the state_map dict.
    """
    state_bin = []
    for elem in ktz_map["props"]:
        if elem in marking:
            state_bin.append(1)
        else:
            state_bin.append(0)
    return ktz_map["states"][state_map[str(state_bin)]]


def fill_ktz_map(ktz_map: dict, nb_states: int, properties: list, p_states: list) -> dict:
    """
    Given a ktz_map dict with no properties, a list of properties and a list of states for each, contained in p_states,
    returns an updated ktz_map dict with a new key "p" for each state, containing the list of properties valid in each state.
    nb_states is the number of states to be treated, and is used for feedback on the progress state.
    """
    time_in = time()
    for i, state in enumerate(ktz_map["states"]):
        if int(100*(i+1)/nb_states) % 5 == 0:
            print_progress(i, nb_states, time()-time_in)

        state["p"] = []
        for j, p_s in enumerate(p_states):
            if state["no"] in p_s:
                state["p"].append(properties[j])

    return ktz_map


def get_forbidden_markings(child, safety_muse):
    forbidden_marks = dict()
    for prop, ctl_form in safety_muse.items():
        child.sendline(ctl_form)
        output = []
        last_line = ""
        for line in child:
            line = line.decode("utf-8")
            line = line.replace('\r', "").replace('\n', "")
            output.append(line)
            if "bound" in line:
                sleep(20)
            if "it : states" in last_line:
                break
            if "typing error" in line:
                return 0
            last_line = line
        output = output[-1][1:-1].split(", ")
        if output == ['']:
            forbidden_marks[prop] = []
        else:
            forbidden_marks[prop] = [int(x) for x in output]
    return forbidden_marks


def run_sift(kill_sift_thread: Event, path: str, fused_net: str, fired_tr_queue: Queue, sift_msg_queue: Queue, muse_childs: Queue, multi_rl: dict, calc_time: int, safety_prop: list, robot_index: dict, forbidden_markings: Queue):
    global new_ktz
    new_ktz = False
    ### safety_prop to selt/muse format ###
    safety_sift = ""
    safety_muse = dict()
    for prop in safety_prop:
        safety_muse_i = "AF -("
        formula = " "+prop["prop"]
        formula = formula.replace(" and ", " /\\ ")
        formula = formula.replace(" or ", " \\/ ")
        formula = formula.replace(" not ", " - ")
        for i, elem in enumerate(formula.split(" ")):
            if ":" in elem:
                elem_temp = elem.replace("(", "").replace(")", "")
                elem_temp = elem_temp.split(":")
                if len(elem_temp) == 2:
                    item = "{" + PLACE_SUFFIX.EXECUTING + SEP +elem_temp[1] + "." + \
                        str(robot_index[elem_temp[0]]) + "}"
                else:
                    item = "{" + elem_temp[1]+"__"+elem_temp[2] + \
                        "." + str(robot_index[elem_temp[0]]) + "}"
                if "(" in elem or ")" in elem:
                    i = 0
                    while elem[i] == "(":
                        i += 1
                        item = "(" + item
                    i = -1
                    while elem[i] == ")":
                        i -= 1
                        item += ")"
                safety_sift += item
                safety_muse_i += item
            elif elem == "":
                pass
            else:
                safety_sift += " "+elem+" "
                safety_muse_i += " "+elem+" "
        if prop != safety_prop[-1]:
            safety_sift += " \/ "
        safety_muse_i += ");"
        safety_muse[prop["name"]] = safety_muse_i

    ### Init sift (short lived) ###
    n = 1
    # output = run_script("sift -R -t "+str(max(2,calc_time//2))+" "+fused_net+" "+path+"/out"+str(n%2)+".ktz -f "+'"'+safety_sift+'"')[0]
    output = run_script("sift -R -t "+str(max(2, calc_time//2)) +
                        " "+fused_net+" "+path+"/out"+str(n % 2)+".ktz")[0]
    new_ktz = True
    child = spawn("muse -prelude "+SKINET_MMC)+" -s " +
                  path+"/out"+str(n % 2)+".ktz")
    child.delaybeforesend = 0.0
    for line in child:
        line = line.decode("utf-8")
        if "loaded" in line:
            break
    muse_childs.put([child, path+"/out"+str(n % 2)+".ktz"])
    new_forbidden_markings = get_forbidden_markings(child, safety_muse)
    forbidden_markings.put(new_forbidden_markings)
    sift_msg_queue.put(sift_to_msg(output)+[new_forbidden_markings])

    # Recalculate if new transitions happen
    while True:
        if kill_sift_thread.is_set():
            break

        if not fired_tr_queue.empty() and muse_childs.empty():
            n += 1
            new_ktz = False
            for robot in multi_rl:
                update_net_marking(path, robot, multi_rl[robot]["net"])
            # output = run_script("sift -R -t "+str(calc_time)+" "+fused_net+" "+path+"/out"+str(n%2)+".ktz -f "+'"'+safety_sift+'"')[0]
            output = run_script("sift -R -t "+str(calc_time) +
                                " "+fused_net+" "+path+"/out"+str(n % 2)+".ktz")[0]
            child = spawn("muse -prelude "+SKINET_MMC+" -s " +
                          path+"/out"+str(n % 2)+".ktz")
            child.delaybeforesend = 0.0
            for line in child:
                line = line.decode("utf-8")
                if "loaded" in line:
                    break
            new_ktz = True
            if "expired" in output:
                child.sendline("-<T>T;")
                done = False
                for line in child:
                    line = line.decode("utf-8")
                    line = line.replace('\n', "").replace('\r', "")
                    if "it :" in line and not done:
                        done = True
                    elif done:
                        result = line
                        if result != "[]":
                            state = result[result[::-
                                                  1].index("]")+2:result[::-1].index(" ,")]
                            output_pathto = run_script("pathto "+str(state[:-1])+" "+path+"/out"+str(
                                n % 2)+".ktz")[0][3:].replace('\n', "").replace('\r', "").split(" ")
                            depth = (len(output_pathto)-1)//2
                        break
                output += ", max depth: "+str(depth)+" transitions."

            muse_childs.put([child, path+"/out"+str(n % 2)+".ktz"])
            new_forbidden_markings = get_forbidden_markings(child, safety_muse)
            forbidden_markings.put(new_forbidden_markings)
            sift_msg_queue.put(sift_to_msg(output)+[new_forbidden_markings])
        else:
            sleep(1)


def sift_to_msg(output: str):
    output = output[2:-5].split("\\n")
    return output


def query_muse(current_muse, formula: str, robot_index):

    if "{" not in formula:
        formula_to_muse = ""
        for i, elem in enumerate(formula.split(" ")):
            if ":" in elem:
                elem_temp = elem.replace("(", "").replace(")", "")
                robot, item = elem_temp.split(":")
                try:
                    item = "{" + item + "." + str(robot_index[robot]) + "}"
                except:
                    if KeyError:
                        return -2
                if elem[0] == "(":
                    item = "(" + item
                elif elem[-1] == ")":
                    item += ")"
                formula_to_muse += item
            elif elem == "":
                pass
            else:
                formula_to_muse += " "+elem+" "
        formula = formula_to_muse+";"

    current_muse.sendline(formula)
    done = False
    for line in current_muse:
        line = line.decode("utf-8")
        line = line.replace('\n', "").replace('\r', "")
        if "it :" in line and not done:
            done = True
        elif done:
            result = line
            if result == "[]":
                result = -1
            else:
                result = result[1:-1].split(", ")
            break
        elif "error" in line:
            result = -2
            break
    return result


def treat_query(stdscr, lines, cols, current_muse, multi_rl, current_ktz, safety_prop, last_query, current_forbidden_marking, robot_index):
    for i in range(1, 7):
        stdscr.addstr(lines-i, 0, " "*(cols-1), curses.color_pair(1))

    tr_path = []
    result = query_muse(current_muse, last_query, robot_index)
    if result == -1:
        stdscr.addstr(lines-4, 0, "No state found for query: " +
                      last_query, curses.color_pair(4))
    elif result == -2:
        stdscr.addstr(lines-4, 0, "Typing error in received query: " +
                      last_query, curses.color_pair(4))
    else:
        markings = get_multi_marking(multi_rl)
        from_state = get_ktz_state_with_muse(current_muse, markings)
        if from_state not in current_forbidden_marking and str(from_state) in result:
            tr_path = [["OK", "Query already satisfied by current state!"]]
        else:
            i = 0
            while result[i] in current_forbidden_marking and i < len(result) - 1:
                i += 1
            state = result[i]
            if from_state == -1:
                stdscr.addstr(lines-4, 0, " "*(cols-1), curses.color_pair(6))
                stdscr.addstr(
                    lines-4, 0, "State does not exist. An event occured and is blocking the search.", curses.color_pair(6))
                return tr_path
            transits = run_script("pathto "+str(state)+" -from "+str(from_state)+" "+current_ktz)[
                0][3:].replace('\n', "").replace('\r', "").split(" ")
            tr_path = multi_robot_path(multi_rl, transits[1::2])
            while not check_path_safety(stdscr, lines, cols, multi_rl, tr_path, safety_prop) and i < len(result)-1:
                i += 1
                while result[i] in current_forbidden_marking and i < len(result)-1:
                    i += 1
                state = result[i]
                transits = run_script(
                    "pathto "+str(state)+" -from "+str(from_state)+" "+current_ktz)[0][3:-6].split(" ")
                tr_path = multi_robot_path(multi_rl, transits[1::2])
            if i == len(result) - 1 and (not check_path_safety(stdscr, lines, cols, multi_rl, tr_path, safety_prop) or result[i] in current_forbidden_marking):
                stdscr.addstr(
                    lines-3, 0, "No safe path found for query: "+last_query, curses.color_pair(4))
    return tr_path


def get_multi_marking(multi_rl):
    markings = []
    for i, robot in enumerate(multi_rl.values()):
        for state in robot["net"].marking:
            fused_state = "{"+state+"."+str(i+1)+"}"
            markings.append(fused_state)
    return markings


def multi_robot_path(multi_rl, tr_path):
    robot_list = [robot for robot in multi_rl.keys()]
    robot_path = []
    for tr in tr_path:
        robot = robot_list[int(tr[tr.index(".")+1:])-1]
        robot_path.append([robot, tr[:tr.index(".")]])
    return robot_path


def check_path_safety(stdscr, lines, cols, multi_rl, tr_path, safety_prop, print_str=False):
    # check
    safe = True
    blocking_prop = []
    to_undo = dict()
    unsafe_tr = dict()
    for prop in safety_prop:
        unsafe_tr[prop["name"]] = [
            tr.split(":")[1] for tr in prop["unsafe_tr"]]
    for robot in multi_rl.keys():
        to_undo[robot] = 0
    for transit in tr_path:
        robot, tr = transit
        for prop in safety_prop:
            if tr in unsafe_tr[prop["name"]]:
                blocking_prop.append(prop["name"])
        if blocking_prop != []:
            if print_str:
                stdscr.addstr(lines-2, 0, "Cannot fire. "+tr+" blocked by: " +
                              ", ".join(blocking_prop), curses.color_pair(4))
            safe = False
            break
        else:
            multi_rl[robot]["net"].fire(tr)
            to_undo[robot] += 1
            multi_firable_temp = []
            n = 0
            for robot in multi_rl.keys():
                firable = multi_rl[robot]["net"].firable()
                multi_firable_temp.append(
                    [robot, [n, n+len(firable)], firable])
                n += len(firable)
            for prop in safety_prop:
                unsafe_trs = check_safe_transitions(
                    prop["prop"], multi_rl, multi_firable_temp)
                unsafe_tr[prop["name"]] = [
                    tr.split(":")[1] for tr in unsafe_trs]

    for robot in multi_rl.keys():
        multi_rl[robot]["net"].undo(to_undo[robot])
    return safe


def check_safe_transitions(prop, multi_rl, multi_firable):
    """
    For each firable transition:
        - Fire in SkillsetNet: tr
        - Get new marking
        - Check safety proposition, save value
        - Revert to original marking in SkillsetNet: undo
    Return the list of all 
    """
    unsafe_tr = []
    n = 0
    for i, to_fire in enumerate(multi_firable):
        robot = multi_rl[to_fire[0]]
        tr = to_fire[2]
        for t in tr:
            markings = dict()
            robot["net"].fire(t)
            markings[robot["name"]] = robot["net"].marking
            for name in multi_rl.keys():
                if name != robot["name"]:
                    markings[name] = multi_rl[name]["net"].marking
            if not check_safety_prop_from_markings(prop, markings):
                unsafe_tr.append(robot["name"]+":"+t.name)
            n += 1
            robot["net"].undo()
    return unsafe_tr


def check_safety_prop_from_markings(prop: str, markings):

    formula = prop.split(" ")
    for i, elem in enumerate(formula):
        front_par, back_par = "", ""
        if "(" == elem[0]:
            front_par = "("
            elem = elem[1:]
        if ")" == elem[-1]:
            back_par = ")"
            elem = elem[:-1]
        if ":" in elem:
            if len(elem.split(":")) == 2:
                robot, state = elem.split(":")
            else:
                robot, resource, state = elem.split(":")
            if state in markings[robot]:
                formula[i] = "True"
            else:
                formula[i] = "False"
            formula[i] = front_par + formula[i] + back_par

    formula = " ".join(formula)
    return eval(formula)
