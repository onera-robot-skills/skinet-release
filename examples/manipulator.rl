type {
    float
    Pose
    String
    JointStatus
    JointPosition
    JointTrajectory
    Path
}


skillset manipulator{

    data {
        joint_status : JointStatus              // int8 array
        computed_trajectory: JointTrajectory    // trajectory_angular
        articular_configuration: JointPosition  // current joint configuration
        end_effector_pose: Pose                 // end-effector pose
    }


    resource {
        ArmStatus {
            state { Home Ready Any }
            initial Home
            transition all
        }
        EndEffector_pos {
            state{Free Used Unavailable}
            initial Unavailable
            transition all
        }
        EndEffector_heading {
            state{Free Used Unavailable}
            initial Unavailable
            transition all
        }
        trajectory_status {
            state{None Computed Running}
            initial None
            transition {
                None -> Computed
                Computed -> Running
                Computed -> None
                Running -> None
            }
        }
    }

    event {
        
        EndEffector_pos_to_unavailable{
            effect EndEffector_pos -> Unavailable
        }

        EndEffector_heading_to_unavailable{
            effect EndEffector_heading -> Unavailable
        }

        EndEffector_pos_to_free{
            guard EndEffector_pos == Unavailable
            effect EndEffector_pos -> Free
        }

        EndEffector_heading_to_free{
            guard EndEffector_heading == Unavailable
            effect EndEffector_heading -> Free
        }
    }

    skill unlock_EndEffector_pos {
        input {
            unlock_direction:  JointPosition    // desired JointDirection
        }
        output {
            joint_position:  JointPosition    // reached JointPosition
        }
        precondition {
            EndEffector_pos_unavailable : EndEffector_pos == Unavailable 
        }
        start {
            ArmStatus -> Any
        }
        progress {
            period 0.1
            output{
                error: float
            }
        }
        interrupt  {
            interrupting true
        }
        success at_Idle {
           postcondition EndEffector_pos == Free
        }
    }

    skill unlock_EndEffector_heading {
        input {
            unlock_direction:  JointPosition    // desired JointDirection
        }
        output {
            joint_position:  JointPosition    // reached JointPosition
        }
        precondition {
            EndEffector_heading_unavailable : EndEffector_heading == Unavailable 
        }
        start {
            ArmStatus -> Any
        }
        progress {
            period 0.1
            output{
                error: float
            }
        }
        interrupt  {
            interrupting true
        }
        success at_Idle {
           postcondition EndEffector_heading == Free
        }
    }


    skill joystick_articular {

        input {
            joystick_topic:     String    // topic name publishing JointPosition
            timeout:  float // [s] Duration limit in Idle to switch in success 
        }

        precondition {
            end_effector_free :     EndEffector_pos == Free
                                and EndEffector_heading == Free
        }

        start {
            ArmStatus -> Any
            EndEffector_pos -> Used
            EndEffector_heading -> Used
        }

        invariant {
            pos_used {
                guard EndEffector_pos != Unavailable
                effect {
                    EndEffector_heading -> Free
                }
            }
            heading_used{
                guard EndEffector_heading != Unavailable
                effect {
                    EndEffector_pos -> Free
                }
            }
        }

        progress {
            period 0.1
            output{
                delay: float
            }
        }

        interrupt  {
            interrupting true
            effect {
                EndEffector_pos -> Free
                EndEffector_heading -> Free
            }
        }
        
        success end_Idle {
            effect {
                EndEffector_pos -> Free
                EndEffector_heading -> Free
            }
        }

        failure {
            joystick_timeout{
                effect {
                    EndEffector_pos -> Free
                    EndEffector_heading -> Free
                }
            }
        }
    }

    skill goto_articular {

        input {
            target:  JointPosition    // desired JointPosition
        }
        output {
            joint_position:  JointPosition    // reached JointPosition
        }

        precondition {
            end_effector_free :     EndEffector_pos == Free
                                and EndEffector_heading == Free
        }

        start {
            ArmStatus -> Any
            EndEffector_pos -> Used
            EndEffector_heading -> Used
        }

        invariant {
            pos_used {
                guard EndEffector_pos != Unavailable
                effect {
                    EndEffector_heading -> Free
                }
            }
            heading_used{
                guard EndEffector_heading != Unavailable
                effect {
                    EndEffector_pos -> Free
                }
            }
        }

        progress {
            period 0.1
            output{
                error: JointPosition
            }
        }

        interrupt  {
            interrupting true
            effect {
                EndEffector_pos -> Free
                EndEffector_heading -> Free
            }
        }
        
        success {
            end_Idle {
                effect {
                    ArmStatus -> Any
                    EndEffector_pos -> Free
                    EndEffector_heading -> Free
                }
            }
            end_Home {
                effect {
                    ArmStatus -> Home
                    EndEffector_pos -> Free
                    EndEffector_heading -> Free
                }
            }
            end_Ready {
                effect {
                    ArmStatus -> Ready
                    EndEffector_pos -> Free
                    EndEffector_heading -> Free
                }
            }
        }

    }

    skill joystick6dofs{

        input {
            joystick_topic:     String    // topic name publishing Pose
            timeout:  float // [s] Duration limit in Idle to switch in success 
        }

        precondition {
            end_effector_free :     EndEffector_pos == Free
                                and EndEffector_heading == Free
        }

        start {
            ArmStatus -> Any
            EndEffector_pos -> Used
            EndEffector_heading -> Used
        }

        invariant {
            pos_used {
                guard EndEffector_pos != Unavailable
                effect {
                    EndEffector_heading -> Free
                }
            }
            heading_used{
                guard EndEffector_heading != Unavailable
                effect {
                    EndEffector_pos -> Free
                }
            }
        }

        progress {
            period 0.1
            output{
                delay: float
            }
        }

        interrupt  {
            interrupting true
            effect {
                EndEffector_pos -> Free
                EndEffector_heading -> Free
            }
        }
        
        success end_Idle {
            effect {
                EndEffector_pos -> Free
                EndEffector_heading -> Free
            }
        }

        failure {
            joystick_timeout{
                effect {
                    EndEffector_pos -> Free
                    EndEffector_heading -> Free
                }
            }
        }
    }

    skill joystick_translate {

        input {
            joystick_topic:     String    // topic name publishing end-effector position
            timeout:  float // [s] Duration limit in Idle to switch in success 
        }
        precondition {
            end_effector_pos_free :     EndEffector_pos == Free
        }

        start {
            ArmStatus -> Any
            EndEffector_pos -> Used
        }

        invariant {
            pos_used {
                guard EndEffector_pos != Unavailable
            }
        }

        progress {
            period 0.1
            output{
                delay: float
            }
        }

        interrupt  {
            interrupting true
            effect {
                EndEffector_pos -> Free
            }
        }
        
        success end_Idle {
            effect {
                EndEffector_pos -> Free
            }
        }

        failure {
            joystick_timeout{
                effect {
                    EndEffector_pos -> Free
                    EndEffector_heading -> Free
                }
            }
        }
    }

    skill joystick_rotate {

        input {
            joystick_topic:     String    // topic name publishing heading
            timeout:  float // [s] Duration limit in Idle to switch in success 
        }

        precondition {
            end_effector_heading_free : EndEffector_heading == Free
        }

        start {
            ArmStatus -> Any
            EndEffector_heading -> Used
        }

        invariant {
            heading_used{
                guard EndEffector_heading != Unavailable
            }
        }

        progress {
            period 0.1
            output{
                delay: float
            }
        }

        interrupt  {
            interrupting true
            effect {
                EndEffector_heading -> Free
            }
        }
        
        success end_Idle {
            effect {
                EndEffector_heading -> Free
            }
        }
        failure {
            joystick_timeout{
                effect {
                    EndEffector_pos -> Free
                    EndEffector_heading -> Free
                }
            }
        }
    }

    skill ask_trajectory_from_angular{
        input {
            target: JointPosition // [rad]
            duration: float // [s] Duration limit to computed trajectory
        }
        precondition {
            not_running: trajectory_status != Running
        }
        start {
            trajectory_status -> None
        }
        interrupt {
            interrupting false
            effect trajectory_status -> None
        }
        success ok { effect trajectory_status -> Computed }
        failure ko { effect trajectory_status -> None }
    }

    skill ask_trajectory_from_pose{
        input {
            target: Pose
            duration: float // [s] Duration limit to computed trajectory
        }
        precondition {
            not_running: trajectory_status != Running
        }
        start {
            trajectory_status -> None
        }
        interrupt {
            interrupting false
            effect trajectory_status -> None
        }
        success ok { effect trajectory_status -> Computed }
        failure ko { effect trajectory_status -> None }
    }

    skill ask_trajectory_from_path{
        input {
            target: Path
            duration: float // [s] Duration limit to computed trajectory
        }
        precondition {
            not_running: trajectory_status != Running
        }
        start {
            trajectory_status -> None
        }
        interrupt {
            interrupting false
            effect trajectory_status -> None
        }
        success ok { effect trajectory_status -> Computed }
        failure ko { effect trajectory_status -> None }
    }

    skill follow_trajectory{
        precondition {
            trajectory_available: trajectory_status == Computed
            end_effector_free :     EndEffector_pos == Free
                                and EndEffector_heading == Free
        }
        start {
            ArmStatus -> Any
            EndEffector_pos -> Used
            EndEffector_heading -> Used
            trajectory_status -> Running
        }

        invariant {
            pos_used {
                guard EndEffector_pos != Unavailable
                effect {
                    EndEffector_heading -> Free
                    trajectory_status -> None
                }
            }
            heading_used{
                guard EndEffector_heading != Unavailable
                effect {
                    EndEffector_heading -> Free
                    trajectory_status -> None
                }
            }
        }

        progress {
            period 0.1
            output{
                error: float
            }
        }

        interrupt  {
            interrupting true
            effect {
                EndEffector_pos -> Free
                EndEffector_heading -> Free
                trajectory_status -> None
            }
        }

        success end_Idle {
            effect {
                EndEffector_pos -> Free
                EndEffector_heading -> Free
                trajectory_status -> None
            }
        }  
    }
}
