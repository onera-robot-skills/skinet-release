type {
    Float
    Pose
    String
    JointPosition
    JointTrajectory
    Path
}


skillset manipulator_explosion {

    data {
        computed_trajectory: JointTrajectory    // trajectory_angular
        articular_configuration: JointPosition  // current joint configuration
        end_effector_pose: Pose                 // end-effector pose
    }


    resource {
        joint_1 {
            state{Idle Running CloseUp CloseDown Lock}
            initial Idle
            transition all
        }
        joint_2 {
            state{Idle Running CloseUp CloseDown Lock}
            initial Idle
            transition all
        }
        joint_3 {
            state{Idle Running CloseUp CloseDown Lock}
            initial Idle
            transition all
        }
        joint_4 {
            state{Idle Running CloseUp CloseDown Lock}
            initial Idle
            transition all
        }
        joint_5 {
            state{Idle Running CloseUp CloseDown Lock}
            initial Idle
            transition all
        }
        joint_6 {
            state{Idle Running CloseUp CloseDown Lock}
            initial Idle
            transition all
        }
        // EndEffector_pos {
        //     state{Free Used}
        //     initial Free
        //     transition all
        // }
        // EndEffector_heading {
        //     state{Free Used}
        //     initial Free
        //     transition all
        // }
        // trajectory_status {
        //     state{None Computed Running}
        //     initial None
        //     transition {
        //         None -> Computed
        //         Computed -> Running
        //         Computed -> None
        //         Running -> None
        //     }
        // }
    }

    event {
        joint_1_to_Idle{
            effect joint_1 -> Idle
        }
        joint_1_to_Running{
            effect joint_1 -> Running
        }
        joint_1_to_Lock{
            effect joint_1 -> Lock
        }
        joint_1_to_CloseUp{
            effect joint_1 -> CloseUp
        }
        joint_1_to_CloseDown{
            effect joint_1 -> CloseDown
        }

        joint_2_to_Idle{
            effect joint_2 -> Idle
        }
        joint_2_to_Running{
            effect joint_2 -> Running
        }
        joint_2_to_Lock{
            effect joint_2 -> Lock
        }
        joint_2_to_CloseUp{
            effect joint_2 -> CloseUp
        }
        joint_2_to_CloseDown{
            effect joint_2 -> CloseDown
        }

        joint_3_to_Idle{
            effect joint_3 -> Idle
        }
        joint_3_to_Running{
            effect joint_3 -> Running
        }
        joint_3_to_Lock{
            effect joint_3 -> Lock
        }
        joint_3_to_CloseUp{
            effect joint_3 -> CloseUp
        }
        joint_3_to_CloseDown{
            effect joint_3 -> CloseDown
        }

        joint_4_to_Idle{
            effect joint_4 -> Idle
        }
        joint_4_to_Running{
            effect joint_4 -> Running
        }
        joint_4_to_Lock{
            effect joint_4 -> Lock
        }
        joint_4_to_CloseUp{
            effect joint_4 -> CloseUp
        }
        joint_4_to_CloseDown{
            effect joint_4 -> CloseDown
        }

        joint_5_to_Idle{
            effect joint_5 -> Idle
        }
        joint_5_to_Running{
            effect joint_5 -> Running
        }
        joint_5_to_Lock{
            effect joint_5 -> Lock
        }
        joint_5_to_CloseUp{
            effect joint_5 -> CloseUp
        }
        joint_5_to_CloseDown{
            effect joint_5 -> CloseDown
        }

        joint_6_to_Idle{
            effect joint_6 -> Idle
        }
        joint_6_to_Running{
            effect joint_6 -> Running
        }
        joint_6_to_Lock{
            effect joint_6 -> Lock
        }
        joint_6_to_CloseUp{
            effect joint_6 -> CloseUp
        }
        joint_6_to_CloseDown{
            effect joint_6 -> CloseDown
        }
    }

    // skill unlock_joint_1 {
    //     input {
    //         target: Float
    //     }
    //     output {
    //         angle: Float
    //     }
    //     precondition {
    //         joints_are_not_idle : joint_1 == Lock or joint_1 == CloseUp or joint_1 == CloseDown 
    //     }
    //     progress {
    //         period 0.1
    //         output{
    //             error: Float
    //         }
    //     }
    //     interrupt  {
    //         interrupting true
    //     }
    //     success at_Idle {
    //        postcondition joint_1 == Idle
    //     }
    //    failure {
    //        at_CloseUp {
    //            postcondition joint_1 == CloseUp
    //        }
    //        at_CloseDown {
    //            postcondition joint_1 == CloseDown
    //        }
    //        at_Lock {
    //            postcondition joint_1 == Lock
    //        }
    //    }
    // }

    // skill unlock_joint_2 {
    //     input {
    //         target: Float
    //     }
    //     output {
    //         angle: Float
    //     }
    //     precondition {
    //         joints_are_not_idle : joint_2 == Lock or joint_2 == CloseUp or joint_2 == CloseDown 
    //     }
    //     progress {
    //         period 0.1
    //         output{
    //             error: Float
    //         }
    //     }
    //     interrupt  {
    //         interrupting true
    //     }
    //     success at_Idle {
    //        postcondition joint_2 == Idle
    //     }
    //    failure {
    //        at_CloseUp {
    //            postcondition joint_2 == CloseUp
    //        }
    //        at_CloseDown {
    //            postcondition joint_2 == CloseDown
    //        }
    //        at_Lock {
    //            postcondition joint_2 == Lock
    //        }
    //    }
    // }

    // skill unlock_joint_3 {
    //     input {
    //         target: Float
    //     }
    //     output {
    //         angle: Float
    //     }
    //     precondition {
    //         joints_are_not_idle : joint_3 == Lock or joint_3 == CloseUp or joint_3 == CloseDown 
    //     }
    //     progress {
    //         period 0.1
    //         output{
    //             error: Float
    //         }
    //     }
    //     interrupt  {
    //         interrupting true
    //     }
    //     success at_Idle {
    //        postcondition joint_3 == Idle
    //     }
    //    failure {
    //        at_CloseUp {
    //            postcondition joint_3 == CloseUp
    //        }
    //        at_CloseDown {
    //            postcondition joint_3 == CloseDown
    //        }
    //        at_Lock {
    //            postcondition joint_3 == Lock
    //        }
    //    }
    // }

    // skill unlock_joint_4 {
    //     input {
    //         target: Float
    //     }
    //     output {
    //         angle: Float
    //     }
    //     precondition {
    //         joints_are_not_idle : joint_4 == Lock or joint_4 == CloseUp or joint_4 == CloseDown 
    //     }
    //     progress {
    //         period 0.1
    //         output{
    //             error: Float
    //         }
    //     }
    //     interrupt  {
    //         interrupting true
    //     }
    //     success at_Idle {
    //        postcondition joint_4 == Idle
    //     }
    //    failure {
    //        at_CloseUp {
    //            postcondition joint_4 == CloseUp
    //        }
    //        at_CloseDown {
    //            postcondition joint_4 == CloseDown
    //        }
    //        at_Lock {
    //            postcondition joint_4 == Lock
    //        }
    //    }
    // }

    // skill unlock_joint_5 {
    //     input {
    //         target: Float
    //     }
    //     output {
    //         angle: Float
    //     }
    //     precondition {
    //         joints_are_not_idle : joint_5 == Lock or joint_5 == CloseUp or joint_5 == CloseDown 
    //     }
    //     progress {
    //         period 0.1
    //         output{
    //             error: Float
    //         }
    //     }
    //     interrupt  {
    //         interrupting true
    //     }
    //     success at_Idle {
    //        postcondition joint_5 == Idle
    //     }
    //    failure {
    //        at_CloseUp {
    //            postcondition joint_5 == CloseUp
    //        }
    //        at_CloseDown {
    //            postcondition joint_5 == CloseDown
    //        }
    //        at_Lock {
    //            postcondition joint_5 == Lock
    //        }
    //    }
    // }

    // skill unlock_joint_6 {
    //     input {
    //         target: Float
    //     }
    //     output {
    //         angle: Float
    //     }
    //     precondition {
    //         joints_are_not_idle : joint_6 == Lock or joint_6 == CloseUp or joint_6 == CloseDown 
    //     }
    //     progress {
    //         period 0.1
    //         output{
    //             error: Float
    //         }
    //     }
    //     interrupt  {
    //         interrupting true
    //     }
    //     success at_Idle {
    //        postcondition joint_6 == Idle
    //     }
    //    failure {
    //        at_CloseUp {
    //            postcondition joint_6 == CloseUp
    //        }
    //        at_CloseDown {
    //            postcondition joint_6 == CloseDown
    //        }
    //        at_Lock {
    //            postcondition joint_6 == Lock
    //        }
    //    }
    // }

    skill joystick_articular {

        input {
            joystick_topic:  JointPosition    // desired JointPosition
        }
        output {
            joint_position:  JointPosition    // reached JointPosition
        }

        precondition {
            joints_are_idle :   joint_1 == Idle 
                            and joint_2 == Idle
                            and joint_3 == Idle 
            joints_are_also_idle: joint_4 == Idle 
                            and joint_5 == Idle 
                            and joint_6 == Idle
            // end_effector_free :     EndEffector_pos == Free
            //                     and EndEffector_heading == Free
        }

        // start {
        //     EndEffector_pos -> Used
        //     EndEffector_heading -> Used
        // }

        invariant {
            no_joints_locked {
                guard ((joint_1 == Running) or (joint_1 == Idle))
                    and ((joint_2 == Running) or (joint_2 == Idle))
                    and ((joint_3 == Running) or (joint_3 == Idle))
                    and ((joint_4 == Running) or (joint_4 == Idle))
                    and ((joint_5 == Running) or (joint_5 == Idle))
                    and ((joint_6 == Running) or (joint_6 == Idle))
                // effect {
                //     EndEffector_pos -> Free
                //     EndEffector_heading -> Free
                // }
            }
        }

        progress {
            period 0.1
            output{
                error: Float
            }
        }

        interrupt  {
            interrupting true
            // effect {
            //     EndEffector_pos -> Free
            //     EndEffector_heading -> Free
            // }
        }
        
        success end_Idle {
            // effect {
            //     EndEffector_pos -> Free
            //     EndEffector_heading -> Free
            // }
            postcondition    (joint_1 == Idle) 
                        and (joint_2 == Idle) 
                        and (joint_3 == Idle) 
                        and (joint_4 == Idle) 
                        and (joint_5 == Idle) 
                        and (joint_6 == Idle)
        }

        // failure {
        //     end_effector_lock{
        //         // effect {
        //             // EndEffector_pos -> Free
        //             // EndEffector_heading -> Free
        //         // }
        //         postcondition   ((joint_1 !=Idle) or (joint_2 !=Idle) or (joint_3 !=Idle)) 
        //                     and ((joint_4 !=Idle) or (joint_5 !=Idle) or (joint_6 !=Idle))
        //     }
        //     pos_lock{
        //         // effect {
        //             // EndEffector_pos -> Free
        //             // EndEffector_heading -> Free
        //         // }
        //         postcondition (joint_1 !=Idle) or (joint_2 !=Idle) or (joint_3 !=Idle)
        //     }
        //     heading_lock{
        //         // effect {
        //             // EndEffector_pos -> Free
        //             // EndEffector_heading -> Free
        //         // }
        //         postcondition (joint_4 !=Idle) or (joint_5 !=Idle) or (joint_6 !=Idle)
        //     }
        // }
    }

}
