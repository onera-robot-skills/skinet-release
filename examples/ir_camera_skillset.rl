type Float

skillset infrared_camera {

    skill detect_heatpoints {
        progress {
            period 1.0
            output {
                average_heat: Float
            }
        }
        interrupt {
            interrupting false
        }
        success heatpoint_detected {}
    }

}