type {
    Boolean
    Int
    Float
    String
    Vector3
    Battery
    GeoPoint
    GeoPointList
}

skillset uav {
    data {
        battery: Battery        // Battery state
        position: GeoPoint      // Current position (WGS84)
        origin: GeoPoint        // Skillset internal origin used to set target position
        home: GeoPoint          // Autopilot internal home position (emergency landing)
    }

    resource {
        authority {
            // extern
            state { Pilot Skill }
            initial Pilot
            transition all
        }

        flight_status {
            // extern
            state { NotReady OnGround InAir InAirUnsafe}
            initial NotReady
            transition all
        }

        origin_status {
            // internal
            state { Invalid Valid }
            initial Invalid
            transition all
        }

        motion {
            // internal
            state { Free Used }
            initial Free
            transition all
        }

        heading {
            // internal
            state { Free Used }
            initial Free
            transition all
        }

        battery {
            // extern
            state { Good Low Critical }
            initial Good
            transition all
        }
    }

    event {
        authority_to_pilot {
            effect authority -> Pilot
        }
        authority_to_skill {
            effect authority -> Skill
        }

        flight_status_to_not_ready {
            effect flight_status -> NotReady
        }
        flight_status_to_on_ground {
            effect flight_status -> OnGround
        }
        flight_status_to_in_air {
            effect flight_status -> InAir
        }
        flight_status_to_in_air_unsafe {
            effect flight_status -> InAirUnsafe
        }

        battery_to_good {
            effect battery -> Good
        }
        battery_to_low {
            effect battery -> Low
        }
        battery_to_critical {
            effect battery -> Critical
        }
    }

    skill ask_authority {
        precondition {
            no_authority : authority == Pilot
            // home_status : home_status == Valid // capture_home is unavailable on Anafi AI and Pixhawk
        }
        interrupt {
            interrupting true
            postcondition authority == Pilot
        }
        success ok {
            postcondition authority == Skill
        }
        failure ko {
            postcondition authority == Pilot
        }
    }
    // property: not F(authority == Pilot and (motion == Used or heading == Used))
    // invariant: not (authority == Pilot and (motion == Used or heading == Used))

    skill capture_origin {
        input {
            duration: Float // [s] Duration limit to catch gps signal and store the mean value
        }

        precondition {
            on_ground: (flight_status == NotReady) or (flight_status == OnGround)
        }

        success ok {
            postcondition origin_status == Valid
        }
        failure ko {
            postcondition origin_status == Invalid
        }
    }

    skill takeoff {
        // TODO
        // parameter {
        //     height: Float // [m] validate can fail if h>h_geo_fence (valid range: [1.0; 7.0])
        //     speed:  Float // [m/s] maximum ascending velocity (valid range: ]0.0; 3.0]
        // }
        input {
            height: Float // [m] validate can fail if h>h_geo_fence (valid range: [1.0; 7.0])
        }

        precondition {
            not_moving   : motion == Free
            has_authority: authority == Skill
            on_ground    : flight_status == OnGround
            battery_good : battery == Good
            origin_valid : origin_status == Valid
        }
        start {
            motion -> Used
        }
        invariant {
            in_control {
                guard motion == Used
            }
            has_authority {
                guard  authority == Skill
                effect motion -> Free
            }
            battery {
                guard battery != Critical
                effect motion -> Free
            }
        }
        progress {
            period 1.0
            output height: Float
        }
        interrupt {
            interrupting true
            effect motion -> Free
        }
        success at_altitude {
            effect motion -> Free
            postcondition flight_status == InAir
        }
        failure {
            grounded {
                effect motion -> Free
                postcondition flight_status == OnGround
            }
            emergency {
                effect motion -> Free
                postcondition flight_status == InAirUnsafe
            }
        }
    }

    skill landing {
        precondition {
            not_moving :    motion == Free
            has_authority : authority == Skill
            not_on_ground : (flight_status == InAir) or (flight_status == InAirUnsafe)
        }

        start {
            motion -> Used
        }

        invariant {
            in_control {
                guard motion == Used
            }
            has_authority {
                guard  authority == Skill
                effect motion -> Free
            }
        }
        progress {
            period 1.0
            output height: Float
        }
        interrupt {
            interrupting true
            effect motion -> Free
        }
        success {
            on_ground {
                effect motion -> Free
                postcondition flight_status == OnGround
            }
            stopped {
                effect motion -> Free
                postcondition flight_status == NotReady
            }
        }
        failure {
            aborted {
                effect motion -> Free
                postcondition flight_status == InAir
            }
            emergency {
                // drifted, crashed, ...
                effect motion -> Free
                postcondition flight_status == InAirUnsafe
            }
        }
    }


    // Previous go2setpoint without heading control
    skill goto_waypoint {
        input {
            target:   GeoPoint // latitude, longitude, altitude relative to homepoint
            speed:    Float
            obstacle_avoidance: Boolean
        }

        output {
            position: GeoPoint
        }

        precondition {
            not_moving   : motion == Free
            has_authority: authority == Skill
            in_air       : flight_status == InAir
            battery_not_critical : battery != Critical
        }

        start {
            motion -> Used
        }

        invariant {
            in_control {
                guard motion == Used
            }
            has_authority {
                guard  authority == Skill
                effect motion -> Free
            }
            in_air {
                guard flight_status == InAir
                effect motion -> Free
            }
            battery_not_critical {
                guard battery != Critical
                effect motion -> Free
            }
        }
        progress {
            period 1.0
            output distance: Float
        }
        interrupt {
            interrupting true
            effect motion -> Free
        }
        success arrived {
            effect motion -> Free
        }
        failure not_arrived {
            // drifted, blocked, etc.
            effect motion -> Free
        }
    }

    // Previous go2setpoint with heading to target
    skill goto_straight {
        input {
            target:   GeoPoint // latitude, longitude, altitude relative to homepoint
            speed:    Float
            obstacle_avoidance: Boolean
        }

        output {
            position: GeoPoint
        }

        precondition {
            not_moving   : motion == Free
            not_heading  : heading == Free
            has_authority: authority == Skill
            in_air       : flight_status == InAir
            battery_not_critical : battery != Critical
        }

        start {
            motion -> Used
            heading -> Used
        }

        invariant {
            in_control {
                guard (motion == Used) and (heading == Used)
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
            has_authority {
                guard  authority == Skill
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
            in_air {
                guard flight_status == InAir
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
            battery_not_critical {
                guard battery != Critical
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
        }
        progress {
            period 1.0
            output {
                distance: Float
                delta_angle: Float
            }
        }
        interrupt {
            interrupting true
            effect {
                motion -> Free
                heading -> Free
            }
        }
        success arrived {
            effect {
                motion -> Free
                heading -> Free
            }
        }
        failure not_arrived {
            // drifted, blocked, etc.
            effect {
                motion -> Free
                heading -> Free
            }
        }
    }

    // Previous: go2altitude, go2relative_meters, go2relative_meters_flu
    skill goto_relative {
        input {
            body:   Boolean       // expressed in body frame if true, in ENU if false
            target: Vector3       // Relative Position in meter
            speed:  Float
        }

        precondition {
            not_moving   : motion == Free
            has_authority: authority == Skill
            in_air       : flight_status == InAir
            battery_not_critical : battery != Critical
        }

        start {
            motion -> Used
        }

        invariant {
            in_control {
                guard motion == Used
            }
            has_authority {
                guard  authority == Skill
                effect motion -> Free
            }
            in_air {
                guard flight_status == InAir
                effect motion -> Free
            }
            battery_not_critical {
                guard battery != Critical
                effect motion -> Free
            }
        }
        progress {
            period 1.0
            output distance: Float
        }
        interrupt {
            interrupting true
            effect motion -> Free
        }
        success arrived {
            effect motion -> Free
        }
        failure not_arrived {
            // drifted, blocked, etc.
            effect motion -> Free
        }
    }

    skill set_heading {
        input {
            locked: Boolean     // Endless skill if locked is true, exit after reaching target heading if locked is false
            body:   Boolean     // Heading is expressed in body frame if true, in ENU frame if false
	        heading : Float	    // The desired heading of the UAV
        }

        output {
            heading: Float

        }

        precondition {
            not_heading   : heading == Free
            has_authority: authority == Skill
            in_air       : flight_status == InAir
            battery_not_critical : battery != Critical
        }
        start {
            heading -> Used
        }

        invariant {
            in_control {
                guard heading == Used
            }
            has_authority {
                guard  authority == Skill
                effect heading -> Free
            }
            in_air {
                guard flight_status == InAir
                effect heading -> Free
            }
            battery_not_critical {
                guard battery != Critical
                effect heading -> Free
            }
        }
        progress {
            period 1.0
            output delta_angle: Float
        }
        interrupt {
            interrupting true
            effect heading -> Free
        }
        success reached {
            effect heading -> Free
        }
        failure not_reached {
            // drifted, blocked, etc.
            effect heading -> Free
        }
    }

    // Previous: follow_path
    skill follow_path {
        input {
	        waypoint_list : GeoPointList    // Altitude is relative to homepoint [m]
            validation_distance : Float     // Threshold value to validate each point
        }

        output {
            position: GeoPoint
        }

        precondition {
            not_moving   : motion == Free
            has_authority: authority == Skill
            in_air       : flight_status == InAir
            battery_not_critical : battery != Critical
        }
        start {
            motion -> Used
        }
        invariant {
            in_control {
                guard motion == Used
                effect {
                    motion -> Free
                }
            }
            has_authority {
                guard  authority == Skill
                effect {
                    motion -> Free
                }
            }
            in_air {
                guard flight_status == InAir
                effect {
                    motion -> Free
                }
            }
            battery_not_critical {
                guard battery != Critical
                effect {
                    motion -> Free
                }
            }
        }

        progress {
            period 1.0
            output {
                next_id: Int
                distance_to_next: Float
                distance_to_end: Float
            }
        }
        interrupt {
            interrupting true
            effect {
                motion -> Free
            }
        }
        success arrived {
            effect {
                motion -> Free
            }
        }
        failure not_arrived {
            // drifted, blocked, etc.
            effect {
                motion -> Free
            }
        }
    }

    skill follow_path_straight {
        input {
	        waypoint_list : GeoPointList    // Altitude is relative to homepoint [m]
            validation_distance : Float     // Threshold value to validate each point
            obstacle_avoidance: Boolean
        }

        output {
            position: GeoPoint
        }

        precondition {
            not_moving   : motion == Free
            not_heading  : heading == Free
            has_authority: authority == Skill
            in_air       : flight_status == InAir
            battery_not_critical : battery != Critical
        }
        start {
            motion -> Used
            heading -> Used
        }
        invariant {
            in_control {
                guard (motion == Used) and (heading == Used)
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
            has_authority {
                guard  authority == Skill
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
            in_air {
                guard flight_status == InAir
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
            battery_not_critical {
                guard battery != Critical
                effect {
                    motion -> Free
                    heading -> Free
                }
            }
        }

        progress {
            period 1.0
            output {
                next_id: Int
                distance_to_next: Float
                distance_to_end: Float
            }
        }
        interrupt {
            interrupting true
            effect {
                motion -> Free
                heading -> Free
            }
        }
        success arrived {
            effect {
                motion -> Free
                heading -> Free
            }
        }
        failure not_arrived {
            // drifted, blocked, etc.
            effect {
                motion -> Free
                heading -> Free
            }
        }
    }

    // // Track position provided on update_topic relative to body frame
    // skill follow_target {
    //     input {
    //         relative_altitude: Float    // desired altitude relative to target
    //         update_topic:     String    // topic name publishing Point
    //         max_distance:     Float     // max distance in horizontal plane
    //         obstacle_avoidance: Boolean
    //     }

    //     precondition {
    //         not_moving   : motion == Free
    //         not_heading  : heading == Free
    //         has_authority: authority == Skill
    //         in_air       : flight_status == InAir
    //         battery_not_critical : battery != Critical
    //     }

    //     start {
    //         motion -> Used
    //         heading -> Used
    //     }

    //     invariant {
    //         in_control {
    //             guard (motion == Used) and (heading == Used)
    //             effect {
    //                 motion -> Free
    //                 heading -> Free
    //             }
    //         }
    //         has_authority {
    //             guard  authority == Skill
    //             effect {
    //                 motion -> Free
    //                 heading -> Free
    //             }
    //         }
    //         in_air {
    //             guard flight_status == InAir
    //             effect {
    //                 motion -> Free
    //                 heading -> Free
    //             }
    //         }
    //         battery_not_critical {
    //             guard battery != Critical
    //             effect {
    //                 motion -> Free
    //                 heading -> Free
    //             }
    //         }
    //     }

    //     progress {
    //         period 1.0
    //         output distance: Float
    //     }
    //     interrupt {
    //         interrupting true
    //         effect motion -> Free
    //     }
    //     failure emergency {
    //         // drifted, blocked, etc.
    //         effect motion -> Free
    //     }
    // }


}