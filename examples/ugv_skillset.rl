type
{
    Float
    Bool
    Int
    Battery
    Point
    GeoPose
    GeoPoint
    GeoPointList
    // StringList
}

skillset ugv
{
    data
    {
        battery: Battery
        pose: GeoPose
        //heading: Float          // North referenced heading - positive clockwise [deg]
        origin: GeoPoint
    }

    resource
    {
        motion
        {
            state { Free Used }
            initial Free
            transition all
        }

        origin
        {
            state { Invalid Valid }
            initial Invalid
            transition all
        }

        control_mode
        {
            state { Manual Auto }
            initial Manual
            transition all
        }

        // battery
        // {
        //     state { Good Low Critical }
        //     initial Good
        //     transition {
        //         Good -> Low
        //         Good -> Critical
        //         Low -> Critical
        //         Critical -> Low
        //     }
        // }

        // localisation
        // {
        //     state { Good Loose }
        //     initial Good
        //     transition all
        // }
    }

    // ========================================================================
    event
    {
        control_to_manual  { effect { control_mode -> Manual } }
        control_to_auto    { effect { control_mode -> Auto } }
        // battery_low        { effect { battery -> Low} }
        // battery_critical   { effect { battery -> Critical} }
        // localisation_loose { effect { localisation -> Loose } }
        // localisation_good  { effect { localisation -> Good } }
        emergency_stop     { effect { motion -> Free } }
    }

    // ========================================================================
    skill capture_origin
    {
        input {
            duration: Float
        }

        output origin: GeoPoint

        precondition {
            no_motion: (motion == Free)
        }

        invariant dont_move {
            guard (motion == Free)
            effect {
                origin -> Invalid
            }
        }

        progress {
            period 1.0
            output completion: Float
        }

        interrupt {
            interrupting false
            effect origin -> Invalid
            }

        success origin_captured { effect origin -> Valid }
        failure origin_error { effect origin -> Invalid }
    }

    // ========================================================================
    skill goto_body
    {
        input {
            target_position : Point     // Position in body frame [m]
            speed : Float               // [m/s] maximum travelling speed
        }

        output distance : Float

        precondition {
            control_available: (control_mode == Auto)
            motion_available:  (motion == Free)
        }

        start motion -> Used

        invariant {
            keep_control {
                guard (control_mode == Auto)
                effect motion->Free
            }
            keep_motion {
                guard (motion == Used)
                effect motion->Free
            }
        }

        progress {
            period 1.0
            output {
                distance: Float
                completion: Float
            }
        }

        interrupt {
            interrupting true
            effect motion -> Free
        }

        success arrived {
            effect motion -> Free
        }

        failure blocked {
            effect motion -> Free
        }
    }

    // ========================================================================
    skill goto_waypoint
    {
        input {
            target_position : GeoPoint  // Position in WGS84 (latitude, longitude) [deg]
            speed : Float               // [m/s] maximum travelling speed
        }

        output distance : Float

        precondition {
            control_available: (control_mode == Auto)
            motion_available:  (motion == Free)
            origin_captured:   (origin == Valid)
        }

        start motion -> Used

        invariant {
            keep_control {
                guard (control_mode == Auto)
                effect motion->Free
            }
            keep_motion {
                guard (motion == Used)
                effect motion->Free
            }
        }

        progress {
            period 1.0
            output {
                distance: Float
                completion: Float
            }
        }

        interrupt {
            interrupting true
            effect motion -> Free
        }

        success arrived {
            effect motion -> Free
        }

        failure blocked {
            effect motion -> Free
        }
    }

    // ========================================================================
    skill rotate
    {
        input {
            body_relative : Bool      //
            target_heading : Float    // North referenced heading - positive clockwise [deg]
        }

        precondition {
            control_available: (control_mode == Auto)
            motion_available:  (motion == Free)
            origin_captured:   (origin == Valid)
        }

        start motion -> Used

        invariant {
            keep_control {
                guard (control_mode == Auto)
                effect motion -> Free
            }
            keep_motion {
                guard (motion == Used)
                effect motion -> Free
            }
        }

        progress {
            period 1.0
            output {
                angular_distance: Float
                completion: Float
            }
        }

        interrupt {
            interrupting true
            effect motion -> Free
        }

        success arrived {
            effect motion -> Free
        }

        failure blocked {
            effect motion -> Free
        }
    }

    // ========================================================================
    skill follow_path
    {
        input {
            path: GeoPointList          // List of consecutive GeoPoint to follow (lat, lon)
            speed: Float                // [m/s] maximum travelling speed
        }

        output {
            index : Int                 // Index of the last target point
            distance : Float            // Distance to the last target point
        }

        precondition {
            control_available: (control_mode == Auto)
            motion_available:  (motion == Free)
            origin_captured:   (origin == Valid)
        }

        start motion -> Used

        invariant
        {
            keep_control {
                guard (control_mode == Auto)
                effect motion -> Free
            }
            keep_motion {
                guard (motion == Used)
                effect motion->Free
            }
        }

        progress {
            period 1.0
            output
            {
                index : Int                 // Index of the current target point
                distance : Float            // Distance to the current target point
            }
        }

        interrupt {
            interrupting true
            effect motion -> Free
        }

        success arrived {
            effect motion -> Free
        }

        failure blocked {
            effect motion -> Free
        }
    }
}