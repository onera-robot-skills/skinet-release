type {
    Pose2D
    Waypoint
    Float
    GeoPoint
    String
    float
    JointPosition
}

skillset spot_and_arm {

    data {
        battery: Float
        odom_pose: Pose2D
        graphnav_pose: Pose2D
        geo_pose: GeoPoint
    }

    resource {
        power_status {
            state { PowerOff PowerOn }
            initial PowerOff
            transition all
        }  
        lease_status {
            state { AutoMode ManualMode }
            initial AutoMode
            transition all
        }
        spot_status {
            state { Sitting Standing }
            initial Sitting
            transition all
        }
        control_mode {
            state { Idle Busy }
            initial Idle
            transition all
        }
        arm_status {
            state { Home Ready Any Unknown }
            initial Home
            transition all
        }
        end_effector_status {
            state{ Idle Busy Blocked }
            initial Idle
            transition all
        }
    }

    event {
        toauto_frommanual {
            guard lease_status == ManualMode
            effect lease_status -> AutoMode
        }
        tomanual_fromauto {
            guard lease_status == AutoMode
            effect lease_status -> ManualMode
        }
        end_effector_blocked {
            effect {
                end_effector_status -> Blocked
            }
        }
        end_effector_restored {
            effect {
                end_effector_status -> Idle
            }
        }
        // power_switchoff {
        //     guard power_status == PowerOn
        //     effect {
        //         power_status -> PowerOff
        //         spot_status -> Sitting
        //     }
        // }
        // power_switchon {
        //     guard power_status == PowerOff
        //     effect power_status -> PowerOn
        // }
        // status_standing {
        //     guard spot_status == Sitting and power_status == PowerOn
        //     effect spot_status -> Standing
        // }
        // status_sitting {
        //     guard spot_status == Standing
        //     effect spot_status -> Sitting
        // }
    }

    skill init_power {
        precondition {
            is_sitting: spot_status == Sitting
            can_move   : lease_status == AutoMode and control_mode == Idle
            is_powered : power_status == PowerOff
        }
        start {control_mode -> Busy}
        success is_poweredon {
            effect control_mode -> Idle
            postcondition power_status == PowerOn
        }
        failure couldnot_poweron {effect control_mode -> Idle}
    }

    skill safe_poweroff {
        precondition {
            is_sitting: spot_status == Sitting
            can_move   : lease_status == AutoMode and control_mode == Idle
            is_powered : power_status == PowerOn
        }
        start {control_mode -> Busy}
        success is_poweredoff {
            effect control_mode -> Idle
            postcondition power_status == PowerOff
        }
        failure couldnot_poweroff {effect control_mode -> Idle}
    }

    skill standup {
        precondition {
            is_sitting: spot_status == Sitting
            can_move   : lease_status == AutoMode and control_mode == Idle
            is_powered : power_status == PowerOn
        }
        start {control_mode -> Busy}
        invariant {
            is_powered {
                guard power_status == PowerOn
                effect control_mode -> Idle
            }
        }
        success is_standing {
            effect control_mode -> Idle
            postcondition spot_status == Standing
        }
        failure couldnot_stand {effect control_mode -> Idle}
    }

    skill sitdown {
        precondition {
            is_standing: spot_status == Standing
            can_move    : lease_status == AutoMode and control_mode == Idle
            is_powered  : power_status == PowerOn
        }
        start {control_mode -> Busy}
        invariant {
            is_powered {
                guard power_status == PowerOn
                effect control_mode -> Idle
            }
        }
        success is_sitting {
            effect control_mode -> Idle
            postcondition spot_status == Sitting
        }
        failure couldnot_sit {effect control_mode -> Idle}
    }

    skill spot_go_to {
        input target: Pose2D
        output position: Pose2D
        precondition {
            iswalking: spot_status == Standing
            can_move  : lease_status == AutoMode and control_mode == Idle
            is_powered: power_status == PowerOn
        }
        start {control_mode -> Busy}
        invariant {
            is_ok {
                guard lease_status == AutoMode and spot_status == Standing
                effect control_mode -> Idle
            }
        }
        progress{
            period 1.0
            output distance: Float
        }
        interrupt {
            interrupting true
            effect control_mode -> Idle
        }
        success is_arrived {effect control_mode -> Idle}
        failure not_arrived {effect control_mode -> Idle}
    }

    skill teleop {
        precondition {
            iswalking: spot_status == Standing
            can_move  : lease_status == AutoMode and control_mode == Idle
            is_powered: power_status == PowerOn
        }
        start {
            control_mode -> Busy
            lease_status -> ManualMode
        }
        invariant {
            is_ok {
                guard lease_status == ManualMode and spot_status == Standing
                effect control_mode -> Idle
            }
        }
        progress{period 1.0}
        interrupt { 
            interrupting true
            effect {
                control_mode -> Idle
                lease_status -> AutoMode
            }
        }
        success teleop_done {
            effect {
                control_mode -> Idle
                lease_status -> AutoMode
            }
        }
        failure teleop_failed {
            effect {
                control_mode -> Idle
                lease_status -> AutoMode
            }
        }
    }

    skill joystick_articular {

        input {
            joystick_topic:     String    // topic name publishing JointPosition
            timeout:  float // [s] Duration limit in Idle to switch in success 
        }
        precondition {
            spot_standing : spot_status == Standing
            end_effector_free :     end_effector_status == Idle
        }
        start {
            arm_status -> Any
            end_effector_status -> Busy
        }
        invariant {
            is_standing {
                guard spot_status == Standing
                effect end_effector_status -> Idle
            }
            is_ok {
                guard end_effector_status != Blocked
                effect arm_status -> Unknown

            }
        }
        progress {
            period 0.1
            output{
                delay: float
            }
        }
        interrupt  {
            interrupting true
            effect {
                end_effector_status -> Idle
            }
        }
        success end_Idle {
            effect {
                end_effector_status -> Idle
            }
        }
        failure {
            joystick_timeout{
                effect {
                    end_effector_status -> Idle
                }
            }
        }
    }

    skill goto_articular {
        input {
            target:  JointPosition    // desired JointPosition
        }
        output {
            joint_position:  JointPosition    // reached JointPosition
        }
        precondition {
            spot_standing : spot_status == Standing
            end_effector_free :     end_effector_status == Idle
        }
        start {
            arm_status -> Any
            end_effector_status -> Busy
        }
        invariant {
            is_standing {
                guard spot_status == Standing
                effect end_effector_status -> Idle
            }
            is_ok {
                guard end_effector_status != Blocked
                effect arm_status -> Unknown
            }
        }
        progress {
            period 0.1
            output{
                error: JointPosition
            }
        }
        interrupt  {
            interrupting true
            effect {
                end_effector_status -> Idle
            }
        }
        success {
            end_Idle {
                effect {
                    arm_status -> Any
                    end_effector_status -> Idle
                }
            }
            end_Home {
                effect {
                    arm_status -> Home
                    end_effector_status -> Idle
                }
            }
            end_Ready {
                effect {
                    arm_status -> Ready
                    end_effector_status -> Idle
                }
            }
        }

    }

}
