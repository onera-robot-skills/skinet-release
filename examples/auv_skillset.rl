type {
    Vector3
    GeoPoint
    Float
    Transect
}

skillset auv_skillset {

	data {
		battery: Float
		position: Vector3
		orientation: Vector3
        altitude: Float
        geo_position: GeoPoint
        origin: GeoPoint
        xy_error: Float
	}

    resource {
        lease_status {
            state { AutoMode ManualMode }
            initial AutoMode
            transition all
        }
        control_mode {
            state { Idle Busy }
            initial Idle
            transition all
        }
        power_status {
            state { PowerOff PowerOn }
            initial PowerOn
            transition all
        }  
		immersed_status{
			state { Surface Immersed }
			initial Surface
			transition all
		}

        // environment seabed_proximity {
        //     state { safe <> warning <> critical }
        //     thresholds {
        //         altitude.data >= 4.0
        //        | 4.0 > altitude.data > 2.0
        //        | altitude.data <= 2.0
        //     }
        //     forbid {
        //         -
        //         | dive_straight, skill_transect, go_to_waypoint
        //         | go_to_waypoint, dive_straight, go_to_body, skill_transect, teleop, stay
        //     }
        //     expect {
        //         -
        //         | go_to_body
        //         | reach_surface
        //     }
        // }
		seabed_proximity{
			state { Safe Warning Critical }
			initial Safe
			transition {
                Safe -> Warning
                Warning -> Safe
                Warning -> Critical
                Critical -> Warning
            }
		}
    }

    event {	
        power_switchoff { // loss of power
            guard power_status == PowerOn
            effect power_status -> PowerOff
        }
        power_switchon { // power retrieved
            guard power_status == PowerOff
            effect power_status -> PowerOn
        }

		status_surface { // is at surface
			guard immersed_status == Immersed
			effect immersed_status -> Surface
		}
		status_immersed { // is immersed
			guard immersed_status == Surface
			effect immersed_status -> Immersed
		}

        toauto_frommanual { // auto
            guard lease_status == ManualMode
            effect lease_status -> AutoMode
        }
        tomanual_fromauto { // manual
            guard lease_status == AutoMode
            effect lease_status -> ManualMode
        }

        seabed_to_safe { // safe
            guard seabed_proximity == Warning
            effect seabed_proximity -> Safe
        }
        seabed_to_warning { // warning
            guard seabed_proximity == Safe or seabed_proximity == Critical
            effect seabed_proximity -> Warning
        }
        seabed_to_critical { // critical
            guard seabed_proximity == Warning
            effect seabed_proximity -> Critical
        }
    }

    skill e_stop { // emergency self-stop
        start { power_status -> PowerOff }
        interrupt {
            interrupting true
            effect power_status -> PowerOn
        }
    }

    skill stay {
        input {
            timeout: Float
        }
		precondition {
			is_auto    : lease_status == AutoMode
			is_free    : control_mode == Idle
			is_powered : power_status == PowerOn
			seabed_proximity_not_critical : seabed_proximity != Critical
		}
		start {control_mode -> Busy}
       	invariant {
			is_auto {
				guard lease_status == AutoMode
				effect control_mode -> Idle
			}
			is_powered {
				guard power_status == PowerOn
				effect control_mode -> Idle
			}
            seabed_proximity_not_critical {
				guard seabed_proximity != Critical
				effect control_mode -> Idle
            }
        }
		interrupt {
			interrupting true
			effect control_mode -> Idle
		}
		success timeout_reached {
			effect control_mode -> Idle
		}
		failure drifted_away {effect control_mode -> Idle}
    }

	skill dive_straight {
		input {
			depth_goal : Float
		}
		precondition {
			is_auto    : lease_status == AutoMode
			is_free    : control_mode == Idle
			is_powered : power_status == PowerOn
		}
		start {control_mode -> Busy}
       	invariant {
			is_auto {
				guard lease_status == AutoMode
				effect control_mode -> Idle
			}
			is_powered {
				guard power_status == PowerOn
				effect control_mode -> Idle
			}
        }
		progress {
			period 1.0
			output distance: Float
		}
		interrupt {
			interrupting true
			effect control_mode -> Idle
		}
		success reached {
			effect control_mode -> Idle
			postcondition immersed_status == Immersed
		}
		failure couldnot_reach {effect control_mode -> Idle}
	}

	skill reach_surface {
		precondition {
			is_auto     : lease_status == AutoMode
			is_free     : control_mode == Idle
			is_powered  : power_status == PowerOn
			is_immersed : immersed_status == Immersed
		}
		start {control_mode -> Busy}
       	invariant {
			is_auto {
				guard lease_status == AutoMode
				effect control_mode -> Idle
			}
			is_powered {
				guard power_status == PowerOn
				effect control_mode -> Idle
			}
        }
		progress{
			period 1.0
			output distance: Float
		}
		interrupt {
			interrupting true
			effect control_mode -> Idle
		}
		success reached {
			effect control_mode -> Idle
			postcondition immersed_status == Surface
		}
		failure couldnot_reach {effect control_mode -> Idle}
	}

	skill go_to_body {	
		input {
			position_goal : Vector3
			orientation_goal : Vector3
		}
		precondition {
			is_auto    : lease_status == AutoMode
			is_free    : control_mode == Idle
			is_powered : power_status == PowerOn
			seabed_proximity_not_critical : seabed_proximity != Critical
		}

		start {control_mode -> Busy}
       	invariant {
			is_auto {
				guard lease_status == AutoMode
				effect control_mode -> Idle
			}
			is_powered {
				guard power_status == PowerOn
				effect control_mode -> Idle
			}
            seabed_proximity_not_critical {
				guard seabed_proximity != Critical
				effect control_mode -> Idle
            }
        }
		progress{
			period 1.0
			output distance: Float
		}
		interrupt {
			interrupting true
			effect control_mode -> Idle
		}
		success reached {effect control_mode -> Idle}
		failure couldnot_reach {effect control_mode -> Idle}
	}

	skill go_to_waypoint {	
		input {
			position_goal : GeoPoint
            depth_goal : Float
		}
		precondition {
			is_auto    : lease_status == AutoMode
			is_free    : control_mode == Idle
			is_powered : power_status == PowerOn
			seabed_proximity_safe : seabed_proximity == Safe
		}

		start {control_mode -> Busy}
       	invariant {
			is_auto {
				guard lease_status == AutoMode
				effect control_mode -> Idle
			}
			is_powered {
				guard power_status == PowerOn
				effect control_mode -> Idle
			}
            seabed_proximity_safe {
				guard seabed_proximity == Safe
				effect control_mode -> Idle
            }
        }
		progress{
			period 1.0
			output distance: Float
		}
		interrupt {
			interrupting true
			effect control_mode -> Idle
		}
		success reached {effect control_mode -> Idle}
		failure couldnot_reach {effect control_mode -> Idle}
	}

	skill teleop {	
		precondition {
			is_auto    : lease_status == AutoMode
			is_free    : control_mode == Idle
			is_powered : power_status == PowerOn
			seabed_proximity_not_critical : seabed_proximity != Critical
		}

		start { 
            control_mode -> Busy
            lease_status -> ManualMode
        }
       	invariant {
			is_auto {
				guard lease_status == ManualMode
				effect {
                    control_mode -> Idle
                    lease_status -> AutoMode
                }
			}
			is_powered {
				guard power_status == PowerOn
				effect {
                    control_mode -> Idle
                    lease_status -> AutoMode
                }
			}
            seabed_proximity_not_critical {
				guard seabed_proximity != Critical
				effect control_mode -> Idle
            }
        }
		interrupt {
			interrupting true
            effect {
                control_mode -> Idle
                lease_status -> AutoMode
            }
		}
        success teleop_success {
            effect {
                control_mode -> Idle
                lease_status -> AutoMode
            }
        }
		failure {
            lost_contact {
				effect {
                    control_mode -> Idle
                    lease_status -> AutoMode
                }
            }
            no_commands {
				effect {
                    control_mode -> Idle
                    lease_status -> AutoMode
                }
            }
        }
	}

	skill skill_transect {	
		input {
			transect : Transect
		}
		precondition {
			is_auto    : lease_status == AutoMode
			is_free    : control_mode == Idle
			is_powered : power_status == PowerOn
			seabed_proximity_safe : seabed_proximity == Safe
		}

		start {control_mode -> Busy}
       	invariant {
			is_auto {
				guard lease_status == AutoMode
				effect control_mode -> Idle
			}
			is_powered {
				guard power_status == PowerOn
				effect control_mode -> Idle
			}
            seabed_proximity_safe {
				guard seabed_proximity == Safe
				effect control_mode -> Idle
            }
        }
		progress{
			period 1.0
			output distance: Float
		}
		interrupt {
			interrupting true
			effect control_mode -> Idle
		}
		success done {effect control_mode -> Idle}
		failure transect_failed {effect control_mode -> Idle}
	}

}