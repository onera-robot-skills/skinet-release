type {
    Pose2D
    Float
}

skillset spot_easy {

    data {
        battery: Float
        position: Pose2D
        home: Pose2D
    }

    resource {
        power_status {
            state { PowerOff PowerOn }
            initial PowerOff
            transition all
        }  
        lease_status {
            state { AutoMode ManualMode }
            initial AutoMode
            transition all
        }
        control_mode {
            state { Idle Busy }
            initial Idle
            transition all
        }
    }

    event {
        toauto_frommanual {
            guard lease_status == ManualMode
            effect lease_status -> AutoMode
        }
        tomanual_fromauto {
            guard lease_status == AutoMode
            effect lease_status -> ManualMode
        }
        power_switchoff {
            guard power_status == PowerOn
            effect {
                power_status -> PowerOff
            }
        }
        power_switchon {
            guard power_status == PowerOff
            effect power_status -> PowerOn
        }
    }

    skill init_power {
        precondition {
            canmove   : lease_status == AutoMode and control_mode == Idle
            ispowered : power_status == PowerOff
        }
        start {control_mode -> Busy}
        progress{period 1.0}
        success is_poweredon {
            effect control_mode -> Idle
            postcondition power_status == PowerOn
        }
        failure {
            no_battery {effect control_mode -> Idle}
            is_estopped {effect control_mode -> Idle}
        }
    }

    skill safe_poweroff {
        precondition {
            canmove   : lease_status == AutoMode and control_mode == Idle
            ispowered : power_status == PowerOn
        }
        start {control_mode -> Busy}
        progress{period 1.0}
        success is_poweredoff {
            effect control_mode -> Idle
            postcondition power_status == PowerOff
        }
        failure couldnot_poweroff {effect control_mode -> Idle}
    }

    skill standup {
        precondition {
            canmove   : lease_status == AutoMode and control_mode == Idle
            ispowered : power_status == PowerOn
        }
        start {control_mode -> Busy}
        invariant {
            is_powered {
                guard power_status == PowerOn
                effect control_mode -> Idle
            }
        }
        success is_standing {
            effect control_mode -> Idle
        }
        failure couldnot_stand {effect control_mode -> Idle}
    }

    skill sitdown {
        precondition {
            canmove    : lease_status == AutoMode and control_mode == Idle
            ispowered  : power_status == PowerOn
        }
        start {control_mode -> Busy}
        invariant {
            is_powered {
                guard power_status == PowerOn
                effect control_mode -> Idle
            }
        }
        success is_sitting {
            effect control_mode -> Idle
        }
        failure couldnot_sit {effect control_mode -> Idle}
    }

    skill go_to {
        input target: Pose2D
        output position: Pose2D
        precondition {
            canmove  : lease_status == AutoMode and control_mode == Idle
            ispowered: power_status == PowerOn
        }
        start {control_mode -> Busy}
        invariant {
            is_auto {
                guard lease_status == AutoMode
            }
            is_powered {
                guard power_status == PowerOn
            }
        }
        progress{
            period 1.0
            output distance: Float
        }
        interrupt {
            interrupting true
            effect control_mode -> Idle
        }
        success is_arrived {effect control_mode -> Idle}
        failure not_arrived {effect control_mode -> Idle}
    }

}
